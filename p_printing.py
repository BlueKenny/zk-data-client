#!/usr/bin/env python3
import sys
sys.path.append("/etc/zk-data-libs")
import libs.send
import libs.BlueFunc
import time

secure_key = sys.argv[1]
#1.69-1.78
#1.94-2.01
t_min = 99
t_max = 0

server_liste = libs.send.get_server_list()
for server_data in server_liste:
    server_name = server_data[0]
    server_ip = server_data[1]
    
    if server_name == "burkardt_test":
        while True:
            t1 = libs.BlueFunc.Timestamp()
            answer = libs.send.pre_printing_documents(server_ip, secure_key)
            t2 = libs.BlueFunc.Timestamp()
            
            diff = round(t2-t1,2)
            if diff < t_min: t_min = diff
            if diff > t_max: t_max = diff
            
            print("time for printing: " + str(t_min) + " - " + str(t_max) + "s")
            
            if answer == ["", ""]: break
            else: time.sleep(10)
