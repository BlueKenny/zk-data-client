#!/usr/bin/env python3
version = "24.02.7"

import sys
sys.path.append("/etc/zk-data-libs/")
import libs.api.api_woocommerce
import libs.send
import libs.BlueFunc
import random

def script(ip, secure_key, article_as_dict): # return article_as_dict    
    print("article_as_dict: " + str(article_as_dict))
    
    server_name = libs.send.get_server_name(ip, secure_key):
    if server_name == "test":
        # Settings TEST
        api_woocommerce_url = "https://cars.zaunz.org/wp-json/wc/v3/"
        api_woocommerce_key = "ck_XXX"
        api_woocommerce_secret = "cs_XXX"
        categories_to_check = ["5"] # Only Cars
        enable_image = True
        enable_backorders = True
        categorie_key = "artikel2" # Categorie name ist artikel2 and not categorie
        upload_conditions = []
    else:
        return article_as_dict

    if not "wp-json/wc/v3" in api_woocommerce_url: api_woocommerce_url = api_woocommerce_url + "wp-json/wc/v3/"
    if not api_woocommerce_url[-1] == "/": api_woocommerce_url = api_woocommerce_url + "/"  


    if str(article_as_dict["categorie"]) in categories_to_check:
        stock_id = libs.api.api_woocommerce.search(api_woocommerce_url, api_woocommerce_key, api_woocommerce_secret, article_as_dict["identification"])
        if str(stock_id) == "None": # create  
            stock_id = libs.api.api_woocommerce.create_product(api_woocommerce_url, api_woocommerce_key, api_woocommerce_secret, article_as_dict)

        new_data = {}
        new_data["name"] = str(article_as_dict["name_de"])
        
        reloaded_art_dict = libs.send.get_article(ip, secure_key, article_as_dict["identification"])
        
        try: new_data["stock_quantity"] = str(article_as_dict["anzahl"])
        except: new_data["stock_quantity"] = str(reloaded_art_dict["anzahl"])
        try: new_data["description"] = str(article_as_dict["beschreibung_de"])
        except: new_data["description"] = str(reloaded_art_dict["beschreibung_de"])
        
        new_data["regular_price"] = str(article_as_dict["preisvk"])
        
        new_data["manage_stock"] = True
        new_data["short_description"] = "<div>Artikelnummer: " + str(article_as_dict["artikel"]) + "</div><div>EAN Barcode: " + str(article_as_dict["barcode"]) + "</div>"
        
        if enable_backorders:
            new_data["backorders"] = "notify"
            new_data["backorders_allowed"] = True
        else:
            new_data["backorders"] = "no"
            new_data["backorders_allowed"] = False
        
        cat_data = libs.api.api_woocommerce.get_categories(api_woocommerce_url, api_woocommerce_key, api_woocommerce_secret)
        categorie_name = article_as_dict[categorie_key]
        new_data["categories"] = ""
        for i_data in cat_data:            
            if i_data["name"] == categorie_name: new_data["categories"] = [{"id": i_data["id"]}]
        if new_data["categories"] == "":
            cat_data = libs.api.api_woocommerce.create_categorie(api_woocommerce_url, api_woocommerce_key, api_woocommerce_secret, categorie_name)
            new_data["categories"] = [{"id": cat_data["id"]}]
                        
        if str(article_as_dict["bild"])[0] == "/": article_as_dict["bild"]) = ""
        
        if enable_image:
            if not str(article_as_dict["bild"]) == "":                
                new_data["images"] = [{"src": article_as_dict["bild"]}]


        do_upload = True
        for key in upload_conditions:
            if str(article_as_dict[key]) == "": do_upload = False
            
        if do_upload:
            result = libs.api.api_woocommerce.set_data(api_woocommerce_url, api_woocommerce_key, api_woocommerce_secret, stock_id, new_data)

    return article_as_dict

if False:
    x = 12
    ip = "127.0.0.1:51515"
    secure_key = ""
    if True:#for x in range(10, 15):
        print("x: " + str(x))
        dicto = {}
        dicto["identification"] = 20000 + x
        dicto["categorie"] = "20"
        dicto["name_de"] = "Test - Artikel " + str(x)
        dicto["preisvk"] = random.randint(1, 20) * x / 10
        dicto["anzahl"] = random.randint(0, 100)
        dicto["barcode"] = 12345610121400 + random.randint(10, 99)
        dicto["beschreibung_de"] = "TESTO\nTEST2"
        dicto["artikel"] = "10" + str(random.randint(100, 999))
        dicto["artikel2"] = "MINIGT"
        dicto["bild"] = ""
        
        script(ip, secure_key, dicto)
