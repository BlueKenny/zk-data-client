#!/usr/bin/env python3

# this is a default script
# for article categorie 10 and 2
# add <G2> to name_de and name_fr

def script(article_as_dict): # return article_as_dict
    if str(article_as_dict["categorie"]) == "2" or str(article_as_dict["categorie"]) == "10":
        if not "<G2>" in article_as_dict["name_de"]:
            article_as_dict["name_de"] = article_as_dict["name_de"] + " <G2>"
            
        if not "<G2>" in article_as_dict["name_fr"]:
            article_as_dict["name_fr"] = article_as_dict["name_fr"] + " <G2>"

    print(article_as_dict)
    
    return article_as_dict
