#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
sys.path.append("/etc/zk-data-libs/")

from libs.appjar0_94 import gui

import libs.BlueFunc
import libs.send

def login():
    server_name = app.getOptionBox("ServerList")
    print("server_name: " + server_name)
    tk_last_selected_server_index = tk_server_list.index(server_name)
    print("tk_last_selected_server_index: " + str(tk_last_selected_server_index))
    libs.BlueFunc.set_data("tk_last_selected_server_index", tk_last_selected_server_index)
    ip = server_list[tk_last_selected_server_index][1]
    print("ip: " + ip)
    
    secure_key = app.getEntry("secure_key")
    
    user_data = libs.send.getUserData_from_key(ip, secure_key)
    '''
    if str(user_data) == "" or str(user_data) == "-1" or secure_key == None:
        
    else:
        os.system("python3 /etc/zk-data-client/qml/SucheStock.py " + ip + " \"" + secure_key + "\" &")
        app.stop()
    '''
     
    try:
        name = user_data["name"]
        os.system("python3 /etc/zk-data-client/qml/SucheStock.py " + ip + " \"" + secure_key + "\" &")
        app.stop()
    except:
        secure_key = app.infoBox("Passwort", "Passwort ist Falsch oder Server nicht erreichbar", parent=None)
    
    
    
app = gui("Login ZK-DATA", handleArgs=False)
app.setIcon("/usr/share/pixmaps/zk-data-stock.gif")
app.addLabel("title", "ZK-DATA Login")
app.enableEnter(login)

server_list = libs.send.get_server_list()
tk_server_list = []
for tk_server in server_list:
    tk_server_list.append(tk_server[0])

app.addLabelOptionBox("ServerList", tk_server_list)

tk_last_selected_server_index = libs.BlueFunc.get_data("tk_last_selected_server_index")
try: app.setOptionBox("ServerList", tk_last_selected_server_index, value=True, callFunction=True, override=False)
except: True

               
app.addLabelSecretEntry("secure_key")
app.setFocus("secure_key")

app.addButton("Login", login)
    
app.go()
