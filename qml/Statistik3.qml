import QtQuick 2.15
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.0// NEW

import QtCharts 2.0
import QtWebView 1.15

//Qt.openUrlExternally()

ScrollView {
    id: statsPage

    height: mainWindow.height
    width: mainWindow.width

    contentWidth: mainWindow.width
    contentHeight: rectangle_diagram.height + rectangle_details.height + 50

    property var jahr: 2025
    property var umsatz: " ? "
    property var gewinn: " ? "

    property var list_umsatz: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    property var list_ausgaben: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    property var list_gewinn: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    property var list_stock: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    property var list_stock_diff: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    property int y_min: 0
    property int y_max: 0

    clip: true

    function relaod_stats() {
        //mySeries.clear()
        statsPage.y_min = 0
        statsPage.y_max = 0

        statsPage.umsatz = 0.0
        statsPage.gewinn = 0.0

        console.warn("relaod_stats() for " + statsPage.jahr)
        python.call("libs.send.get_stats_gewinn", [vars.serverIP, vars.secure_key, statsPage.jahr], function(answer_dict) {            

            //checkbox_ausgaben_anzeigen
            var list_of_lists = [answer_dict["gewinn"], answer_dict["ausgaben"], answer_dict["umsatz"], answer_dict["stock"], answer_dict["stock_diff"]]

            list_gewinn = list_of_lists[0]
            list_ausgaben = list_of_lists[1]
            list_umsatz = list_of_lists[2]
            list_stock = list_of_lists[3]
            list_stock_diff = list_of_lists[4]


            // get Umsatz
            for (var i = 0; i < list_of_lists[2].length; i++) {
                if (list_of_lists[2][i] > statsPage.y_max) {
                    statsPage.y_max = list_of_lists[2][i] + 1
                }
                statsPage.umsatz = statsPage.umsatz + list_of_lists[2][i]
                statsPage.umsatz = parseFloat(statsPage.umsatz)//.toFixed(2)
            }

            // get Minimum
            for (var i = 0; i < list_of_lists[1].length; i++) {
                if (list_of_lists[1][i] < statsPage.y_min) {
                    statsPage.y_min = list_of_lists[1][i] - 1
                }
            }

            // get Gewinn
            for (var i = 0; i < list_of_lists[0].length; i++) {
                statsPage.gewinn = statsPage.gewinn + list_of_lists[0][i]
                statsPage.gewinn = parseFloat(statsPage.gewinn)
            }

            if (checkbox_ausgaben_anzeigen.checked == true) {
                axisY.min = statsPage.y_min

                barset_ausgaben.color = "red"
                barset_umsatz.color = "yellow"
                barset_gewinn.color = "lightgreen"
                barset_stock_diff.color = "lightblue"

                label_gewinn.text = "Gewinn: " + statsPage.gewinn.toFixed(2)

                rectangle_details.height = mainWindow.height
            } else {
                axisY.min = 0

                barset_ausgaben.color = "transparent"
                barset_umsatz.color = "lightgreen"
                barset_gewinn.color = "transparent"
                barset_stock_diff.color = "transparent"

                label_gewinn.text = ""

                rectangle_details.height = 0
            }


            chart_view.update()

            python.call("libs.statistik3.create_stats", [answer_dict], function(url) {

                webview_statistik.url = url
            })

        });
    }

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        onClicked: {
            view.push(frameSelect)
        }
    }

    Label {
        id: labelTitle
        text: "Statistik v3"
        font.pixelSize: vars.fontTitle
        x: mainWindow.width / 2 - width / 2
        y: buttonBack.height / 2 - height / 2
    }


    Rectangle {
        id: rectangle_diagram

        width: mainWindow.width
        height: mainWindow.height

        anchors.top: buttonBack.bottom
        anchors.topMargin: 10

        gradient: Gradient {
            GradientStop { position: 0.2; color: "lightsteelblue" }
            GradientStop { position: 1.5; color: "steelblue" }
        }

        color: vars.colorBackground

        CheckBox {
            id: checkbox_ausgaben_anzeigen
            text: "Ausgaben anzeigen"
            font.pixelSize: vars.fontTitle
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.bottom: label_umsatz.top
            anchors.bottomMargin: 10
            y: buttonBack.height / 2 - height / 2
            visible: false

            onCheckedChanged: {
                relaod_stats()
            }
        }

        Label {
            id: label_umsatz
            text: "Umsatz ohne MwSt: " + statsPage.umsatz.toFixed(2)
            font.pixelSize: vars.fontTitle
            anchors.right: button_year_back.left
            anchors.rightMargin: 20
            y: buttonBack.height / 2 - height / 2
        }
        Label {
            id: label_gewinn
            text: "Gewinn: " + statsPage.gewinn.toFixed(2)
            font.pixelSize: vars.fontTitle
            anchors.right: button_year_back.left
            anchors.rightMargin: 20
            anchors.top: label_umsatz.bottom
            anchors.topMargin: 10
            y: buttonBack.height / 2 - height / 2
        }

        ButtonSelect {
            id: button_year_back
            anchors.right: labelJahr.left
            anchors.rightMargin: 10
            text: " < "
            width: 50
            height: labelJahr.height
            y: labelJahr.y //+ height / 2
            onClicked: {
                statsPage.jahr = statsPage.jahr - 1
                relaod_stats()
            }
        }
        Label {
            id: labelJahr
            text: "Jahr: " + statsPage.jahr
            font.pixelSize: vars.fontTitle
            y: buttonBack.height / 2 - height / 2
            anchors.right: button_year_forward.left
            anchors.rightMargin: 10

        }

        ButtonSelect {
            id: button_year_forward
            anchors.right: rectangle_diagram.right
            anchors.rightMargin: 10
            text: " > "
            width: 50
            height: labelJahr.height
            y: labelJahr.y //+ height / 2
            onClicked: {
                statsPage.jahr = statsPage.jahr + 1
                relaod_stats()
            }
        }

        ChartView {
            id: chart_view
            //title: "Umsatz / Ausgaben / Gewinn in " + jahr
            title: "Umsatz: " + jahr
            anchors.top: label_gewinn.bottom
            anchors.topMargin: 10
            anchors.left: rectangle_diagram.left
            anchors.leftMargin: 10
            anchors.right: rectangle_diagram.right
            anchors.rightMargin: 10
            anchors.bottom: rectangle_diagram.bottom
            anchors.bottomMargin: 10


            legend.alignment: Qt.AlignBottom
            antialiasing: true


            BarSeries {
                id: mySeries
                axisY: ValueAxis {
                        id: axisY
                        min: statsPage.y_min
                        max: statsPage.y_max
                    }

                axisX: BarCategoryAxis { categories: ["Januar", "Februar", "März", "April", "Mai", "Juni", "July", "August", "September", "Oktober", "November", "Dezember"] }

                BarSet { id: barset_gewinn; label: "Gewinn"; values: statsPage.list_gewinn; color: "lightgreen"}
                BarSet { id: barset_ausgaben; label: "Ausgaben"; values: statsPage.list_ausgaben; color: "red"}
                BarSet { id: barset_umsatz; label: "Umsatz"; values: statsPage.list_umsatz; color: "yellow"}
                BarSet { id: barset_stock_diff; label: "Stock differenz"; values: statsPage.list_stock_diff; color: "lightblue"}

                //BarSet { label: "Umsatz"; values: statsPage.list_umsatz; color: "lightgreen"}
            }

        }
    }


    Rectangle {
        id: rectangle_details

        width: mainWindow.width
        height: 0

        anchors.top: rectangle_diagram.bottom
        anchors.topMargin: 10

        gradient: Gradient {
            GradientStop { position: 0.2; color: "lightsteelblue" }
            GradientStop { position: 1.5; color: "steelblue" }
        }

        //color: vars.colorBackground

        WebView {
            id: webview_statistik
            anchors.top: rectangle_details.top
            anchors.topMargin: 40
            anchors.bottom: rectangle_details.bottom
            anchors.bottomMargin: 40

            anchors.left: rectangle_details.left
            anchors.leftMargin: 40
            anchors.right: rectangle_details.right
            anchors.rightMargin: 40

        }
    }

    //InfoLabel {}
    //function setInfoLabelText(text) {vars.infoLabelText = text}
    //Busy {}
    //function busy(status) {vars.busy = status}

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.BlueFunc', function () {});
            importModule('libs.send', function () {});
            importModule('libs.statistik3', function () {});

            call("libs.send.getUserData_from_key", [vars.serverIP, vars.secure_key], function(userDATA) {
                if (userDATA["permission_stats"] > 1) {
                    checkbox_ausgaben_anzeigen.visible = true
                    barset_stock_diff.label = "Stock differenz"
                } else {
                    barset_stock_diff.label = ""
                }

                relaod_stats()

            })


        }
    }
}
