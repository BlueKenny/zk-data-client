import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.0
import QtQuick.Window 2.0

Rectangle {
    id: mainKalender
    //height: Screen.height; width: Screen.width
    //height: parent.height; width: parent.width
    height: 800; width: 800
    color: "white"
    visible: false
    z: 3
    border.width: 2
    radius: 40

    property int year: 2019
    property int month: 1
    property int day: 1

    property int show_month: 1
    property int show_year: 2019

    property int selected_day: 1

    property var weekday_translate: ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"]
    property var month_translate: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"]

    property var selected_date: ""

    property var month_to_string: ""
    property var day_to_string: ""

    signal set_date()
    // mouse_area_of_day

    onVisibleChanged: {
        if (selected_date == ""){
            show_year = year
            show_month = month
            selected_day = 1
        } else {
            show_year = selected_date.split("-")[0]
            show_month = selected_date.split("-")[1]
            selected_day = selected_date.split("-")[2]
        }


        getCalendar()
    }

    function getCalendar() {
        console.warn("getCalendar()")

        kalenderModel.clear()

        python.call("calendar.monthrange", [show_year, show_month], function (range) {
            var start_weekday = range[0]
            var end_day = range[1]

            for (var i = 0; i < start_weekday; i++) {// Leere tage bevor wochen begin
                kalenderModel.append({"day_number": "", "weekday": ""})
            }
            for (var i = 1; i < end_day + 1; i++) {
                var weekday = start_weekday + i
                var isActualDay = false
                var isSelectedDay = false

                buttonToday.visible = true
                if (show_year == year) {
                    if (show_month == month) {
                        buttonToday.visible = false
                        if (day == i) {
                            isActualDay = true
                        }
                    }
                }
                if (show_year ==selected_date.split("-")[0]) {
                    if (show_month == selected_date.split("-")[1]) {
                        if (selected_date.split("-")[2] == i) {
                            isSelectedDay = true
                        }
                    }
                }



                while (weekday > 7) {weekday = weekday - 7}

                //weekday = weekday.toString()
                kalenderModel.append({"day_number": i.toString(), "weekday": weekday_translate[weekday-1], "isActualDay": isActualDay, "isSelectedDay": isSelectedDay})
                //})
            }

        });
    }


    Rectangle {
        id: kalenderTopPanel
        width: parent.width
        height: 100
        border.width: 1
        color: "#1c9ec6"
        radius: mainKalender.radius

        Label {
            id: labelYear
            text: show_year
            font.pixelSize: 50
            y: parent.height / 2 - height / 2

        }

        Label {
            id: labelMonth
            text: show_month + " " + month_translate[show_month-1]
            x: parent.width / 2 - width / 2
            font.pixelSize: 50
            y: parent.height / 2 - height / 2
        }
        ButtonSelect {
            id: button_left
            text: "<-"
            width: kalenderGrid.width / 8 / 2
            height: parent.height / 2
            color: "transparent"
            anchors.right: labelMonth.left
            anchors.rightMargin: 10
            y: parent.height / 2 - height / 2
            onClicked: {
                show_month --
                if (show_month == 0) {show_year --; show_month = 12}
                getCalendar()
            }
        }
        ButtonSelect {
            id: button_right
            text: "->"
            width: kalenderGrid.width / 8 / 2
            height: parent.height / 2
            color: "transparent"
            anchors.left: labelMonth.right
            anchors.leftMargin: 10
            y: parent.height / 2 - height / 2
            onClicked: {
                show_month ++
                if (show_month == 13) {show_year ++; show_month = 1}
                getCalendar()
            }
        }

        ButtonSelect {
            id: buttonToday
            text: "Heute"
            width: kalenderGrid.width / 8 / 2
            height: parent.height / 2
            color: "transparent"
            anchors.right: buttonClose.left
            anchors.rightMargin: 10//parent.width / 12
            y: parent.height / 2 - height / 2
            onClicked: {
                show_year = year
                show_month = month
                getCalendar()
            }
        }        

        ButtonSelect {
            id: buttonClose
            text: "X"
            width: kalenderGrid.width / 8 / 2
            height: parent.height / 2
            color: "transparent"
            anchors.right: parent.right
            anchors.rightMargin: 10//parent.width / 12
            y: parent.height / 2 - height / 2
            onClicked: {
                mainKalender.visible = false
            }
        }
    }


    GridView {
        id: kalenderGrid
        anchors.top: kalenderTopPanel.bottom
        anchors.bottom: parent.bottom
        width: parent.width

        cellHeight: height / 5
        cellWidth: width / 7
        clip: true

        Component {
            id: kalenderDelegate
            Item {
                height: kalenderGrid.height / 5
                width: kalenderGrid.width / 7
                Rectangle {
                    height: kalenderGrid.height / 5
                    width: kalenderGrid.width / 7
                    border.width: 1

                    color: isActualDay ? "lightblue" : "transparent"

                    Label {
                        id: labelDayNumber
                        text: day_number
                        anchors.centerIn: parent
                        font.pixelSize: mouse_area_of_day.containsMouse ? 75 : 50
                        color: isSelectedDay ? "red": "black"
                    }
                    Label {
                        id: labelWeekday
                        text: weekday

                        font.pixelSize: 20
                    }

                    MouseArea {
                        id: mouse_area_of_day
                        anchors.fill: parent
                        z: 4
                        hoverEnabled: true

                        onClicked: {
                            selected_day = day_number

                            month_to_string = show_month.toString()
                            day_to_string = day_number.toString()

                            if (month_to_string.length == 1) {
                                month_to_string = "0" + month_to_string
                            }

                            if (day_to_string.length == 1) {
                                day_to_string = "0" + day_to_string
                            }

                            selected_date = show_year + "-" + month_to_string + "-" + day_to_string
                            mainKalender.visible = false

                            mainKalender.set_date()
                        }
                    }
                }
            }
        }

        ListModel {
            id: kalenderModel
        }

        delegate: kalenderDelegate
        model: kalenderModel
    }

    Python {
        id: python
        Component.onCompleted: {

            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.BlueFunc', function () {});
            importModule('libs.send', function () {});
            importModule('calendar', function () {});

            python.call("libs.BlueFunc.Date", [], function (get_day) {
                year = get_day.split("-")[0]
                month = get_day.split("-")[1]
                day = get_day.split("-")[2]
            })
        }

    }
}
