import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.0// NEW
//Qt.openUrlExternally()

Rectangle {
    id: statsPage
    width: mainWindow.width

    property var gesamt_summe: 0.0

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    color: vars.colorBackground

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        onClicked: {
            view.push(frameSelect)
        }
    }

    Label {
        id: labelTitle
        text: "Statistik"
        font.pixelSize: vars.fontTitle
        x: mainWindow.width / 2 - width / 2
        y: buttonBack.height / 2 - height / 2
    }

    Label {
        id: labelGesamt
        text: "Gesamt einnahmen: " + statsPage.gesamt_summe + " €"
        font.pixelSize: vars.fontTitle
        y: buttonBack.height / 2 - height / 2
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

    Chart {
        id: chart
        anchors.top: buttonBack.bottom
        anchors.topMargin: 20

        height: parent.height - buttonBack.height - 40
        width: parent.width
        x_names: ["Januar", "Februar", "März", "April", "Mai", "Juni", "July", "August", "September", "Oktober", "November", "Dezember"]
        points: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.BlueFunc', function () {});
            importModule('libs.send', function () {});

            call("libs.send.get_stats_rechnung", [vars.serverIP, vars.secure_key, "2021"], function(answer_list) {
                chart.points = answer_list
                chart.redraw()


                for (var i = 0; i < answer_list.length; ++i) {
                    statsPage.gesamt_summe = statsPage.gesamt_summe + answer_list[i]
                }
            })

        }
    }
}
