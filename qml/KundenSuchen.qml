import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0

Rectangle {
    height: mainWindow.height
    width: mainWindow.width

    color: vars.colorBackground

    Keys.onPressed: {
        if ( event.key === Qt.Key_F1 ) {
            key_F1()
        }
    }

    Item {
        id: variable
        property int last_id: 0
        property bool sucheRunning: false
        property int sucheIndex: 0
    }

    function suchen() {
        vars.busy = true

        variable.sucheIndex += 1
        var this_process = variable.sucheIndex

        contactModel.clear();

        console.warn("process: " + this_process)

        python.call("libs.send.SearchKunden", [vars.serverIP, vars.secure_key, {"identification":textKundenSuchenIdentification.text, "name":textKundenSuchenName.text, "ort":textKundenSuchenOrt.text}], function(liste) {
            vars.busy = false
            for (var i=0; i< liste.length; i++) {
                if (this_process == variable.sucheIndex) {
                    contactModel.append({"identification": liste[i]})
                }
            }
        });

        buttonKundenSuchenNeu.enabled = true
    }

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        onClicked: {
            variable.sucheIndex += 1

            vars.lieferscheinAnzeigenSetKundeID = false
            view.push(frameSelect)
        }
    }

    Label {
        id: labelKundenSuchenTitle
        text: "Kunden Suchen"
        x: mainWindow.width / 2 - width / 2
        y: buttonBack.height / 2 - height / 2
        font.pixelSize: vars.fontTitle
    }

    Label {
        text: "Identification"
        font.pixelSize: vars.fontText
        x: mainWindow.width / 3 - width / 2
        y: vars.headerPosY//mainWindow.height / 20
    }
    TextField {
        id: textKundenSuchenIdentification
        inputMethodHints: Qt.ImhDigitsOnly
        //text: vars.kundenSuchenTextIdentification
        width: vars.isPhone ? mainWindow.width / 5 : mainWindow.width / 10
        font.pixelSize: vars.fontText
        x: mainWindow.width / 3 * 2 - width / 2
        y: vars.headerPosY//mainWindow.height / 20
        selectByMouse: true
        onTextChanged: {
            vars.login_timeout = vars.login_max_timeout
            vars.kundenSuchenTextIdentification = text
            suchen()
        }
    }

    Label {
        text: "Name"
        font.pixelSize: vars.fontText
        x: mainWindow.width / 3 - width / 2
        y: textKundenSuchenIdentification.y + vars.headerPosSpacing//mainWindow.height / 20 * 2
    }
    TextField {
        id: textKundenSuchenName
        inputMethodHints: Qt.ImhNoPredictiveText
        //text: vars.kundenSuchenTextName
        width: vars.isPhone ? mainWindow.width / 5 : mainWindow.width / 10
        font.pixelSize: vars.fontText
        x: mainWindow.width / 3 * 2 - width / 2
        y: textKundenSuchenIdentification.y + vars.headerPosSpacing//mainWindow.height / 20 * 2
        font.capitalization: Font.AllUppercase
        selectByMouse: true
        onTextChanged:  {//onAccepted: {
            vars.login_timeout = vars.login_max_timeout
            vars.kundenSuchenTextName = text
            suchen()
        }
    }

    Label {
        text: "Ort"
        font.pixelSize: vars.fontText
        x: mainWindow.width / 3 - width / 2
        y: textKundenSuchenIdentification.y + vars.headerPosSpacing * 2//mainWindow.height / 20 * 2
    }
    TextField {
        id: textKundenSuchenOrt
        inputMethodHints: Qt.ImhNoPredictiveText
        //text: vars.kundenSuchenTextOrt
        width: vars.isPhone ? mainWindow.width / 5 : mainWindow.width / 10
        font.pixelSize: vars.fontText
        x: mainWindow.width / 3 * 2 - width / 2
        y: textKundenSuchenIdentification.y + vars.headerPosSpacing * 2//mainWindow.height / 20 * 2
        font.capitalization: Font.AllUppercase
        selectByMouse: true
        onTextChanged: {
            vars.login_timeout = vars.login_max_timeout
            vars.kundenSuchenTextOrt = text
            suchen()
        }
    }

    ButtonSelect {
        id: buttonKundenSuchenNeu
        text: "Neuer Kunde"
        x: mainWindow.width / 2 - width / 2
        y: mainWindow.height / 20 * 5
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText

        onClicked: {
            variable.sucheIndex += 1

            vars.busy = true
            buttonKundenSuchenNeu.enabled = false
            python.call("libs.BlueFunc.date_year_and_month_with_three_digit", [], function(start_id) {
                start_id = start_id + "00"
                python.call("libs.send.create_client_with_identification", [vars.serverIP, vars.secure_key, start_id], function(kunde_dict) {
                    buttonKundenSuchenNeu.enabled = true
                    vars.busy = false

                    vars.kundeAnzeigenKundeID = kunde_dict["identification"]
                    view.push(frameKundenAnzeigen)
                });
            })

        }
    }

    Label {
        anchors.right: parent.right
        y: buttonKundenSuchenNeu.y
        text: contactModel.count + " ergebnise"
    }

    ListView {
        id: listKundenSuchen
        y: mainWindow.height / 20 * 7
        //height: (mainWindow.height / 20) * 14
        width: mainWindow.width
        anchors.top: buttonKundenSuchenNeu.bottom
        anchors.bottom: parent.bottom

        ScrollBar.vertical: ScrollBar {
            active: true;
            //policy: ScrollBar.AlwaysOn
            width: vars.scrollbarWidth
        }

        clip: true

        ListModel {
            id: contactModel
        }
        Component {
            id: contactDelegate
            Item {
                id: itemListe
                width: listKundenSuchen.width
                height: vars.listItemHeight
                x: parent.width / 2 - width / 2

                Label {
                    id: labelKundenSucheListeID
                    //text: identification + " : "
                    font.pixelSize: vars.fontText
                    anchors.right: labelKundenSucheListeName.left
                    y: parent.height / 2 - height / 2
                    color: "green"
                }
                Label {
                    id: labelKundenSucheListeName
                    //text: name
                    font.pixelSize: vars.fontText
                    //anchors.left: labelKundenSucheListeID.right
                    //anchors.right: labelKundenSucheListeOrt.left
                    x: itemListe.width / 2 - width / 2
                    y: parent.height / 2 - height / 2
                }
                Label {
                    id: labelKundenSucheListeOrt
                    //text: " Ort: " + ort
                    font.pixelSize: vars.fontText
                    anchors.left: labelKundenSucheListeName.right
                    y: parent.height / 2 - height / 2
                    color: "red"
                }
                MouseArea {
                    anchors.fill: itemListe //labelKundenSucheListeEintrag
                    onClicked: {
                        variable.sucheIndex += 1
                        vars.kundeAnzeigenKundeID = identification
                        view.push(frameKundenAnzeigen)
                    }
                }

                Component.onCompleted: {
                    python.call("libs.send.GetKunden", [vars.serverIP, vars.secure_key, identification], function(item) {
                        //name = item["name"]
                        labelKundenSucheListeName.text = item["name"]
                        labelKundenSucheListeOrt.text = " Ort: " + item["ort"]

                        labelKundenSucheListeID.text = item["identification"] + ": "

                        labelKundenSucheListeID.font.strikeout = item["disabled"]
                        labelKundenSucheListeName.font.strikeout = item["disabled"]
                        labelKundenSucheListeOrt.font.strikeout = item["disabled"]
                    })
                }
            }
        }

        model: contactModel
        delegate: contactDelegate
    }

    InfoLabel {}
    function setInfoLabelText(text) {vars.infoLabelText = text}
    Busy {}

    Component.onCompleted: {
        vars.busy = false

        textKundenSuchenIdentification.text = vars.kundenSuchenTextIdentification
        textKundenSuchenName.text = vars.kundenSuchenTextName
        textKundenSuchenOrt.text = vars.kundenSuchenTextOrt


    }
/*
    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));

            importModule('libs.send', function () {});


        }
    }*/
}
