import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0

//import Fluid.Controls 1.0 as FluidControls

Rectangle {
    id: window
    color: vars.colorBackground

    property var index: 0
    property int max_index: 25
    property bool is_searching: false
    property int process: 0

    property int rest: 0

    Keys.onPressed: {
        console.warn("key " + event.key + " pressed")
        if ( event.key === Qt.Key_F1 ) {
            vars.lieferscheinSuchen_Angebote = false
            key_F1()
        }
        if ( event.key === Qt.Key_F2 ) {
            //if (vars.userData["permission_invoice"] > 0) {
            view.push(frameRechnungSuchen)
            //}
        }
        if ( event.key === Qt.Key_F3 ) {
            //if (vars.userData["permission_invoice_e"] > 0) {
            view.push(frameRechnungESuchen)
            //}
        }
        if ( event.key === Qt.Key_F12 ) {
            python.call("libs.send.delete_invoice_pdf", [vars.serverIP, vars.secure_key, vars.rechnungID], function() {})
        }
    }

    function rechnung_open_pdf(identification) {
        vars.busy = true

        python.call("libs.send.get_invoice_pdf", [vars.serverIP, vars.secure_key, identification], function(pdf_path){
            python.call("os.system", ["wget -N http://" + vars.serverIP + ":51515" + pdf_path + " -P /tmp/ && evince /tmp/" + identification + ".pdf &"], function() {
                vars.busy = false
            })
        })

        /*
        python.call("libs.send.print_invoice", [vars.serverIP, vars.secure_key, identification], function(link) {
            python.call("os.system",["evince /tmp/zk-data_pdf/" + vars.serverIP + "/" + identification + ".pdf &"], function() { });
            vars.busy = false
        })
        */

        /*
        python.call("PrinterSelect.main.Drucken", [vars.serverIP, vars.secure_key, 0, identification, true, 1], function(sucess) {
            console.warn("pdf: http://" + vars.serverIP + "/zk-data/Rechnung/" + identification + ".pdf")
            if (sucess == true) {
                python.call("os.system",["evince http://" + vars.serverIP + "/zk-data/Rechnung/" + identification + ".pdf &"], function() { });
                vars.busy = false
            }
        });
        */
    }

    function suchen(process_id) {
        is_searching = true
        vars.login_timeout = vars.login_max_timeout

        console.warn("rechnung suchen()")
        //search_rechnung(identification, kunde, rest, datum, index)
        python.call("libs.send.search_rechnung", [vars.serverIP, vars.secure_key, text_rechnung_identification.text, text_kunde_identification.text, checkbox_nicht_bezahlt.checked, "",index], function(answer) {
            console.warn("Search Rechnung: " + answer)

            if (answer == "-1" || answer == "") { // or
                is_searching = false
            } else {
                python.call("libs.send.get_rechnung", [vars.serverIP, vars.secure_key, answer], function(rechnung) {
                    index = index + 1

                    if (rechnung == -1) {

                    } else {
                        if (answer.indexOf("RV") === 0) {
                            rechnung["rest"] = 0
                        }

                        if (rechnung["rest"] == 0) {
                            rechnung["ist_bezahlt"] = "black"
                        } else {
                            rechnung["ist_bezahlt"] = "red"
                        }

                        if (process == process_id) {

                            if (rechnung_model.count < max_index) {

                                if (checkbox_faellig.checked == true) {
                                    var date_datum_now = new Date()
                                    var date_datum_todo = new Date(rechnung["datum_todo"])

                                    if (date_datum_now > date_datum_todo) {// add 1 day to today
                                        rechnung_model.append(rechnung)
                                        window.rest = window.rest + rechnung["rest"]
                                    }

                                } else {
                                    rechnung_model.append(rechnung)
                                    window.rest = window.rest + rechnung["rest"]
                                }

                                suchen(process_id)


                            } else {
                                rechnung_model.append({"identification": "-1", "datum": "Weiter Rechnungen Laden - Zu viele ergebnise !                                     ", "ist_bezahlt": "red", "datum_todo": ""})
                                is_searching = false
                            }



                        }
                    }

                })
            }
        })
    }

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        onClicked: {
            view.push(frameSelect)
        }
    }

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    Label {
        id: label_title
        text: "Rechnung Suchen (Ausgang)"
        font.pixelSize: vars.fontTitle
        x: parent.width / 2 - width / 2
        y: buttonBack.height / 2 - height / 2
    }

    Rectangle {
        id: rectangle_rechnung_identification
        width: label_rechnung_identification.width + text_rechnung_identification.width + button_kassenverkauf.width + button_rechnung.width
        height: text_rechnung_identification.height

        color: "transparent"
        //border.width: 1
        anchors.top: buttonBack.bottom
        anchors.topMargin: 10
        x: parent.width / 2 - width / 2

        Label {
            id: label_rechnung_identification
            text: "Rechnung n° "
            y: parent.height / 2 - height / 2

            //anchors.top: buttonBack.bottom
            //anchors.topMargin: 10
        }
        TextField {
            id: text_rechnung_identification
            //anchors.top: buttonBack.bottom
            //anchors.topMargin: 10
            selectByMouse: true
            anchors.left: label_rechnung_identification.right
            //enabled: is_searching ? false : true

            onTextChanged: {
                vars.rechnungSuchenIdentification = text
            }

            onEditingFinished: {
                if(focus == true) {
                    vars.rechnungSuchenIdentification = text

                    window.rest = 0
                    window.index = 0
                    process = process + 1
                    max_index = 25
                    rechnung_model.clear()
                    suchen(process)
                }
            }
        }
        ButtonSelect {
            id: button_kassenverkauf
            text: "Kassenverkauf"
            height: text_rechnung_identification.height
            width: vars.backButtonWidth / 2
            anchors.left: text_rechnung_identification.right
            anchors.leftMargin: 10
            //enabled: is_searching ? false : true
            onClicked: {
                text_rechnung_identification.text = "K"
                //checkbox_nicht_bezahlt.checked = true

                window.rest = 0
                window.index = 0
                process = process + 1
                max_index = 25
                rechnung_model.clear()
                suchen(process)
            }
        }
        ButtonSelect {
            id: button_rechnung
            text: "Rechnungen"
            height: text_rechnung_identification.height
            width: vars.backButtonWidth / 2
            anchors.left: button_kassenverkauf.right
            anchors.leftMargin: 10
            //enabled: is_searching ? false : true
            onClicked: {
                text_rechnung_identification.text = "R"
                //checkbox_nicht_bezahlt.checked = true

                window.rest = 0
                window.index = 0
                process = process + 1
                max_index = 25
                rechnung_model.clear()
                suchen(process)
            }
        }
        ButtonSelect {
            id: button_vorlage
            text: "Vorlagen"
            height: text_rechnung_identification.height
            width: vars.backButtonWidth / 2
            anchors.left: button_rechnung.right
            anchors.leftMargin: 10
            //enabled: is_searching ? false : true
            onClicked: {
                text_rechnung_identification.text = "RV"
                //checkbox_nicht_bezahlt.checked = true

                window.rest = 0
                window.index = 0
                process = process + 1
                max_index = 25
                rechnung_model.clear()
                suchen(process)
            }
        }

    }

    Rectangle {
        id: rectangle_kunde_identification
        width: label_kunde_identification.width + text_kunde_identification.width
        height: rectangle_rechnung_identification.height

        color: "transparent"
        anchors.top: rectangle_rechnung_identification.bottom
        anchors.topMargin: 10
        x: parent.width / 2 - width / 2

        Label {
            id: label_kunde_identification
            text: "Kunde "
            y: parent.height / 2 - height / 2

            //anchors.top: buttonBack.bottom
            //anchors.topMargin: 10
        }
        TextField {
            id: text_kunde_identification
            //anchors.top: buttonBack.bottom
            //anchors.topMargin: 10
            selectByMouse: true
            anchors.left: label_kunde_identification.right
            //enabled: is_searching ? false : true

            onTextChanged: {
                vars.rechnungSuchenKunde = text
            }

            onEditingFinished: {
                if(focus == true) {
                    vars.rechnungSuchenKunde = text

                    window.rest = 0
                    window.index = 0
                    process = process + 1
                    max_index = 25
                    rechnung_model.clear()
                    suchen(process)
                }
            }
        }
    }

    CheckBox {
        id: checkbox_nicht_bezahlt
        anchors.top: rectangle_kunde_identification.bottom
        anchors.topMargin: 10
        text: "Nur Offene Rechnungen Anzeigen"
        checked: vars.rechnungSuchenOffene
        //enabled: is_searching ? false : true
        onCheckedChanged: {
            if(focus == true) {
                vars.rechnungSuchenOffene = checked

                window.rest = 0
                window.index = 0
                process = process + 1
                max_index = 25
                rechnung_model.clear()
                suchen(process)
            }
        }
    }
    CheckBox {
        id: checkbox_faellig
        anchors.top: rectangle_kunde_identification.bottom
        anchors.topMargin: 10
        anchors.left: checkbox_nicht_bezahlt.right
        anchors.leftMargin: 10
        text: "Nur Rechnungen Anzeigen die das Fälligkeitsdatum erreicht haben"
        checked: vars.rechnungSuchenFaellig
        //enabled: is_searching ? false : true
        onCheckedChanged: {
            if(focus == true) {
                vars.rechnungSuchenFaellig = checked

                window.rest = 0
                window.index = 0
                process = process + 1
                max_index = 25
                rechnung_model.clear()
                suchen(process)
            }
        }
    }

    BusyIndicator {
        id: busyindication_inv_loading
        anchors.left: checkbox_faellig.right
        anchors.leftMargin: 10
        anchors.bottom: rectangle_rechnungen.top
        anchors.bottomMargin: 10
        visible: is_searching
    }
    
    ButtonSelect {
        id: button_print
        text: "Drucken"
        height: text_rechnung_identification.height
        width: vars.backButtonWidth / 2
        anchors.left: busyindication_inv_loading.right
        anchors.leftMargin: 10
        y: busyindication_inv_loading.y + height / 2
        onClicked: {
            vars.busy = true
            python.call("libs.create_document.get_list_invoices", [vars.serverIP, vars.secure_key, text_rechnung_identification.text, text_kunde_identification.text, checkbox_nicht_bezahlt.checked, checkbox_faellig.checked], function() {
                vars.busy = false
            })
                              
        }
    }

    Label {
        text: "Rest: " + window.rest
        anchors.right: label_count.left
        anchors.rightMargin: 10

        y: checkbox_nicht_bezahlt.y + checkbox_nicht_bezahlt.height / 2
    }

    Label {
        id: label_count
        text: "Ergebnise: " + rechnung_model.count
        //anchors.top: rectangle_kunde_identification.bottom
        //anchors.topMargin: 10
        anchors.right: parent.right

        y: checkbox_nicht_bezahlt.y + checkbox_nicht_bezahlt.height / 2
    }

    Rectangle {
        id: rectangle_rechnungen
        border.width: 1

        anchors.top: checkbox_nicht_bezahlt.bottom
        anchors.topMargin: 10
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        color: "transparent"


        ListView {
            id: rechnung_liste

            anchors.fill: parent
            clip: true

            ListModel {
                id: rechnung_model
            }

            onAtYEndChanged: {
                if (atYEnd == true) {
                    console.warn("Load more !!")

                    if (rechnung_model.count > 0) {
                        var last_line_identification = rechnung_model.get(rechnung_model.count - 1).identification

                        if (last_line_identification === -1) {
                            rechnung_model.remove(max_index, 1)
                            max_index = max_index + 25

                            process = process + 1
                            suchen(process)
                        } else {
                            console.warn("Nothing to load")
                        }

                    }

                }
            }

            Component {
                id: rechnung_delegate
                Item {
                    height: 40
                    width: rechnung_liste.width//parent.width

                    Rectangle {
                        height: 30
                        width: parent.width
                        //anchors.fill: parent
                        border.width: 1
                        color: "transparent"

                        MouseArea {
                            anchors.fill: parent
                            acceptedButtons: Qt.LeftButton | Qt.RightButton | Qt.MiddleButton
                            
                            hoverEnabled: true
                            
                            onEntered: {
                                label_rechnung_id.font.pixelSize = vars.fontTitle + 2
                                label_rechnung_kunde.font.pixelSize = vars.fontTitle + 2
                                label_rechnung_rest.font.pixelSize = vars.fontTitle + 2
                            }
                            onExited: {
                                label_rechnung_id.font.pixelSize = vars.fontTitle
                                label_rechnung_kunde.font.pixelSize = vars.fontTitle
                                label_rechnung_rest.font.pixelSize = vars.fontTitle
                            }

                            onClicked: {                                
                                rechnung_liste.currentIndex = index

                                if (identification == -1) {
                                    rechnung_model.remove(max_index, 1)
                                    max_index = max_index + 25

                                    process = process + 1
                                    suchen(process)

                                } else {
                                    if(mouse.button & Qt.RightButton) {
                                        vars.busy = true
                                        rechnung_open_pdf(identification)
                                    } else {
                                        if (mouse.button & Qt.MiddleButton) {
                                            text_rechnung_identification.text = ""
                                            text_kunde_identification.text = kunde_id
                                            vars.rechnungSuchenOffene = true
                                            vars.rechnungSuchenFaellig = false

                                            window.rest = 0
                                            window.index = 0
                                            process = process + 1
                                            rechnung_model.clear()
                                            suchen(process)

                                        } else {
                                            process = process + 1

                                            vars.rechnungID = identification
                                            view.push(frameRechnungAnzeigen)
                                        }
                                    }

                                }


                            }
                        }
                        Label {
                            id: label_rechnung_id
                            text: identification
                            anchors.left: parent.left
                            anchors.leftMargin: 20
                            font.pixelSize: vars.fontTitle
                        }
                        Label {
                            id: label_rechnung_kunde
                            text: kunde_id + ": " + kunde_name
                            anchors.left: label_rechnung_id.right
                            anchors.leftMargin: 20
                            font.pixelSize: vars.fontTitle
                            /*
                            Component.onCompleted: {
                                python.call("libs.send.GetKunden", [vars.serverIP, vars.secure_key, label_rechnung_kunde.text], function(answer) {
                                    label_rechnung_kunde.text = answer["identification"] + ": " + answer["name"]
                                })
                            }*/
                        }
                        Label {
                            id: label_rechnung_rappel
                            text: "Erinnerung: " + rappel
                            anchors.left: label_rechnung_kunde.right
                            anchors.leftMargin: 20
                            onTextChanged: {
                                if (rappel > 0) {
                                    visible = true
                                    if (rappel > 2) {
                                        color = "red"
                                    }

                                } else {
                                    visible = false
                                }

                            }
                        }


                        Label {
                            id: label_rechnung_rest
                            text: rest + " / " + total + " €"
                            x: parent.width / 2 - width / 2
                            font.pixelSize: vars.fontTitle
                            color: ist_bezahlt
                        }
                        Button {
                            id: button_rappel
                            anchors.right: label_rechnung_datum.left
                            anchors.rightMargin: 10
                            height: label_rechnung_datum.height
                            y: parent.height / 2 - height / 2
                            text: "Erinnerung senden"
                            visible: false

                            onClicked: {
                                vars.busy = true
                                python.call("libs.send.print_rappel", [vars.serverIP, vars.secure_key, kunde_id], function(dokument) {
                                    console.warn("dokument: " + dokument)
                                    var url = "http://" + vars.serverIP.split(":")[0] + "/zk-data/file/files/" + dokument
                                    console.warn(url)

                                    vars.busy = true

                                    python.call("libs.send.get_client", [vars.serverIP, vars.secure_key, kunde_id], function(kunde) {
                                        if (kunde["email3"].indexOf("@") !== -1) {
                                            if (kunde["email3"] === "POST@POST") { // no mail !
                                                python.call("os.system",["wget -N " + url + " -P /tmp/"], function() {
                                                    python.call("os.system",["evince /tmp/" + dokument + " &"], function() {
                                                        vars.busy = false
                                                    });
                                                });
                                            } else { // mail
                                                python.call("os.system",["wget -N " + url + " -P /tmp/"], function() {
                                                    python.call("os.system", ["thunderbird -compose to='" + kunde["email3"] + "',subject='" + "Rappel / Erinnerung" + "',body='',attachment='/tmp/" + dokument + "' &"], function() {
                                                        vars.busy = false
                                                    })
                                                });
                                            }
                                        } else { // no mail !
                                            python.call("os.system",["wget -N " + url + " -P /tmp/"], function() {
                                                python.call("os.system",["evince /tmp/" + dokument + " &"], function() {
                                                    vars.busy = false
                                                });
                                            });
                                        }


                                    })


                                    /*
                                    window.rest = 0
                                    window.index = 0
                                    process = process + 1
                                    max_index = 25
                                    rechnung_model.clear()
                                    suchen(process)
                                    */


                                })
                                /*
                                python.call("libs.send.set_rechnung_rappel", [vars.serverIP, vars.secure_key, identification], function(rappel_sucess) {
                                    if (rappel_sucess == true) {
                                        rechnung_open_pdf(identification)
                                        button_rappel.visible = false
                                    }
                                });
                                */
                            }
                        }


                        Label {
                            id: label_rechnung_datum
                            text: datum
                            anchors.right: label_rechnung_datum_todo.left
                            anchors.rightMargin: 20
                            font.pixelSize: vars.fontTitle
                        }
                        Label {
                            id: label_rechnung_datum_todo
                            text: datum_todo
                            anchors.right: parent.right
                            anchors.rightMargin: 20
                            font.pixelSize: vars.fontTitle
                            Component.onCompleted: {
                                var date_datum_now = new Date()
                                var date_datum_todo = new Date(label_rechnung_datum_todo.text)

                                if (date_datum_now > date_datum_todo) {// add 1 day to today
                                    if (rest !== 0) {
                                        //console.warn(identification + " Muss bezahlt werden !")
                                        label_rechnung_datum_todo.color = "red"
                                        button_rappel.visible = true
                                    }
                                }
                            }
                        }

                    }
                }
            }
            model: rechnung_model
            delegate: rechnung_delegate

        }
    }


    Component.onCompleted: {
        window.rest = 0
        window.index = 0

        text_rechnung_identification.text = vars.rechnungSuchenIdentification
        text_kunde_identification.text = vars.rechnungSuchenKunde
        //checkbox_nicht_bezahlt.checked = vars.rechnungSuchenOffene
        //checkbox_nicht_bezahlt.checked = vars.rechnungSuchenFaellig

        rechnung_model.clear()
        text_rechnung_identification.forceActiveFocus()

        process = process + 1
        //max_index = 25
        suchen(process)
    }

    /*
    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.create_document', function () {});
            importModule('PrinterSelect', function () {});
            importModule('os', function () {});


            window.rest = 0
            window.index = 0
            rechnung_model.clear()
            //suchen(window.process)

            text_rechnung_identification.forceActiveFocus()
        }
    }
    */
}
