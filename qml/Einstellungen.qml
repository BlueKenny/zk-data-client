import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.0// NEW
//Qt.openUrlExternally()

Rectangle {
    id: settingPage
    //height: mainWindow.height
    width: mainWindow.width

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    color: vars.colorBackground

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        onClicked: {
            view.push(frameSelect)
        }
    }

    Label {
        id: labelEinstellungenTitle
        text: "Einstellungen"
        font.pixelSize: vars.fontTitle
        x: mainWindow.width / 2 - width / 2
        y: buttonBack.height / 2 - height / 2
    }

    Label {
        id: labelDrucker
        text: "Drucker"
        font.pixelSize: vars.fontText
        //y: vars.headerPosY + vars.headerPosSpacing
        anchors.top: buttonBack.bottom
        anchors.topMargin: 40
        x: textEinstellungenServer.x / 2 - width / 2
    }
    TextField {
        id: textDruckerName
        text: vars.druckerIP
        width: mainWindow.width / 3 * 2
        font.pixelSize: vars.fontText
        x: mainWindow.width / 3 * 2 - width / 2
        //y: vars.headerPosY + vars.headerPosSpacing
        anchors.top: buttonBack.bottom
        anchors.topMargin: 40
        selectByMouse: true
        onTextChanged: {
            if (focus == true) {
                vars.druckerIP = text.toUpperCase()
                python.call("libs.BlueFunc.setData", ["PrinterIP", text.toUpperCase()], function() {});
            }
        }
    }

    ButtonSelect {
        id: buttonDonate
        //y: vars.headerPosY + vars.headerPosSpacing * 5
        anchors.top: textDruckerName.bottom
        anchors.topMargin: 40
        height: buttonBack.height
        x: parent.width / 2 - width / 2
        text: "Spenden"

        onClicked: {Qt.openUrlExternally("https://de.liberapay.com/BlueKenny/donate")}
    }

    ButtonSelect {
        id: buttonStartServer
        //y: vars.headerPosY + vars.headerPosSpacing * 5
        anchors.top: buttonDonate.bottom
        anchors.topMargin: 40
        height: buttonBack.height
        x: parent.width / 2 - width / 2
        text: "Eigenen Server Starten"
        visible: false

        onClicked: {
            python.call("os.system", ["cd /etc/zk-data/qml && gnome-terminal -e './runServer.py'"], function() {
                python.call("os.system", ["sleep 3"], function() {
                    view.push(frameSelect)
                })
            })
        }
    }

    ButtonSelect {
        id: buttonStartAPI
        //y: vars.headerPosY + vars.headerPosSpacing * 5
        anchors.top: buttonStartServer.bottom
        anchors.topMargin: 40
        height: buttonBack.height
        x: parent.width / 2 - width / 2
        text: "API Anbindung Starten"
        visible: true

        onClicked: {
            python.call("os.system", ["cd /etc/zk-data/qml/libs/AdminTools/ && gnome-terminal -e './api.py'"], function() { })
        }
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.BlueFunc', function () {});
            importModule('libs.send', function () {});

            call("libs.send.getUserList", [vars.serverIP, vars.secure_key], function(antwort) {
                console.warn("antwort: " + antwort)
                if (antwort == "") {
                    buttonStartServer.visible = true
                } else {
                    buttonStartServer.visible = false
                }

            })


        }
    }
}
