import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0


Rectangle {
    id: window
    width: mainWindow.width
    //anchors.bottom: keyboard.top
    height: mainWindow.height

    color: "blue"//vars.colorBackground

    Item {
        id: variable
        property int position : 0
    }

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    function addItemToList(item) {
        for (var i=0; i<item.length; i++) {
            console.warn("addItemToList: " + item[i]["identification"])
            contactModel.append(item[i]);
            liste.currentIndex = liste.count - 1
        }
    }

    Label {
        id: warningText
        text: ""
        font.pixelSize: window.width / 20
        x: window.width / 2 - width / 2
        y: window.height / 2 - height / 2
        visible: false
        color: "red"
    }

    ListView {
        id: liste
        anchors.top: window.top
        anchors.bottom: window.bottom
        width: window.width

        focus: true
        highlightMoveDuration: 100
        highlight: Rectangle { color: "green"; width: window.width}

        clip: true

        ScrollBar.vertical: ScrollBar {
            active: true;
            //policy: ScrollBar.AlwaysOn
            width: window.width / 35//vars.scrollbarWidth
        }

        ListModel {
            id: contactModel
        }
        Component {
            id: contactDelegate
            Item {
                id: itemListe
                width: liste.width
                height: vars.listItemHeight

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        liste.currentIndex = index
                        python.call("libs.BlueFunc.setClipboard", [textEK.text], function() {});
                    }
                    onDoubleClicked: {
                        python.call("libs.send.removeMinimum", [vars.serverIP, vars.secure_key, identification], function(sucess) {
                            if (sucess == true) {
                                textIdentification.color = "red"
                                textAnzahl.color = "red"
                                textName.color = "red"
                                textEK.color = "red"
                                textVK.color = "red"
                            }
                        });
                    }
                }
                Label {
                    id: textIdentification
                    text: identification
                    width: window.width / 10
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText

                }

                Label {
                    id: textAnzahl
                    text: anzahl + "x (" + restAnzahl + ")"
                    width: window.width / 10
                    anchors.left: textIdentification.right
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                }

                Label {
                    id: textName
                    text: name
                    anchors.left: textAnzahl.right
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                }

                Label {
                    id: textEK
                    text: artikel
                    x: window.width / 10 * 6
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                }

                Label {
                    id: textVK
                    text: lieferant
                    //anchors.right: liste.right
                    x: window.width / 10 * 9
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                }
            }
        }

        model: contactModel
        delegate: contactDelegate
    }

    function addItem() {
        console.warn("timerAddItem")

        python.call("libs.send.getMinimum", [vars.serverIP, vars.secure_key, variable.position], function(item) {
            console.warn("item: " + item["name_de"])
            if ( item["identification"] === undefined ) {
                interval = 1000
                variable.position = 0
            } else {
                addItemToList([item])
                variable.position += 1
                addItem()
            }
        });
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});

            warningText.visible = false

            addItem()


            //call("Einstellungen.main.GetName", [], function(namePC) {vars.namePC = namePC});



            //call("KundenSuchen.main.SearchKunden", [textKundenSuchenIdentification.text, textKundenSuchenName.text], function() {});
        }
    }
}
