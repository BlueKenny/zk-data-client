import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0
import QtQuick.Window 2.0//2.12

import Qt.labs.settings 1.0

ApplicationWindow {
    id: mainWindow
    height: Screen.height - 5; width: Screen.width
    title: qsTr("ZK-DATA - " + vars.serverNAME + " - " + vars.version)
    color: vars.colorBackground


    function key_F1() {
        console.warn("F1 Pressed")
        vars.lieferscheinSuchenTextIdentification = ""
        vars.lieferscheinSuchenTextRechnung = ""
        vars.lieferscheinSuchenTextName = ""
        vars.lieferscheinSuchenFertig = 0
        vars.lieferscheinSuchenCheckEigene = true
        vars.lieferscheinSuchenStart = true
        vars.lieferscheinSuchenInhalt = ""

        vars.lieferscheinSuchen_Angebote = false
        vars.lieferscheinSuchen_Bestellungen = false
        view.push(frameLieferscheinSuchen)
    }

    Settings {
        id: mainSettings
        property alias server_ip: vars.serverIP
        property alias server_name: vars.serverNAME
        property alias printer_ip: vars.druckerIP
    }

    Item {
        id: vars

        property string lieferscheinSuchenTextIdentification: ""
        property string lieferscheinSuchenTextName: ""
        property string lieferscheinSuchenTextRechnung: ""
        property int lieferscheinSuchenFertig: 0
        property bool lieferscheinSuchenCheckEigene: true
        property int lieferscheinSuchen_Last_Selection: 0
        property bool lieferscheinSuchen_Angebote: false
        property bool lieferscheinSuchen_Bestellungen: false
        property string lieferscheinSuchenInhalt: ""

        property bool lieferscheinSuchenStart: false

        property string lieferscheinID: ""
        property string lieferscheinDatum: ""
        property string lieferscheinAnzeigenKundeID: ""
       // wenn True dann wird gerade ein Kunde ausgewählt für einen Lieferschein
        property bool lieferscheinAnzeigenSetKundeID: false

        property string rechnungID: ""
        property string rechnungSuchenIdentification: ""
        property string rechnungSuchenKunde: ""
        property bool rechnungSuchenOffene: true
        property bool rechnungSuchenFaellig: false

        property string rechnungID_e: "-1"
        property string rechnungSuchenEIdentification: ""
        property string rechnungSuchenEKunde: ""
        property bool rechnungSuchenEOffene: true
        property string rechnungSuchenEDokumentID: ""
        property int rechnungSuchenEIndex: 2        
        property int rechnungSuchenEZoom: 75

        property int rechnungAnzeigenZahlungsart: 0

        property string secure_key: ""
        property string version: "????"
        property string kundenSuchenTextIdentification: ""
        property string kundenSuchenTextName: ""
        property string kundenSuchenTextOrt: ""
        property string kundeAnzeigenKundeID: ""
        property string kundenSuchenVorherigeAnsicht: ""

        property string druckerIP: "NULL"
        property string serverIP: "NULL"
        property string serverNAME: "NULL"
        property string serverVERSION: "NULL"
        property string namePC: "NULL"
        property string userColor: "NULL"
        property var userData: []

        property int login_timeout: -1
        property int login_max_timeout: -1

        property string kassenbuchDatum: "???"

        property string scanner_name: "NULL"

        property bool isPhone: false

        property int fontTitle: 18//vars.isPhone ? mainWindow.width / 30 : mainWindow.width / 80
        property int pageTitleY: mainWindow.height / 15 / 2 - fontTitle / 2// - height / 2
        property int backButtonHeight: 50//mainWindow.height / 15
        property int backButtonWidth: 200//mainWindow.width / 6
        property int headerPosY: mainWindow.height / 20 + vars.backButtonHeight
        property int headerPosSpacing: mainWindow.height / 20
        property int fontText: vars.fontTitle * 0.8
        property int scrollbarWidth: vars.isPhone ? 25 : 30        
        property int pixelDensity: Screen.pixelDensity

        property string infoLabelText
        property bool busy: true

        property string colorBackground: "lightsteelblue" //"#fffff0"
        //property string colorButton: "whitesmoke" //"#ffffe0"

        property var transaction_konto: []
        property var mwst: []
        property var article_categories: []

        property int listItemHeight: 55 //mainWindow.height / 12
    }

    ListModel {
        id: menu_listModel
        /*
        ListElement {
            name: "Einstellungen"
            frame: "Einstellungen.qml"
        }
        */
    }

/*
    BusyIndicator {
        id: busyindicator
        //running: true//image.status === Image.Loadings
        visible: true//vars.busy
        x: mainWindow.width / 2 - width / 2
        y: mainWindow.height / 2 - height / 2
    }*/

    StackView {
        id: view

        pushEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to:1
                duration: 200
            }
        }
        pushExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0
                duration: 200
            }
        }

        onCurrentItemChanged: {
            vars.login_timeout = vars.login_max_timeout
            console.warn("vars.timeout set to " + vars.login_max_timeout)
        }

        initialItem: frameSelect
        anchors.fill: parent

        Component {
            id: frameAdminTools
            AdminTools {}
        }
        Component {
            id: frameSelect
            Select {}
        }
        Component {
            id: frameStatistik
            Statistik3 {}
        }
        Component {
            id: frameStatistik4
            Statistik4 {}
        }
        /*
        Component {
            id: frameStock
            Stock {}
        }*/
        Component {
            id: frameTransaktionenImport
            Transaktionen_Import {}
        }
        Component {
            id: frameTransaktionen
            Transaktionen {}
        }
        Component {
            id: frameLieferscheinAnzeigen
            LieferscheinAnzeigen {}
        }
        Component {
            id: frameLieferscheinSuchen
            LieferscheinSuchen {}
        }
        Component {
            id: frameRechnungAnzeigen
            RechnungAnzeigen {}
        }
        Component {
            id: frameRechnungSuchen
            RechnungSuchen {}
        }
        Component {
            id: frameRechnungESuchen
            RechnungESuchen {}
        }
        Component {
            id: frameRechnungEAnzeigen
            RechnungEAnzeigen {}
        }
        Component {
           id: frameKundenAnzeigen
           KundenAnzeigen {}
        }
        Component {
            id: frameKundenSuchen
            KundenSuchen {}
        }
        Component {
           id: frameEinstellungen
           Einstellungen {}
        }
        Component {
           id: framePrinterSelect
           PrinterSelect {}
        }
        Component {
           id: frameInventar
           Inventar {}
        }
        Component {
           id: frameMinimum
           Minimum {}
        }
        Component {
           id: frameBeltSearch
           BeltSearch {}
        }
        Component {
            id: frameLogin
            Login {}
        }
    }

    function setInfoLabelText(text) {vars.infoLabelText = text}
    function busy(status) {vars.busy = status}


    Popup {
        id: popup
        x: (parent.width - width ) / 2
        y: (parent.height - height ) / 2
        width: parent.width / 2
        height: parent.height / 2
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        Label {
            id: popup_label
            text: vars.version
        }
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.BlueFunc', function () {});
            importModule('libs.barcode', function () {});
            importModule('PrinterSelect', function () {});
            importModule('libs.create_document', function () {});
            importModule('os', function () {});

            addImportPath(Qt.resolvedUrl('/etc/zk-data-client/qml/'));
            importModule('LieferscheinSuchen', function () {});

            vars.rechnungID = "K1"

            view.push(frameLogin)


        }

    }
}
