#!/usr/bin/env python3

import os
import threading
import time
import sys
sys.path.append("/etc/zk-data-libs/")

try: import pyotherside
except: True
import libs.send
import libs.BlueFunc


class Main:    
    def __init__(self):
        self.busy(False)

    def busy(self, status):
        status = bool(status)
        print("busy = " + str(status))
        pyotherside.send("busy", status)

    def printBarcode(self, nummer):
        print("printBarcode(" + str(nummer) + ")")

        nummer = str(nummer)
        dict_lieferschein = libs.send.GetLieferschein(secure_key, nummer)
        name = dict_lieferschein["kunde_name"]
        maschine = dict_lieferschein["maschine"]
        datum = dict_lieferschein["datum"]
        
        libs.barcode.PrintNoteBarcode(nummer, name, maschine, datum)

    def GetPrinter(self, ip, secure_key):
        print("GetPrinter()")
        self.busy(True)

        AntwortPrinterList = libs.send.GetPrinter(ip, secure_key)
        Index = 1
        PrinterList = []
        PrinterList.append({"name": "PDF", "position": "0"})

        for printer in AntwortPrinterList:
            PrinterList.append({"name": str(printer), "position": str(Index), "local": False})
            Index += Index

        

        LastPrinter = int(libs.BlueFunc.getData("LastPrinter"))

        pyotherside.send("antwortPrinters", PrinterList, LastPrinter)
        self.busy(False)

    def Drucken(self, ip, secure_key, Printer, Lieferschein, info, quantity):
        print("Drucken(" + str(secure_key) + ", " + str(Printer) + ", " + str(Lieferschein) + ", " + str(info) + ", " + str(quantity) + ")")
        sucess = libs.send.Print(ip, secure_key, Printer, Lieferschein, bool(info), int(quantity))
        return sucess

    def SetLastPrinter(self, Index):
        print("SetLastPrinter(" + str(Index) + ")")
        libs.BlueFunc.setData("LastPrinter", Index)
        
main = Main()

