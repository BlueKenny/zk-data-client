import QtQuick 2.7
import QtQuick.Controls 2.0

import io.thp.pyotherside 1.5


Rectangle {
    id: window
    height: mainWindow.height
    width: mainWindow.width


    function antwortPrinters(item, hightlight) {
        contactModel.clear();
        for (var i=0; i<item.length; i++) {
            contactModel.append(item[i]);
        }
        listViewPrinter.currentIndex = hightlight
    }


    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    color: vars.colorBackground


    ButtonSelect {
        id: buttonBack
        text: "Zurück"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        onClicked: {
            view.push(frameLieferscheinAnzeigen)
        }
    }

    ButtonSelect {
        id: buttonPrint
        text: "Drucken"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        anchors.right: window.right
        onClicked: {
                python.call("PrinterSelect.main.Drucken", [vars.serverIP, vars.secure_key, listViewPrinter.currentIndex, "L" + vars.lieferscheinID, checkBoxInfo.checked, spinbox_printing_quantity.value], function(sucess) {
                    console.warn("sucess of Printing " + sucess)
                    if (sucess == true) {
                        infoBox.text = "Lieferschein wird gedruckt"
                        infoBox.color = "lightgreen"
                        if (listViewPrinter.currentIndex == 0) {
                            python.call("os.system",["evince http://" + vars.serverIP + "/zk-data/Lieferscheine/L" + vars.lieferscheinID + ".pdf"], function() {});
                        }
                    } else {
                        infoBox.text = "Fehler beim Drucken"
                        infoBox.color = "orange"
                    }
                    infoBox.show = true
                });

            view.push(frameLieferscheinAnzeigen)
        }
    }

    Rectangle {
        id: rectangleInfo
        anchors.top: buttonBack.bottom
        //border.width: 2
        width: window.width
        height: window.height / 10
        color: "transparent"
        CheckBox {
            id: checkBoxInfo
            //x: rectangleInfo.width / 10 * 8
            y: rectangleInfo.height / 2 - height / 2
            text: "Infobereich Drucken"
            font.pixelSize: vars.fontText
            checked: true
        }
        ButtonSelect {
            id: buttonBarcode
            anchors.left: checkBoxInfo.right
            anchors.leftMargin: 20
            y: rectangleInfo.height / 2 - height / 2
            height: vars.backButtonHeight
            text: "Eintrag fürs Arbeitsbuch Drucken"
            onClicked: {
                //libs.barcode.PrintNoteBarcode(nummer, name, maschine, datum)
                python.call('PrinterSelect.main.printBarcode', [vars.lieferscheinID], function() {});
            }
        }

        SpinBox {
            id: spinbox_printing_quantity
            value: 1
            anchors.left: buttonBarcode.right
            anchors.leftMargin: 20
        }
        Image {
            id: gls_labeling
            source: "/etc/zk-data/qml/DATA/gls.png"
            anchors.left: spinbox_printing_quantity.right
            anchors.leftMargin: 10
            height: checkBoxInfo.height * 2
            fillMode: Image.PreserveAspectFit

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    python.call("libs.send.get_delivery", [vars.serverIP, vars.secure_key, vars.lieferscheinID], function(lieferschein) {
                        python.call("libs.send.GetKunden", [vars.serverIP, vars.secure_key, lieferschein["kunde_id"]], function(kunde) {
                            python.call("libs.gls.create_file", [vars.lieferscheinID, kunde["name"], kunde["adresse"], kunde["land"], kunde["plz"], kunde["ort"], kunde["tel1"], kunde["email1"]], function() {

                            })
                        })
                    })
                }
            }
        }
    }

    ListView {
        id: listViewPrinter
        anchors.top: rectangleInfo.bottom
        anchors.bottom: window.bottom
        width: window.width

        clip: true

        ScrollBar.vertical: ScrollBar {
            active: true;
            //policy: ScrollBar.AlwaysOn
            width: vars.scrollbarWidth
        }

        highlightMoveDuration: 100
        highlight: Rectangle {
            color: "steelblue"
            width: window.width
        }

        ListModel {
            id: contactModel
        }

        Component {
            id: contactDelegate
            Item {
                id: itemListe
                width: listViewPrinter.width
                height: vars.listItemHeight
                Label {
                    text: name
                    x: itemListe.width / 2 - width / 2
                    y: itemListe.height / 2 - height / 2
                    font.pixelSize: vars.fontText
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        listViewPrinter.currentIndex = index
                        python.call('PrinterSelect.main.SetLastPrinter', [index], function() {});
                    }
                }
            }
        }

        model: contactModel
        delegate: contactDelegate
    }

    InfoLabel {}
    function setInfoLabelText(text) {vars.infoLabelText = text}
    Busy {}
    function busy(status) {vars.busy = status}
    InfoBox {id: infoBox}

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('os', function () {});
            importModule('PrinterSelect', function () {});
            importModule('libs.send', function () {});
            importModule('libs.gls', function () {});

            setHandler("busy", busy);
            setHandler("antwortPrinters", antwortPrinters);


            call('PrinterSelect.main.GetPrinter', [vars.serverIP, vars.secure_key], function() {});
            //call('LieferscheinAnzeigen.main.GetIdentification', [], function(lieferscheinNummer) {labelLieferscheinAnzeigenTitle.text = "Lieferschein: " + lieferscheinNummer});
        }
    }

}

