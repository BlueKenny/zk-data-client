import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0

Rectangle {
    id: window_cashbook
    color: vars.colorBackground

    height: mainWindow.height
    width: mainWindow.width
    property var cashbook_image_path: ""


    property var date: ""
    property var konto: 1

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }


    function load_cashbook(query_date) {
        window_cashbook.date = query_date

        python.call("libs.send.get_cashbook_pdf", [vars.serverIP, vars.secure_key, konto, query_date], function(answer_path) {
            console.warn("answer_path: " + answer_path)
            load_pdf("http://" + vars.serverIP + ":51515" + answer_path)
        })


    }

    function load_pdf(pdf_url) {        
        vars.login_timeout = vars.login_max_timeout

        python.call("libs.BlueFunc.pdf_to_ppm", [pdf_url, ""], function(answer_path_list) {
            console.warn("answer_path_list: " + answer_path_list)

            cashbook_Model.clear()
            for (var i = 0; i < answer_path_list.length; i++) {
                cashbook_Model.append({"image_path": answer_path_list[i]})
            }

            vars.busy = false
        })

    }


    Label {
        id: label_cashbook_datum
        text: date
        x: window_cashbook.width / 2 - width / 2
        y: buttonBack.y + buttonBack.height / 2 - height / 2
    }


    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        onClicked: {
            view.push(frameSelect)
        }
    }
    ButtonSelect {
        id: buttonPDF
        text: "PDF Anzeigen"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        anchors.right: buttonDateBack.left
        anchors.rightMargin: 10
        enabled: vars.busy ? false : true
        onClicked: {
            vars.busy = true
            python.call("os.system", ["evince /tmp/" + konto + "_" + window_cashbook.date + ".pdf"], function() {vars.busy = false})
        }
    }

    ButtonSelect {
        id: buttonDateBack
        text: "Tag -1"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        anchors.right: buttonDateForward.left
        anchors.rightMargin: 10
        enabled: vars.busy ? false : true
        onClicked: {
            vars.busy = true
            python.call("libs.send.get_cashbook_date_before", [vars.serverIP, vars.secure_key, konto, window_cashbook.date], function(answer_date) {
                load_cashbook(answer_date)
            })
        }
    }
    ButtonSelect {
        id: buttonDateForward
        text: "Tag +1"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        anchors.right: window_cashbook.right
        anchors.rightMargin: 10
        enabled: vars.busy ? false : true
        onClicked: {
            vars.busy = true
            python.call("libs.send.get_cashbook_date_after", [vars.serverIP, vars.secure_key, konto, window_cashbook.date], function(answer_date) {
                load_cashbook(answer_date)
            })
        }
    }


    Rectangle {
        id: rectangle_cashbook
        width: parent.width// / 100 * vars.rechnungSuchenEZoom
        //height: window_rechnung_e_anzeigen.height
        anchors.top: buttonBack.bottom
        anchors.topMargin: 10
        anchors.bottom: window_cashbook.bottom


        border.width: 1

        ListView {
            id: list_cashbook
            anchors.fill: parent

            clip: true

            ListModel {
                id: cashbook_Model
            }


            delegate: cashbook_Delegate
            model: cashbook_Model
            focus: true

            Component {
                id: cashbook_Delegate
                Item {
                    id: cashbook_item
                    width: rectangle_cashbook.width
                    height: width*1.7

                    Text {
                        id: cashbook_text
                        anchors.left: cashbook_item.left
                        anchors.leftMargin: 10
                        text: cashbook_image_path
                    }

                    Image {
                        id: cashbook_img
                        anchors.fill: cashbook_item
                        fillMode: Image.PreserveAspectFit

                        source: image_path
                    }

                }
            }

        }
    }

    Busy { }


    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.BlueFunc', function () {});

            vars.busy = true
            call("libs.send.get_cashbook_date_last", [vars.serverIP, vars.secure_key, konto], function(answer_datum) {
                console.warn("answer_datum: " + answer_datum)
                load_cashbook(answer_datum)
            })





        }
    }

}
