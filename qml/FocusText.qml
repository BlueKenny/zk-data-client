import QtQuick 2.7
import QtQuick.Controls 2.0
import io.thp.pyotherside 1.4

Item {
    id: root
    signal closed()

    //property bool visible: false
    property string text: ""
    property bool enabled_text: true
    property string border_color: "#80000000"

    z: parent.z + 5
    visible: false
    anchors.fill: parent

    onVisibleChanged: {
        if (visible == true) {textfield.forceActiveFocus()}
    }

    Rectangle {
        id: rootRectangle
        anchors.fill: parent
        visible: root.visible
        z: 2
        color: root.border_color//"#80000000"
        Label {
            text: "INFO"
            x: rootRectangle.width / 2 - width / 2
            font.pixelSize: vars.fontTitle
            y: rectangle.y / 2 - height / 2
        }

        MouseArea {
            id: mouseAreaClose
            anchors.fill: rootRectangle
            z: rootRectangle.z - 2
            onClicked: {
                if ( rectangle.x > mouseX | mouseX > (rectangle.x + rectangle.width) | rectangle.y > mouseY | mouseY > (rectangle.y + rectangle.height )) {
                    root.visible = false
                    Qt.inputMethod.hide()
                    closed()
                }
            }
        }
        Rectangle {
            id: rectangle
            z: rootRectangle.z + 1
            border.width: 2
            radius: 20
            height: parent.height / 4 * 3
            width: parent.width / 4 * 3
            anchors.centerIn: parent
            TextArea {
                id: textfield
                anchors.fill: parent
                text: root.text
                enabled: root.enabled_text
                selectByMouse: true
                onTextChanged: {root.text = text}
                font.pixelSize: vars.fontText + 2
                //maximumLength: 32 //<TODO>
            }

        }
    }

}
