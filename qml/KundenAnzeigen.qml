import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.0

Rectangle {
    id: window
    width: mainWindow.width
    anchors.bottom: parent.bottom

    color: vars.colorBackground

    Keys.onPressed: {
        if ( event.key === Qt.Key_F1 ) {
            key_F1()
        }
        if ( event.key === Qt.Key_F7 ) {
            python.call("libs.send.get_logs", [vars.serverIP, vars.secure_key, "client" , vars.kundeAnzeigenKundeID], function(file) {
                python.call("os.system", ["wget http://" + vars.serverIP.split(":")[0] + file + " -O /tmp/client_log.csv"], function() {
                    python.call("os.system", ["libreoffice /tmp/client_log.csv &"], function() {})
                })
            })
        }
        if ( event.key === Qt.Key_F9 ) {
            python.call("libs.barcode.PrintClient", [vars.kundeAnzeigenKundeID, textName.text, textAdresse.text, textLand.text, textPlz.text, textOrt.text, textTVA.text], function() {
                //
            })
        }
    }

    Item {
        id: variable
        property int identification
    }

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    function get_client() {
        kundenTotalVK.text = ""
        kundenTotalEK.text = ""
        // DISABLE ALL ???
        vars.busy = true

        python.call("libs.send.get_client", [vars.serverIP, vars.secure_key, vars.kundeAnzeigenKundeID], function(client_dict) {
            textName.text = client_dict["name"]
            textPlz.text = client_dict["plz"]
            textOrt.text = client_dict["ort"]
            textLand.text = client_dict["land"]
            textAdresse.text = client_dict["adresse"]
            textTel1.text = client_dict["tel1"]
            textTel2.text = client_dict["tel2"]
            textTel3.text = client_dict["tel3"]
            textMail1.text = client_dict["email1"]
            textMail2.text = client_dict["email2"]
            textMail3.text = client_dict["email3"]
            textPaymentDeadline.text = client_dict["payment_deadline"]
            textTVA.text = client_dict["tva"]
            switchSpracheDE.checked = client_dict["sprache_de"]
            switchSpracheFR.checked = client_dict["sprache_fr"]
            switchSpracheNL.checked = client_dict["sprache_nl"]
            textInfo.text = client_dict["info"]
            textWarning.text = client_dict["warning"]
            checkBoxDealer.checked = client_dict["dealer"]
            checkBoxSupplier.checked = client_dict["supplier"]
            checkBoxSupplierSepa.checked = client_dict["sepa"]
            checkBoxDisabled.checked = client_dict["disabled"]
            if (client_dict["disabled"] == 1) {buttonOK.enabled = false} else {buttonOK.enabled = true}
            label_erstellt.text = "Erstellt am: " + client_dict["creation"]

            if (vars.userData["admin"] === true) {
                textWarning.enabled = true
            } else {
                textWarning.enabled = false
            }

            if (client_dict["tva"] == "") {
                label_tva_status.color = "black"
                label_tva_status.text = ""
            } else {
                if (client_dict["tva_status"] == "1") {label_tva_status.color = "green"}
                else {label_tva_status.color = "red"}
                label_tva_status.text = client_dict["tva_status_text"]

            }

            var timestamp = Number(client_dict["lastchange"]) * 1000
            var date = new Date(timestamp);
            date = date.toLocaleString();
            label_last_changed.text = "Letzte änderung am " + date

            label_rappel.text = "Anzahl der Erinnerungen: " + client_dict["rappel"]
            if (client_dict["rappel"] < 2) {label_rappel.color = "green"}
            if (client_dict["rappel"] == 2) {label_rappel.color = "orange"}
            if (client_dict["rappel"] > 2) {label_rappel.color = "red"}

            console.warn("vars.kundenSuchenVorherigeAnsicht: " + vars.kundenSuchenVorherigeAnsicht)
            buttonOK.visible = false
            if (vars.kundenSuchenVorherigeAnsicht == "frameLieferscheinSuchen") {
                buttonOK.visible = true
                buttonBack.text = "Zurück"
                buttonOK.text = "Kunde Wählen"
            }
            if (vars.kundenSuchenVorherigeAnsicht == "frameLieferscheinAnzeigen") {
                buttonOK.visible = true
                buttonBack.text = "Zurück"
                buttonOK.text = "Kunde Wählen"
            }
            if (vars.kundenSuchenVorherigeAnsicht == "frameLieferscheinAnzeigenKunde") {
                buttonOK.visible = true
                buttonBack.text = "Hauptmenu"
                buttonOK.text = "Zurück zum Lieferschein"
            }

            python.call("libs.send.get_client_total", [vars.serverIP, vars.secure_key, vars.kundeAnzeigenKundeID], function(answer) {
                kundenTotalVK.text = "Verkaufswert: " + answer[0]
                kundenTotalEK.text = "Einkaufswert: " + answer[1]
            })

            vars.busy = false

        })


    }

    function set_client() {
        vars.busy = true

        var client_dict = {
            "identification": vars.kundeAnzeigenKundeID,
            "name": textName.text,
            "plz": textPlz.text,
            "ort": textOrt.text,
            "land": textLand.text,
            "adresse": textAdresse.text,
            "tel1": textTel1.text,
            "tel2": textTel2.text,
            "tel3": textTel3.text,
            "email1": textMail1.text,
            "email2": textMail2.text,
            "email3": textMail3.text,
            "payment_deadline": textPaymentDeadline.text,
            "tva": textTVA.text,
            "sprache_de": switchSpracheDE.checked,
            "sprache_fr": switchSpracheFR.checked,
            "sprache_nl": switchSpracheNL.checked,
            "info": textInfo.text,
            "warning": textWarning.text,
            "dealer": checkBoxDealer.checked,
            "supplier": checkBoxSupplier.checked,
            "sepa": checkBoxSupplierSepa.checked,
            "disabled": checkBoxDisabled.checked
        }

        python.call("libs.send.set_client", [vars.serverIP, vars.secure_key, client_dict], function(sucess) {
            if (sucess == true) {                    infoBox.show = true; infoBox.text = "Kunde gespeichert"; infoBox.farbe="lightgreen"
                textInfo.color = "black"
                textWarning.color = "black"
                textTVA.color = "black"
                textTel1.color = "black"
                textTel2.color = "black"
                textTel3.color = "black"
                textMail1.color = "black"
                textMail2.color = "black"
                textMail3.color = "black"
                textPaymentDeadline.color = "black"
                textAdresse.color = "black"
                textOrt.color = "black"
                textPlz.color = "black"
                textName.color = "black"

                get_client()
            } else {
                infoBox.show = true; infoBox.text = "Fehler, Kunde wurde nicht gespeichert"; infoBox.farbe="orange"
            }

            vars.busy = false

        })

        python.call("libs.send.add_to_prediction", [vars.serverIP, vars.secure_key, "predict_client_place", textOrt.text, textAdresse.text], function() {})
        python.call("libs.send.add_to_prediction", [vars.serverIP, vars.secure_key, "predict_client_postcode", textPlz.text, textOrt.text], function() {})
        python.call("libs.send.add_to_prediction", [vars.serverIP, vars.secure_key, "predict_client_country", textLand.text, textPlz.text], function() {})

    }


    ButtonSelect {
        id: buttonBack
        text: "Zurück"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        onClicked: {
            console.warn("vars.kundenSuchenVorherigeAnsicht: " + vars.kundenSuchenVorherigeAnsicht)

            vars.lieferscheinAnzeigenSetKundeID = false

            if (vars.kundenSuchenVorherigeAnsicht == "frameKundenSuchen") {
                view.push(frameKundenSuchen)
            }

            if (vars.kundenSuchenVorherigeAnsicht == "frameLieferscheinAnzeigenKunde") {
                view.push(frameSelect)
            }
            if (vars.kundenSuchenVorherigeAnsicht == "frameLieferscheinAnzeigen") {
                view.push(frameKundenSuchen)
            }

            if (vars.kundenSuchenVorherigeAnsicht == "") {
                view.push(frameKundenSuchen)
            }
        }
    }

    ButtonSelect {
        id: buttonOK
        text: "Kunde Wählen"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        anchors.right: window.right

        onClicked: {
            console.warn("vars.kundenSuchenVorherigeAnsicht = " + vars.kundenSuchenVorherigeAnsicht)
            if (vars.kundenSuchenVorherigeAnsicht == "frameKundenSuchen") {
                view.push(frameKundenSuchen)
            }
            if (vars.kundenSuchenVorherigeAnsicht == "frameLieferscheinSuchen") {
                vars.lieferscheinSuchenTextName = vars.kundeAnzeigenKundeID
                vars.lieferscheinSuchenStart = true
                view.push(frameLieferscheinSuchen)
            }
            if (vars.kundenSuchenVorherigeAnsicht == "frameLieferscheinAnzeigen") {
                //vars.lieferscheinAnzeigenKundeID = vars.kundeAnzeigenKundeID
                python.call("libs.send.set_delivery_client", [vars.serverIP, vars.secure_key, vars.lieferscheinID, vars.kundeAnzeigenKundeID], function(sucess) {
                    if (sucess == true) {
                        view.push(frameLieferscheinAnzeigen)
                    }
                })

            }
            if (vars.kundenSuchenVorherigeAnsicht == "frameLieferscheinAnzeigenKunde") {
                view.push(frameLieferscheinAnzeigen)
            }
        }
    }

    ButtonSelect {
        id: buttonSpeichern
        text: "Speichern"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        y: kundenNummerTitle.y + kundenNummerTitle.height + 10
        x: mainWindow.width / 2 - width / 2
        onClicked: {
            vars.login_timeout = vars.login_max_timeout
            set_client()
        }
    }

    Label {
        id: kundenNummerTitle
        text: "Kundennummer: " + vars.kundeAnzeigenKundeID
        font.pixelSize: vars.fontTitle
        y: vars.pageTitleY
        x: mainWindow.width / 2 - width / 2
        onTextChanged: {
            get_client()
        }
    }

    Label {
        id: kundenTotalVK
        text: "Verkaufswert: ?"
        font.pixelSize: vars.fontTitle
        x: mainWindow.width - width
        anchors.top: buttonOK.bottom
    }
    Label {
        id: kundenTotalEK
        text: "Einkaufswert: ?"
        font.pixelSize: vars.fontTitle
        x: mainWindow.width - width
        anchors.top: kundenTotalVK.bottom
    }


    Rectangle {
        width: window.width
        anchors.top: buttonSpeichern.bottom
        anchors.bottom: parent.bottom
        color: vars.colorBackground

        ListView {
            id: liste
            focus: true
            highlightMoveDuration: 0

            clip: true

            ScrollBar.vertical: ScrollBar {
                active: true;
                width: vars.scrollbarWidth
            }

            z: parent.z
            anchors.fill: parent

            Label {
                id: label_erstellt
                text: "Erstellt am: ?"
                anchors.bottom: labelName.top
                anchors.bottomMargin: 10
                font.pixelSize: vars.fontText
            }
            Label {
                id: label_last_changed
                text: "Letzte änderung am: ?"
                anchors.bottom: labelName.top
                anchors.bottomMargin: 10
                anchors.left: label_erstellt.right
                anchors.leftMargin: 10
                font.pixelSize: vars.fontText
            }

            Label {
                id: labelName
                text: "Name: "
                y: vars.listItemHeight * 1
                font.pixelSize: vars.fontText
            }
            TextField {
                id: textName
                anchors.left: labelName.right
                anchors.right: liste.right
                y: labelName.y
                font.pixelSize: vars.fontText
                font.capitalization: Font.AllUppercase
                selectByMouse: true
                onTextChanged: {if (focus == true) {color = "red"}}
            }
            Label {
                id: labelPlz
                text: "PLZ : "
                y: vars.listItemHeight * 2
                font.pixelSize: vars.fontText
            }
            TextField {
                id: textPlz
                anchors.left: labelPlz.right
                y: labelPlz.y
                font.pixelSize: vars.fontText
                inputMethodHints: Qt.ImhDigitsOnly
                selectByMouse: true
                onTextChanged: {if (focus == true) {color = "red"}}
                onAccepted: {
                    if (textLand.text == "") {
                        python.call("libs.send.make_prediction", [vars.serverIP, vars.secure_key, "predict_client_country", textPlz.text], function(answer) {
                            ai_helper_country.open_popup("Kunde Land", answer)
                        })
                    }
                }
            }
            Label {
                id: labelOrt
                text: "Ort : "

                y: labelPlz.y
                anchors.left: textPlz.right
                font.pixelSize: vars.fontText
            }
            TextField {
                id: textOrt
                anchors.left: labelOrt.right
                anchors.right: liste.right
                y: labelPlz.y
                font.pixelSize: vars.fontText
                font.capitalization: Font.AllUppercase
                selectByMouse: true
                onTextChanged: {if (focus == true) {color = "red"}}
                onAccepted: {
                    if (textPlz.text == "") {
                        python.call("libs.send.make_prediction", [vars.serverIP, vars.secure_key, "predict_client_postcode", textOrt.text], function(answer) {
                            ai_helper_postcode.open_popup("Kunden Postleitzahl", answer)
                        })
                    }
                }
            }


            Label {
                id: labelLand
                text: "Land : "
                y: vars.listItemHeight * 3
                font.pixelSize: vars.fontText
            }

            SearchBox {
                id: textLand
                y: labelLand.y
                anchors.left: labelLand.right
                anchors.leftMargin: 10

                text: "BE"
                liste: ["AU", "BE", "DE", "FR", "LU", "NL", "IT", "IRL", "GB", "UK", "RO", "SVK", ""]
            }

            Label {
                id: labelAdresse
                text: "Adresse : "
                y: labelLand.y
                anchors.left: textLand.right
                font.pixelSize: vars.fontText
            }
            TextField {
                id: textAdresse
                anchors.left: labelAdresse.right
                anchors.right: labelTVA.left//liste.right
                y: labelLand.y
                font.pixelSize: vars.fontText
                font.capitalization: Font.AllUppercase
                selectByMouse: true
                onAccepted: {
                    if (textOrt.text == "") {
                        python.call("libs.send.make_prediction", [vars.serverIP, vars.secure_key, "predict_client_place", textAdresse.text], function(answer) {
                            ai_helper_place.open_popup("Kunden Ort", answer)
                        })
                    }
                }
                onTextChanged: {if (focus == true) {color = "red"}}

            }

            Label {
                id: labelTel
                text: "Tel : "
                y: vars.listItemHeight * 4
                font.pixelSize: vars.fontText
            }
            TextField {
                id: textTel1
                anchors.left: labelTel.right
                y: labelTel.y
                font.pixelSize: vars.fontText
                inputMethodHints: Qt.ImhDigitsOnly
                selectByMouse: true
                onTextChanged: {if (focus == true) {color = "red"}}
            }
            TextField {
                id: textTel2
                anchors.left: textTel1.right
                y: labelTel.y
                font.pixelSize: vars.fontText
                inputMethodHints: Qt.ImhDigitsOnly
                selectByMouse: true
                onTextChanged: {if (focus == true) {color = "red"}}
            }
            TextField {
                id: textTel3
                anchors.left: textTel2.right
                y: labelTel.y
                font.pixelSize: vars.fontText
                inputMethodHints: Qt.ImhDigitsOnly
                selectByMouse: true
                onTextChanged: {if (focus == true) {color = "red"}}
            }

            Label {
                id: labelMail
                text: "E-Mail : "
                y: vars.listItemHeight * 5
                font.pixelSize: vars.fontText
            }
            TextField {
                id: textMail1
                anchors.left: labelMail.right
                y: labelMail.y
                font.pixelSize: vars.fontText
                selectByMouse: true
                placeholderText: "kontakt email"
                onTextChanged: {if (focus == true) {color = "red"}}
            }
            TextField {
                id: textMail2
                anchors.left: textMail1.right
                y: labelMail.y
                font.pixelSize: vars.fontText
                selectByMouse: true
                placeholderText: "kontakt email für WooCommerce"
                onTextChanged: {if (focus == true) {color = "red"}}
            }
            TextField {
                id: textMail3
                anchors.left: textMail2.right
                y: labelMail.y
                font.pixelSize: vars.fontText
                selectByMouse: true
                placeholderText: "kontakt email für Rechnungen"
                onTextChanged: {if (focus == true) {color = "red"}}
            }

            Label {
                id: labelPaymentDeadline
                text: "Zahlungsfrist : "
                y: vars.listItemHeight * 5
                anchors.right: textPaymentDeadline.left
                font.pixelSize: vars.fontText
            }
            TextField {
                id: textPaymentDeadline
                anchors.right: labelTVA.left
                y: labelMail.y
                font.pixelSize: vars.fontText
                selectByMouse: true
                onTextChanged: {if (focus == true) {color = "red"}}
            }

            Label {
                id: labelTVA
                text: "TVA : "
                y: vars.listItemHeight * 4
                anchors.right: textTVA.left
                font.pixelSize: vars.fontText
            }
            TextField {
                id: textTVA
                anchors.right: liste.right
                y: labelTVA.y
                font.pixelSize: vars.fontText
                font.capitalization: Font.AllUppercase
                selectByMouse: true
                onTextChanged: {
                    if (focus == true) {color = "red"}
                    if(text == "") {} else {
                        text = text.toUpperCase()
                    }
                }
            }
            Label {
                id: label_tva_status
                text: "..."
                anchors.top: textTVA.bottom
                anchors.topMargin: 10
                anchors.right: parent.right
                height: textTVA.height
                width: 200
            }

            Label {
                id: labelSpracheDE
                text: "DE : "
                y: vars.listItemHeight * 6
                font.pixelSize: vars.fontText
            }
            Switch {
                id: switchSpracheDE
                y: labelSpracheDE.y
                anchors.left: labelSpracheDE.right
            }
            Label {
                id: labelSpracheFR
                text: "FR : "
                y: labelSpracheDE.y
                anchors.left: switchSpracheDE.right
                font.pixelSize: vars.fontText
            }
            Switch {
                id: switchSpracheFR
                y: labelSpracheDE.y
                anchors.left: labelSpracheFR.right
            }
            Label {
                id: labelSpracheNL
                text: "NL : "
                y: labelSpracheDE.y
                anchors.left: switchSpracheFR.right
                font.pixelSize: vars.fontText
            }
            Switch {
                id: switchSpracheNL
                y: labelSpracheDE.y
                anchors.left: labelSpracheNL.right
            }

            Label {
                id: label_rappel
                text: "Erinnerungen / Durchschnitt: ?"
                anchors.left: switchSpracheNL.right
                anchors.leftMargin: 10
                y: labelSpracheDE.y
            }

            Label {
                id: labelInfo
                text: "Info : "
                y: vars.listItemHeight * 7
                font.pixelSize: vars.fontText
            }
            TextField {
                id: textInfo
                anchors.top: labelInfo.bottom
                anchors.left: liste.left
                width: parent.width / 2 - labelInfo.width
                font.pixelSize: vars.fontText
                selectByMouse: true
                onFocusChanged: {
                    if (focus == true) {
                        echoMode = TextInput.Normal
                    } else {
                        echoMode = TextInput.Password
                    }
                }
            }
            Label {
                id: labelWarning
                text: "Warnung : "
                y: vars.listItemHeight * 7
                x: textWarning.x
                font.pixelSize: vars.fontText
            }
            TextField {
                id: textWarning
                anchors.top: labelWarning.bottom
                anchors.left: textInfo.right
                anchors.right: liste.right
                font.pixelSize: vars.fontText
                selectByMouse: true
                enabled: false
                onFocusChanged: {
                    if (focus == true) {
                        echoMode = TextInput.Normal
                    } else {
                        echoMode = TextInput.Password
                    }
                }
            }


            CheckBox {
                id: checkBoxDisabled
                text: "Nicht mehr in benutzung"
                anchors.top: textInfo.bottom
                anchors.topMargin: 10
                font.pixelSize: vars.fontText
            }

            CheckBox {
                id: checkBoxDealer
                text: "Zwichenhändler"
                anchors.top: textInfo.bottom
                anchors.topMargin: 10
                anchors.left: checkBoxDisabled.right
                anchors.leftMargin: 10
                font.pixelSize: vars.fontText
            }

            CheckBox {
                id: checkBoxSupplier
                text: "Lieferant"
                anchors.top: textInfo.bottom
                anchors.topMargin: 10
                anchors.left: checkBoxDealer.right
                anchors.leftMargin: 10
                font.pixelSize: vars.fontText
            }

            CheckBox {
                id: checkBoxSupplierSepa
                text: "Sepa Lastschrift"
                anchors.top: textInfo.bottom
                anchors.topMargin: 10
                anchors.left: checkBoxSupplier.right
                anchors.leftMargin: 10
                font.pixelSize: vars.fontText
                visible: checkBoxSupplier.checked
            }

        }
    }

    ButtonSelect {
        id: buttonLieferscheineAnzeigen
        text: "Lieferscheine Anzeigen"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        anchors.bottom: window.bottom
        onClicked: {
            view.push(frameLieferscheinSuchen)
            vars.lieferscheinSuchenTextIdentification = ""
            vars.lieferscheinSuchenTextName = vars.kundeAnzeigenKundeID
            vars.lieferscheinSuchenFertig = 2
            vars.lieferscheinSuchenCheckEigene = false
            vars.lieferscheinSuchenStart = true
        }
    }

    ButtonSelect {
        id: buttonNeuesAngebot
        text: "Neues Angebot"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        anchors.bottom: window.bottom
        anchors.left: buttonLieferscheineAnzeigen.right
        anchors.leftMargin: 10
        onClicked: {
            python.call("libs.send.create_delivery_angebot", [vars.serverIP, vars.secure_key], function(delivery) {
                python.call("libs.send.set_delivery_client", [vars.serverIP, vars.secure_key, delivery["identification"], vars.kundeAnzeigenKundeID], function(sucess) {
                    if(sucess == true) {
                        vars.lieferscheinID = delivery["identification"]
                        view.push(frameLieferscheinAnzeigen)
                    }
                })
            })
        }
    }

    ButtonSelect {
        id: buttonNeuerLieferschein
        text: "Neuer Lieferschein"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        anchors.bottom: window.bottom
        anchors.left: buttonNeuesAngebot.right
        anchors.leftMargin: 10
        onClicked: {
            python.call("libs.send.create_delivery", [vars.serverIP, vars.secure_key], function(delivery) {
                python.call("libs.send.set_delivery_client", [vars.serverIP, vars.secure_key, delivery["identification"], vars.kundeAnzeigenKundeID], function(sucess) {
                    if(sucess == true) {
                        vars.lieferscheinID = delivery["identification"]
                        view.push(frameLieferscheinAnzeigen)
                    }
                })
            })
        }
    }

    InfoLabel {}
    function setInfoLabelText(text) {vars.infoLabelText = text}
    Busy {}
    InfoBox {id: infoBox}

    AIHelperPopup {
        id: ai_helper_place
        onAccepted: {
            textOrt.text = text
        }
    }
    AIHelperPopup {
        id: ai_helper_postcode
        onAccepted: {
            textPlz.text = text
        }
    }
    AIHelperPopup {
        id: ai_helper_country
        onAccepted: {
            textLand.text = text
        }
    }

}


