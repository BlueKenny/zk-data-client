import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.0
import QtQuick.Window 2.0

Rectangle {
    id: window
    height: parent.height; width: mainWindow.width

    color: vars.colorBackground

    property int count: 0
    property int search: 0

    function startBeltSearch() {
        search += 1
        count = 0

        beltModel.clear();

        answerBeltSearch(search, count)
        //count = 0

        //python.call("libs.send.search_belt", [vars.serverIP, vars.secure_key, slider_thick.first.value, slider_thick.second.value, slider_length.first.value, slider_length.second.value], function(ids){
         //   var items = []
        //    count = ids.length

          //  console.warn("ids: " + ids)

            //for (var i=0; i<ids.length; i++) {
            //    console.warn("ids[" + i + "]: " + ids[i])
            //    python.call("libs.send.GetArt", [vars.serverIP, vars.secure_key, ids[i]], function(dict) {
            //        console.warn("dict: " + dict["name_de"])

            //        python.call("libs.BlueFunc.getLastChar", [dict["groesse"]], function(last_char) {
            //            dict["type"] = last_char.toUpperCase()
            //            beltModel.append(dict)
            //        })
            //    })
            //}

            //answerBeltSearch(items)
        //})
    }

    function answerBeltSearch(f_search, f_count) {
        python.call("libs.send.search_belt", [vars.serverIP, vars.secure_key, slider_thick.first.value, slider_thick.second.value, slider_length.first.value, slider_length.second.value, count], function(item){
            if (f_search == search) {
                if (item["identification"] == "-1") {
                    beltModel.append({"identification": "", "anzahl": "", "groesse": "Keine weiteren ergebnise"})
                } else {
                    if (checkbox_show_only_stock.checked == true) {
                        if(item["anzahl"] == "0") {} else {
                            beltModel.append(item)
                        }
                    } else {
                        beltModel.append(item)
                    }
                    count = f_count + 1
                    answerBeltSearch(f_search, count)
                }
            }
        })
    }

    ButtonSelect {
        id: buttonBack
        text: "Zurück"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        onClicked: {
            view.push(frameSelect)
        }
    }

    CheckBox {
        id: checkbox_show_only_stock
        text: "Nur Riemen auf Lager anzeigen"
        anchors.right: parent.right
        anchors.rightMargin: 10
        checked: true
        onCheckedChanged: {
            startBeltSearch()
        }
    }

    Label {
        id: label_min_thick
        text: slider_thick.first.value
        anchors.right: label_thick_to.left
        anchors.top: slider_thick.bottom
        font.pixelSize: 20
        onTextChanged: startBeltSearch()
    }
    Label {
        id: label_thick_to
        text: " bis "
        font.pixelSize: 20
        anchors.top: slider_thick.bottom
        x: parent.width / 2 - width / 2
    }
    Label {
        id: label_max_thick
        text: slider_thick.second.value
        anchors.left: label_thick_to.right
        anchors.top: slider_thick.bottom
        font.pixelSize: 20
        onTextChanged: startBeltSearch()
    }
    RangeSlider {
        id: slider_thick
        anchors.top: buttonBack.bottom
        anchors.topMargin: 10
        from: 5
        to: 30
        first.value: 5
        second.value: 30
        stepSize: 1
        snapMode: RangeSlider.SnapAlways
        anchors.left: parent.left
        anchors.right: parent.right
    }

    Label {
        id: label_min_length
        text: slider_length.first.value.toPrecision(4)
        anchors.right: label_length_to.left
        anchors.top: slider_length.bottom
        font.pixelSize: 20
        onTextChanged: startBeltSearch()
    }
    Label {
        id: label_length_to
        text: " bis "
        font.pixelSize: 20
        anchors.top: slider_length.bottom
        x: parent.width / 2 - width / 2
    }
    Label {
        id: label_max_length
        text: slider_length.second.value.toPrecision(4)
        anchors.left: label_length_to.right
        anchors.top: slider_length.bottom
        font.pixelSize: 20
        onTextChanged: startBeltSearch()
    }
    RangeSlider {
        id: slider_length
        from: 500
        to: 2500
        first.value: 500
        second.value: 2500
        stepSize: 50
        snapMode: RangeSlider.SnapAlways
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: label_thick_to.bottom
    }


    Label {
        id: label_count
        text: beltModel.count + " ergebnise"
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.top: label_length_to.bottom
        font.pixelSize: 20
    }
    ListView {
        id: liste_belt
        anchors.top: label_count.bottom
        anchors.topMargin: 10
        anchors.bottom: parent.bottom
        width: parent.width

        highlight: Rectangle { opacity: 0.1; color: "blue"; width: 500; height: 100; visible: true}

        clip: true

        ListModel {id: beltModel}

        Component {
            id: beltDelegate
            Rectangle {
                height: window.height / 10
                width: window.width
                //border.width: 1

                color: "transparent"

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.warn(index)
                        liste_belt.currentIndex = index
                    }

                    onDoubleClicked: {
                        python.call("os.system", ["/etc/zk-data/qml/ChangeStock.py " + identification], function() {

                        })
                    }

                }

                Label {
                    id: label_anzahl
                    text: anzahl + "x "
                    font.pixelSize: 20
                    y: parent.height / 2 - height / 2
                    //anchors.left: window.left
                    //anchors.leftMargin: 10
                    x: 10
                }
                Label {
                    id: label_identification
                    text: identification + "  "
                    font.pixelSize: 20
                    y: label_anzahl.y
                    anchors.left: label_anzahl.right
                    anchors.leftMargin: 10
                }
                Label {
                    text: name_de
                    font.pixelSize: 20
                    y: label_anzahl.y
                    anchors.left: label_identification.right
                    anchors.leftMargin: 10

                }
                Label {
                    id: label_groesse
                    text: groesse.toUpperCase().replace("BELT", "").replace("X", " x ")
                    x: parent.width / 2 - width / 2
                    y: label_anzahl.y
                    font.pixelSize: 20
                }
                Label {
                    id: label_ort
                    text: ort
                    anchors.left: image.right
                    anchors.right: label_lieferant.left
                    anchors.leftMargin: 20
                    font.pixelSize: 20
                    y: label_anzahl.y
                    anchors.rightMargin: 10
                }
                Label {
                    id: label_lieferant
                    text: lieferant
                    anchors.right: parent.right
                    font.pixelSize: 20
                    y: label_anzahl.y
                    anchors.rightMargin: 10
                }
                Image {
                    id: image
                    source: "DATA/belt_icon/" + type + ".png"
                    width: 40
                    height: 40
                    anchors.left: label_groesse.right
                    anchors.leftMargin: 10
                    y: label_groesse.y - height / 4
                }
            }
        }

        model: beltModel
        delegate: beltDelegate
    }
    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.BlueFunc', function () {});
            importModule('os', function () {});

            startBeltSearch()
            //setHandler("busy", busy);
            //call('libs.BlueFunc.getData', ["PrinterIP"], function (text) {vars.druckerIP = text});

        }
    }
}
