import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.0
//import QtQuick.Controls.Styles 1.2

Rectangle {
    //height: mainWindow.height
    width: mainWindow.width

    color: vars.colorBackground

    Keys.onPressed: {
        if ( event.key === Qt.Key_F1 ) {
            vars.lieferscheinSuchen_Angebote = false
            vars.lieferscheinSuchen_Bestellungen = false
            key_F1()
        }
        if ( event.key === Qt.Key_F2 ) {
            //if (vars.userData["permission_invoice"] > 0) {
             view.push(frameRechnungSuchen)
            //}
        }
        if ( event.key === Qt.Key_F3 ) {
            //if (vars.userData["permission_invoice_e"] > 0) {
            view.push(frameRechnungESuchen)
            //}
        }
        if ( event.key === Qt.Key_Up ) {
            listLieferscheinSuchen.decrementCurrentIndex()
            vars.lieferscheinSuchen_Last_Selection = listLieferscheinSuchen.currentIndex
        }
        if ( event.key === Qt.Key_Down ) {
            listLieferscheinSuchen.incrementCurrentIndex()
            vars.lieferscheinSuchen_Last_Selection = listLieferscheinSuchen.currentIndex
        }
        if ( event.key === Qt.Key_Return ) {
            variable.searchIndex += 1

            vars.lieferscheinID = contactModel.get(listLieferscheinSuchen.currentIndex).identification
            vars.lieferscheinDatum = contactModel.get(listLieferscheinSuchen.currentIndex).datum
            view.push(frameLieferscheinAnzeigen)
        }
    }

    Item {
        id: variable
        //property double last_change: 5000000000.000000
        property bool sucheRunning: false
        property int searchIndex: 0
        property int searchProcess: 0
        property int last_selection: 1
        onLast_selectionChanged: {console.warn("vars.lieferscheinSuchen_Last_Selection changed: " + vars.lieferscheinSuchen_Last_Selection)}
    }

    function set_names() {
        if (vars.lieferscheinSuchen_Angebote) {
            labelLieferscheinSuchenTitle.text = "Angebote Suchen"
            labelLieferscheinnummer.text = "Angebot n°"
            checkLieferscheinSuchenEigene.text = "Nur die eigenen Angebote anzeigen"
            textLieferscheinSuchenRechnung.text = ""
            textLieferscheinSuchenRechnung.visible = false
            labelRechnungnummer.visible = false
        } else {
            if (vars.lieferscheinSuchen_Bestellungen) {
                labelLieferscheinSuchenTitle.text = "Bestellungen Suchen"
                labelLieferscheinnummer.text = "Bestellung n°"
                checkLieferscheinSuchenEigene.text = "Nur die eigenen Bestellungen anzeigen"
                textLieferscheinSuchenRechnung.text = ""
                textLieferscheinSuchenRechnung.visible = false
                labelRechnungnummer.visible = false
            } else {
                labelLieferscheinSuchenTitle.text = "Lieferscheine Suchen"
                labelLieferscheinnummer.text = "Lieferschein n°"
                checkLieferscheinSuchenEigene.text = "Nur die eigenen Lieferscheine anzeigen"
                textLieferscheinSuchenRechnung.visible = true
                labelRechnungnummer.visible = true
            }
        }

    }

    function suchen() {
        set_names()

        vars.busy = true
        vars.login_timeout = vars.login_max_timeout

        //variable.searchIndex = 0
        variable.searchProcess += 1
        var this_process = variable.searchProcess

        contactModel.clear();
        //variable.last_change = 99999999999999
        //sucheEnd(variable.searchIndex, variable.searchProcess)

        python.call("libs.send.search_delivery", [vars.serverIP, vars.secure_key, {"identification":vars.lieferscheinSuchenTextIdentification, "kunde_id":vars.lieferscheinSuchenTextName, "fertig":vars.lieferscheinSuchenFertig, "eigene":vars.lieferscheinSuchenCheckEigene, "rechnung": vars.lieferscheinSuchenTextRechnung, "angebote": vars.lieferscheinSuchen_Angebote, "bestellung": vars.lieferscheinSuchen_Bestellungen, "inhalt": vars.lieferscheinSuchenInhalt}], function(liste) {

            vars.busy = false            

            for (var i=0; i< liste.length; i++) {
                if (variable.searchProcess == this_process) {
                    contactModel.append({"identification": liste[i],
                                            "dcolor": "",
                                            "angebot": "",
                                            "total": 0.0,
                                            "info": "",
                                            "kunde_name": "",
                                            "maschine": "",
                                            "datum": "",
                                            "datum_todo": "",
                                            "rechnung": ""
                                        })
                }
            }

        })

    }

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    Label {
        id: labelLieferscheinSuchenTitle
        text: "???"
        font.pixelSize: vars.fontTitle
        x: mainWindow.width / 2 - width / 2
        y: vars.pageTitleY//buttonBack.height / 2 + buttonBack.y - height / 2
    }

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenü"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        tooltip_text: "Zurück zum Hauptmenu"
        onClicked: {
            variable.searchIndex += 1
            vars.kundenSuchenVorherigeAnsicht = ""
            view.push(frameSelect)
        }
    }

    Label {
        id: labelLieferscheinnummer
        text: "???"
        font.pixelSize: vars.fontText
        //x: mainWindow.width / 3 - width / 2
        anchors.right: textLieferscheinSuchenIdentification.left
        anchors.rightMargin: 20
        y: vars.headerPosY
    }
    TextField {
        id: textLieferscheinSuchenIdentification
        focus: true
        //text: vars.lieferscheinSuchenTextIdentification
        width: mainWindow.width / 10
        //x: mainWindow.width / 3 * 2 - width / 2
        x: mainWindow.width / 2 - width - 20
        y: labelLieferscheinnummer.y - height / 2
        font.pixelSize: vars.fontText
        selectByMouse: true
        inputMethodHints: Qt.ImhDigitsOnly

        onTextChanged: {
            if (focus == true) {
                vars.lieferscheinSuchenTextIdentification = text
                vars.lieferscheinSuchenFertig = 2
                sliderLieferscheinSuchenFertig.value = 2
                vars.lieferscheinSuchenCheckEigene = false
                checkLieferscheinSuchenEigene.checked = false
                suchen()
            }
        }
    }

    Label {
        id: labelRechnungnummer
        text: "Rechnung n°"
        font.pixelSize: vars.fontText
        x: mainWindow.width / 2 + 20
        y: vars.headerPosY
    }
    TextField {
        id: textLieferscheinSuchenRechnung
        //text: vars.lieferscheinSuchenTextRechnung
        width: mainWindow.width / 10
        anchors.left: labelRechnungnummer.right
        anchors.leftMargin: 20
        y: labelLieferscheinnummer.y - height / 2
        font.pixelSize: vars.fontText
        selectByMouse: true
        inputMethodHints: Qt.ImhDigitsOnly

        onTextChanged: {
            if (focus == true) {
                vars.lieferscheinSuchenTextRechnung = text                

                vars.lieferscheinSuchenFertig = 2
                sliderLieferscheinSuchenFertig.value = 2
                vars.lieferscheinSuchenCheckEigene = false
                checkLieferscheinSuchenEigene.checked = false

                suchen()
            }
        }
    }


    Label {
        id: labelKunde
        text: "Kunde"
        font.pixelSize: vars.fontText//labelLieferscheinSuchenTitle.font.pixelSize * 0.8
        anchors.right: textLieferscheinSuchenName.left
        anchors.rightMargin: 20
        y: vars.headerPosY + vars.headerPosSpacing//mainWindow.height / 20 * 2 + buttonBack.height
    }
    TextField {
        id: textLieferscheinSuchenName
        inputMethodHints: Qt.ImhNoPredictiveText
        //text: vars.lieferscheinSuchenTextName
        width: vars.isPhone ? mainWindow.width / 5 : mainWindow.width / 10
        x: textLieferscheinSuchenIdentification.x
        y: labelKunde.y - height / 2//vars.headerPosY + vars.headerPosSpacing//mainWindow.height / 20 * 2 + buttonBack.height
        font.pixelSize: vars.fontText//labelLieferscheinSuchenTitle.font.pixelSize * 0.8
        selectByMouse: true

        onTextChanged: {
            if (focus == true) {
                vars.lieferscheinSuchenTextName = text               

                vars.lieferscheinSuchenFertig = 2
                sliderLieferscheinSuchenFertig.value = 2
                vars.lieferscheinSuchenCheckEigene = false
                checkLieferscheinSuchenEigene.checked = false

                suchen()
            }

        }
    }
    ButtonSelect {
        id: buttonLieferscheinSucheKunden
        text: "Suchen"
        x: textLieferscheinSuchenName.x + textLieferscheinSuchenName.width
        y: textLieferscheinSuchenName.y
        height: textLieferscheinSuchenName.height
        width: textLieferscheinSuchenName.width / 2
        //font.pixelSize: vars.fontText
        onClicked: {
            vars.kundenSuchenVorherigeAnsicht = "frameLieferscheinSuchen"
            view.push(frameKundenSuchen)
        }
    }
    ButtonSelect {
        id: buttonLieferscheinSucheKundenClear
        text: "X"
        x: buttonLieferscheinSucheKunden.x + buttonLieferscheinSucheKunden.width
        y: textLieferscheinSuchenName.y
        height: textLieferscheinSuchenName.height
        width: textLieferscheinSuchenName.width / 2
        //font.pixelSize: vars.fontText
        onClicked: {
            vars.lieferscheinSuchenTextName = ""
            textLieferscheinSuchenName.text = ""
            suchen()
        }
    }

    Label {
        id: labelFertige
        text: "Nicht Fertig | Fertig | Alle"
        font.pixelSize: vars.fontText//labelLieferscheinSuchenTitle.font.pixelSize * 0.8
        //x: vars.isPhone ? mainWindow.width / 3 - width / 2 : mainWindow.width / 3 - width / 2
        anchors.right: sliderLieferscheinSuchenFertig.left
        anchors.rightMargin: 20
        y: mainWindow.height / 20 * 3 + buttonBack.height                


        MouseArea {
            anchors.fill: parent
            onClicked: { if (checkLieferscheinSuchenFertige.checked == true) {checkLieferscheinSuchenFertige.checked = false} else {checkLieferscheinSuchenFertige.checked = true} }
        }
    }


    Slider {
        id: sliderLieferscheinSuchenFertig
        anchors.left: textLieferscheinSuchenName.left
        anchors.leftMargin: 20
        y: mainWindow.height / 20 * 3 + buttonBack.height
        snapMode: Slider.SnapAlways
        from: 0.0
        to: 2.0
        stepSize: 1.0
        //value: vars.lieferscheinSuchenFertig
        onValueChanged: {
            if (value == 0) {labelFertigeSelection.text = "Nicht Fertig"}
            if (value == 1) {labelFertigeSelection.text = "Fertig"}
            if (value == 2) {labelFertigeSelection.text = "Alle"}

            if (focus == true) {
                vars.lieferscheinSuchenFertig = sliderLieferscheinSuchenFertig.value

                suchen()
            }

        }
    }
    Label {
        id: labelFertigeSelection
        text: "Nicht Fertig"
        anchors.left: sliderLieferscheinSuchenFertig.right
        y: sliderLieferscheinSuchenFertig.y
        font.pixelSize: vars.fontText
    }

    CheckBox {
        id: checkLieferscheinSuchenEigene
        text: "???"
        font.pixelSize: vars.fontText

        x: mainWindow.width / 2 - width - 10
        y: mainWindow.height / 20 * 4 + buttonBack.height
        checked: vars.lieferscheinSuchenCheckEigene
        onCheckedChanged: {
            if (focus == true) {
                vars.lieferscheinSuchenCheckEigene = checkLieferscheinSuchenEigene.checked
                suchen()
            }
        }
    }

    Label {
        id: labelInhalt
        text: "Inhalt"
        x: mainWindow.width / 2 + 10
        y: mainWindow.height / 20 * 4 + buttonBack.height
        font.pixelSize: vars.fontText
    }

    TextField {
        id: textInhalt
        text: ""
        font.pixelSize: vars.fontText

        anchors.left: labelInhalt.right
        anchors.rightMargin: 10
        y: mainWindow.height / 20 * 4 + buttonBack.height

        selectByMouse: true

        onTextChanged: {
            if( focus == true) {
                vars.lieferscheinSuchenInhalt = textInhalt.text
                suchen()
            }
        }

    }

    ButtonSelect {
        id: buttonLieferscheinSuchenNeu
        text: "Neu"
        x: mainWindow.width / 2 - width / 2
        y: mainWindow.height / 20 * 5 + buttonBack.height
        z: 2
        height: vars.backButtonHeight//mainWindow.height / 15
        width: vars.backButtonWidth//mainWindow.width / 5
        //font.pixelSize: vars.fontText

        onClicked: {
            variable.searchIndex += 1
            if (vars.lieferscheinSuchen_Angebote == true) {
                python.call("libs.send.create_delivery_angebot", [vars.serverIP, vars.secure_key], function(dict) {
                    vars.lieferscheinID = dict["identification"]
                    view.push(frameLieferscheinAnzeigen)
                })
            } else {
                if (vars.lieferscheinSuchen_Bestellungen == true) {
                    python.call("libs.send.create_delivery_bestellung", [vars.serverIP, vars.secure_key], function(dict) {
                        vars.lieferscheinID = dict["identification"]
                        view.push(frameLieferscheinAnzeigen)
                    })
                } else {
                    python.call("libs.send.create_delivery", [vars.serverIP, vars.secure_key], function(dict) {
                        vars.lieferscheinID = dict["identification"]
                        view.push(frameLieferscheinAnzeigen)
                    })
                }
            }
        }
    }

    Label {
        id: label_answer_count
        anchors.right: parent.right
        anchors.bottom: listLieferscheinSuchen.top
        font.pixelSize: vars.fontText
        text: "Ergebnisse: " + contactModel.count
    }

    ListView {
        id: listLieferscheinSuchen
        //y: mainWindow.height / 20 * 7 + buttonBack.height
        z: buttonLieferscheinSuchenNeu.z - 1
        focus: true
        highlightMoveDuration: 0

        highlight: Rectangle {visible: true ; color: "lightsteelblue"; width: mainWindow.width}

        clip: true

        anchors.topMargin: 20
        anchors.top: buttonLieferscheinSuchenNeu.bottom
        //height: (mainWindow.height / 20) * 13
        anchors.bottom: parent.bottom
        //width: mainWindow.width
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 10
        anchors.rightMargin: 10

        ScrollBar.vertical: ScrollBar {
            id: scrollbar
            active: true;
            //policy: ScrollBar.AlwaysOn
            width: vars.scrollbarWidth
        }

        ListModel {
            id: contactModel
        }
        Component {
            id: contactDelegate
            Item {
                id: itemListe
                width: listLieferscheinSuchen.width

                height: vars.listItemHeight

                Rectangle {
                    id: rectangleLieferscheinSucheListeEintrag
                    width: parent.width
                    height: parent.height
                    border.width: 1
                    color: "transparent"


                    Label {
                        id: lieferscheinSucheListeEintrag_datum
                        text: datum
                        font.pixelSize: vars.fontText
                        anchors.margins: 10
                        anchors.top: rectangleLieferscheinSucheListeEintrag.top
                        color: dcolor
                    }
                    Label {
                        id: lieferscheinSucheListeEintrag_identification
                        text: identification
                        font.pixelSize: vars.isPhone ? vars.fontText : vars.fontTitle
                        anchors.margins: 10
                        anchors.bottom: rectangleLieferscheinSucheListeEintrag.bottom
                        color: dcolor
                    }
                    Label {
                        id: lieferscheinSucheListeEintrag_angebot
                        text: angebot
                        font.pixelSize: vars.fontTitle + 10
                        anchors.margins: 10
                        x: (lieferscheinSucheListeEintrag_name.x - lieferscheinSucheListeEintrag_datum.width) / 2
                        y: parent.height / 2 - height / 2
                        color: dcolor
                    }
                    Label {
                        id: lieferscheinSucheListeEintrag_rechnung
                        text: rechnung
                        font.pixelSize: vars.isPhone ? vars.fontText : vars.fontTitle
                        anchors.margins: 10
                        anchors.leftMargin: 100
                        anchors.top: rectangleLieferscheinSucheListeEintrag.top
                        anchors.left: lieferscheinSucheListeEintrag_datum.right
                        color: dcolor
                    }
                    Label {
                        id: lieferscheinSucheListeEintrag_datum_todo
                        text: datum_todo
                        font.pixelSize: vars.fontText
                        anchors.margins: 10
                        anchors.top: rectangleLieferscheinSucheListeEintrag.top
                        anchors.right: rectangleLieferscheinSucheListeEintrag.right
                        color: dcolor
                    }
                    Label {
                        id: lieferscheinSucheListeEintrag_maschine
                        text: maschine
                        font.pixelSize: vars.fontText
                        anchors.margins: 10
                        anchors.bottom: rectangleLieferscheinSucheListeEintrag.bottom
                        anchors.right: rectangleLieferscheinSucheListeEintrag.right
                        color: dcolor
                    }

                    Label {
                        id: lieferscheinSucheListeEintrag_name
                        text: kunde_name
                        font.pixelSize: vars.fontTitle
                        anchors.margins: 3
                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        color: dcolor
                    }
                    Label {
                        id: lieferscheinSucheListeEintrag_info
                        text: info.replace("\n", " | ")
                        font.pixelSize: vars.fontTitle
                        anchors.margins: 3
                        anchors.bottom: parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        maximumLineCount: 1
                    }
                    Label {
                        id: lieferscheinSucheListeEintrag_total
                        text: total + " €"
                        font.pixelSize: vars.fontTitle
                        anchors.margins: 10
                        anchors.rightMargin: 100
                        anchors.right: lieferscheinSucheListeEintrag_datum_todo.left
                        anchors.verticalCenter: parent.verticalCenter
                        color: dcolor
                        visible: true//mouseAreaLieferscheinSucheListeEintrag.containsMouse
                    }

                    MouseArea {
                        id: mouseAreaLieferscheinSucheListeEintrag
                        anchors.fill: rectangleLieferscheinSucheListeEintrag

                        hoverEnabled: true

                        acceptedButtons: Qt.LeftButton | Qt.RightButton

                        onEntered: {
                            lieferscheinSucheListeEintrag_name.font.pixelSize = vars.fontTitle + 5
                            lieferscheinSucheListeEintrag_info.font.pixelSize = vars.fontTitle + 2
                        }
                        onExited: {
                            lieferscheinSucheListeEintrag_name.font.pixelSize = vars.fontTitle
                            lieferscheinSucheListeEintrag_info.font.pixelSize = vars.fontTitle
                        }

                        onClicked: {
                            if(mouse.button & Qt.RightButton) {
                                vars.busy = true

                                python.call("libs.send.get_delivery_pdf", [vars.serverIP, vars.secure_key, identification], function(pdf_path){
                                var delivery_prefix_letter = "L"
                                    if (angebot == "A") {
                                        delivery_prefix_letter = "A"
                                    } else {
                                        if (angebot == "B") {
                                            delivery_prefix_letter = "B"
                                        }
                                    }

                                    python.call("os.system", ["wget http://" + vars.serverIP + ":51515" + pdf_path + " -O /tmp/" + vars.serverNAME + "/" + delivery_prefix_letter + identification + ".pdf && evince /tmp/" + vars.serverNAME + "/" + delivery_prefix_letter + identification + ".pdf &"], function() {
                                        vars.busy = false
                                    })
                                })


                            } else {
                                vars.lieferscheinSuchen_Last_Selection = index

                                python.call("libs.send.get_delivery_with_log", [vars.serverIP, vars.secure_key, identification], function() {})

                                vars.lieferscheinID = identification
                                vars.lieferscheinDatum = datum
                                view.push(frameLieferscheinAnzeigen)
                                // fix to resize content
                                mainWindow.height = mainWindow.height - 1
                                mainWindow.width = mainWindow.width - 1

                            }
                        }

                    }

                    Component.onCompleted: {
                        python.call("libs.send.get_delivery", [vars.serverIP, vars.secure_key, identification], function(item) {
                            total = item["total"]
                            info = item["info"]
                            kunde_name = item["kunde_name"]
                            maschine = item["maschine"]
                            datum = item["datum"]
                            datum_todo = item["datum_todo"]
                            rechnung = item["rechnung"]

                            if (item["fertig"] == true) {
                                dcolor = "blue"
                            } else {
                                if (item["total"] == 0) {
                                    dcolor = "red"
                                } else {
                                    dcolor = "black"
                                }
                            }

                            if (vars.lieferscheinSuchen_Angebote == true) {
                                if (item["angebot"] == true) {
                                    angebot = "A"
                                }
                                if (item["bestellung"] == true) {
                                    angebot = "B"
                                }

                            } else {
                                if (vars.lieferscheinSuchen_Bestellungen == true) {
                                    if (item["angebot"] == true) {
                                        angebot = "A"
                                    }
                                    if (item["bestellung"] == true) {
                                        angebot = "B"
                                    }
                                } else {
                                    if (item["angebot"] == true) {
                                        angebot = "A"
                                        dcolor = "yellow"
                                    } else {
                                        if (item["bestellung"] == true) {
                                            angebot = "B"
                                            dcolor = "yellow"
                                        } else {
                                            angebot = ""
                                        }
                                    }

                                }
                            }

                        })



                    }
                }
            }

        }

        model: contactModel
        delegate: contactDelegate
    }

    InfoLabel {}
    function setInfoLabelText(text) {vars.infoLabelText = text}
    Busy {}
    function busy(status) {vars.busy = status}

    Component.onCompleted: {
        python.setHandler("busy", busy);
        python.setHandler("setInfoLabelText", setInfoLabelText);

        textLieferscheinSuchenIdentification.text = vars.lieferscheinSuchenTextIdentification
        textLieferscheinSuchenRechnung.text = vars.lieferscheinSuchenTextRechnung
        textLieferscheinSuchenName.text = vars.lieferscheinSuchenTextName
        sliderLieferscheinSuchenFertig.value = vars.lieferscheinSuchenFertig
        checkLieferscheinSuchenEigene.checked = vars.lieferscheinSuchenCheckEigene
        textInhalt.text = vars.lieferscheinSuchenInhalt

        suchen()
    }
    /*
    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('LieferscheinSuchen', function () {});

            setHandler("busy", busy);

            setHandler("setInfoLabelText", setInfoLabelText);

        }
    }
    */
}
