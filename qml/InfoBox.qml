import QtQuick 2.7
import QtQuick.Controls 2.0

Rectangle {
    property string text
    property string farbe
    property bool show: false
    property int size: 100
    id: componentRoot
    y: mainWindow.height / 2 - height / 2
    x: mainWindow.width / 2 - width / 2
    height: mainWindow.height / 100 * size
    width: mainWindow.width / 100 * size
    visible: componentRoot.show
    radius: 50
    z: parent.z + 10
    color: farbe

    onVisibleChanged: {
        size = 100
        animationClose.running = true
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            size = 100
            animationClose.running = false
            animationClose.running = true
        }
    }

    Label {
        x: componentRoot.width / 2 - width / 2
        y: componentRoot.height / 2 - height / 2
        font.pixelSize: vars.fontText
        text: componentRoot.text
    }

    PropertyAnimation {
        id: animationClose
        target: componentRoot
        property: "size"
        from: 100
        to: 0
        duration: 3000
        onStopped: {
            componentRoot.show = false
        }
    }
}

