import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.2
import QtQuick.Controls 1.4 as C

Rectangle {
    id: window
    //width: 800
    //height: 800
    width: mainWindow.width
    anchors.bottom: parent.bottom

    property var list_of_transaktions: []
    property var transaktion_index: 1
    property var transaktion_anzahl_zu_laden: 100

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }


    function load_transaktions(index) {
        busy(true)
        button_clear_cache.visible = false
        list_of_transaktions = []

        python.call("libs.send.get_transaktion", [vars.serverIP, vars.secure_key, index], function(transaktion) {
                if (transaktion == -1) {
                    button_clear_cache.visible = true
                    button_mehr_anzeigen.enabled = true
                    busy(false)
                } else {
                    transaktionModel.append(transaktion)


                    load_transaktions(index + 1)
                }


        })


    }
/*
    function load_cached_transaktions() {
        busy(true)
        button_clear_cache.visible = false
        list_of_transaktions = []

        python.call("libs.send.get_from_cache", ["transaktions"], function(transaktions_from_cache) {
            for (var i = 0; i < transaktions_from_cache.length; i++) {
                transaktionModel.append(transaktions_from_cache[i])
                list_of_transaktions.push(transaktions_from_cache[i])
                transaktion_index = transaktions_from_cache[i]["identification"] + 1
            }

            get_transaktion(transaktion_index)
        })
    }
    function get_transaktion(index) {
        python.call("libs.send.get_transaktion", [vars.serverIP, vars.secure_key, index], function(transaktion) {
            console.warn("transaktion(" + index + "): " + transaktion)
            if (transaktion == -1) {
                busy(false)
                button_clear_cache.visible = true
                python.call("libs.send.set_to_cache", ["transaktions", list_of_transaktions], function() {})
            } else {
                transaktionModel.append(transaktion)
                list_of_transaktions.push(transaktion)
                get_transaktion(index + 1)
                        //python.call("libs.send.set_to_cache", ["transaktion_" + index, transaktion], function() {})
            }
        })

    }
*/

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        onClicked: {
            view.push(frameSelect)
        }
    }

    /*
    ButtonSelect {
        id: button_kassenbuch
        text: "Kassenbuch"
        height: vars.backButtonHeight
        width: vars.backButtonWidth

        anchors.right: parent.right
        anchors.rightMargin: 10

        //font.pixelSize: vars.fontText
        onClicked: {
            python.call("libs.BlueFunc.Date", [], function(datum) {
                python.call("libs.send.get_kassenbuch", [vars.serverIP, vars.secure_key, datum], function() {
                    python.call("os.system", ["evince http://" + mainSettings.server_ip + "/zk-data/Kassenbuch/current.pdf"], function() {

                    })
                })
            })
        }
    }
    ButtonSelect {
        id: button_kassenverkäufe
        text: "Kassenverkäufe"
        height: vars.backButtonHeight
        width: vars.backButtonWidth

        anchors.right: button_kassenbuch.left
        anchors.rightMargin: 10

        enabled: false

        //font.pixelSize: vars.fontText
        onClicked: {
        }
    }
    ButtonSelect {
        id: button_rechnungen
        text: "Rechnungen"
        height: vars.backButtonHeight
        width: vars.backButtonWidth

        anchors.right: button_kassenverkäufe.left
        anchors.rightMargin: 10

        enabled: false

        //font.pixelSize: vars.fontText
        onClicked: {
        }
    }
    */

/*
    RoundButton {
        id: button_import
        text: "Import"
        anchors.right: button_clear_cache.left
        anchors.top: buttonBack.bottom
        anchors.topMargin: 10
        onClicked: {
            view.push(frameTransaktionenImport)
        }
    }
*/
    RoundButton {
        id: button_mehr_anzeigen
        text: "Weitere Transaktionen anzeigen"
        anchors.right: button_clear_cache.left
        anchors.top: buttonBack.bottom
        anchors.topMargin: 10
        onClicked: {
            button_mehr_anzeigen.enabled = false

            transaktion_index = transaktion_index - transaktion_anzahl_zu_laden

            if (transaktion_index < 0) {
                transaktion_index = 1
            }
            transaktionModel.clear()
            load_transaktions(transaktion_index)
        }
    }

    RoundButton {
        id: button_clear_cache
        text: "Cache löschen"
        anchors.right: parent.right
        anchors.top: buttonBack.bottom
        anchors.topMargin: 10
        onClicked: {
            python.call("libs.send.clear_cache", [], function() {
                start(1)
            })
        }
    }

    Label {
        id: lable_transaktion_row_quantity
        text: "Ergebnise: " + tableView_transaktion.rowCount
        anchors.top: buttonBack.bottom
        anchors.topMargin: 10
    }

    Rectangle {
        id: rectangle_transaktion_list
        border.width: 1
        width: parent.width
        anchors.top: button_clear_cache.bottom
        anchors.topMargin: 20
        anchors.bottom: parent.bottom
        C.TableView {
            id: tableView_transaktion
            anchors.fill: parent
            //clip: true
            sortIndicatorVisible: true

            C.TableViewColumn {
                role: "identification"
                title: "ID"
                width: 80
            }
            C.TableViewColumn {
                role: "datum"
                title: "Datum"
                width: 100
            }
            C.TableViewColumn {
                role: "uhrzeit"
                title: "Uhrzeit"
                width: 50
            }
            C.TableViewColumn {
                role: "betrag"
                title: "Betrag"
                width: 80
            }
            C.TableViewColumn {
                role: "konto_in"
                title: "Konto_id"
                width: 50
            }
            C.TableViewColumn {
                role: "konto_name"
                title: "Konto"
                width: 200
            }
            C.TableViewColumn {
                role: "user"
                title: "Bestätigt durch"
                width: 100
            }
            C.TableViewColumn {
                role: "rechnung"
                title: "Rechnung"
                width: 100
            }
            C.TableViewColumn {
                role: "mitteilung"
                title: "Mitteilung"
                width: 200
            }



            ListModel {
                id: transaktionModel
            }
/*
            Component {
                id: transaktionDelegate
                Item {
                    height: 25
                    Label {
                        text: identification
                    }
                }
            }*/

            model: transaktionModel
            //delegate: transaktionDelegate
        }
    }

    function busy(status) {
        busyindicator.visible = status
    }
    BusyIndicator {
        id: busyindicator
        running: true
        x: window.width / 2 - width / 2
        y: window.height / 2 - height / 2
    }

    function start(index) {
        python.call("libs.send.get_transaktion", [vars.serverIP, vars.secure_key, index], function(transaktion) {
            if (transaktion == -1) {
                if (index - transaktion_anzahl_zu_laden < 0) {
                    transaktion_index = 1
                } else {
                    transaktion_index = index - transaktion_anzahl_zu_laden
                }
                transaktionModel.clear()
                load_transaktions(transaktion_index)

            } else {
                start(index + 100)
            }
        })
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.kassenbuch', function () {});

            start(1)


        }
    }
}

