import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.0


Rectangle {
    id: window
    width: mainWindow.width
    //anchors.bottom: keyboard.top
    height: mainWindow.height

    color: vars.colorBackground

    Item {
        id: variable
        property int startBewegung : 0
        property int tag : 1
        property int monat : 1
        property int jahr : 2017
        property bool singelscan: false
    }

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    function send(barcode, anzahl) {
        console.warn("send()")
        vars.login_timeout = vars.login_max_timeout

        var categorie = vars.article_categories[combo_set_categorie.currentIndex]["identification"]
        console.warn("categorie: " + categorie)

        python.call("libs.send.add_inventar", [vars.serverIP, vars.secure_key, barcode, anzahl, categorie, variable.tag, variable.monat, variable.jahr], function(sucess) {
            if ( sucess == false) {
                warningText.visible = true
                warningText.text = "Fehler, \n" + barcode + " \n x" + anzahl
                rectangleStatus.color = "red"
            }

        });

    }

    function addItemToList(item) {
        for (var i=0; i<item.length; i++) {
            console.warn("addItemToList: " + item[i]["name_de"])
            contactModel.append(item[i]);
            if (item[i]["user"] == vars.namePC) {
                liste.currentIndex = liste.count - 1
            }
        }
    }

    Label {
        id: warningText
        text: ""
        font.pixelSize: window.width / 20
        x: window.width / 2 - width / 2
        y: window.height / 2 - height / 2
        visible: false
        color: "red"
    }


    Rectangle {
        id: topBar
        height: 100
        width: window.width
        border.width: 2

        Label {
            id: topBarTextDay
            text: " Tag"
            y: topBar.height / 2 - height / 2
        }
        Tumbler {
            id: tumblerDay
            anchors.left: topBarTextDay.right
            height: topBar.height
            model: 32
            currentIndex: variable.tag
            onCurrentIndexChanged: {
                console.warn("Day Changed: " + currentIndex)
                timerAddItem.stop()
                contactModel.clear()
                variable.tag = currentIndex
                timerAddItem.interval = 100
                variable.startBewegung = 0
                timerAddItem.start()
                warningText.visible = true
                warningText.text = "Achtung ! Datum wurde geändert \n\n" + variable.tag + "/" + variable.monat + "/" + variable.jahr
            }
        }
        MouseArea {
            anchors.top: topBar.top
            anchors.left: tumblerDay.left
            anchors.right: tumblerDay.right
            height: topBar.height / 3
            onClicked: {tumblerDay.currentIndex -= 1}
        }
        MouseArea {
            anchors.bottom: topBar.bottom
            anchors.left: tumblerDay.left
            anchors.right: tumblerDay.right
            height: topBar.height / 3
            onClicked: {tumblerDay.currentIndex += 1}
        }

        Label {
            id: topBarTextMonth
            text: " Monat"
            y: topBar.height / 2 - height / 2
            anchors.left: tumblerDay.right
        }
        Tumbler {
            id: tumblerMonth
            anchors.left: topBarTextMonth.right
            height: topBar.height
            model: 13
            currentIndex: variable.monat
            onCurrentIndexChanged: {
                console.warn("Month Changed: " + currentIndex)
                timerAddItem.stop()/
                contactModel.clear()
                variable.monat = currentIndex
                timerAddItem.interval = 100
                variable.startBewegung = 0
                timerAddItem.start()
                warningText.visible = true
                warningText.text = "Achtung ! Datum wurde geändert \n\n" + variable.tag + "/" + variable.monat + "/" + variable.jahr

            }
        }
        MouseArea {
            anchors.top: topBar.top
            anchors.left: tumblerMonth.left
            anchors.right: tumblerMonth.right
            height: topBar.height / 3
            onClicked: {tumblerMonth.currentIndex -= 1}
        }
        MouseArea {
            anchors.bottom: topBar.bottom
            anchors.left: tumblerMonth.left
            anchors.right: tumblerMonth.right
            height: topBar.height / 3
            onClicked: {tumblerMonth.currentIndex += 1}
        }

        Label {
            id: topBarTextYear
            text: " Jahr"
            y: topBar.height / 2 - height / 2
            anchors.left: tumblerMonth.right
        }
        Tumbler {
            id: tumblerYear
            anchors.left: topBarTextYear.right
            height: topBar.height
            model: 2050
            currentIndex: variable.jahr
            onCurrentIndexChanged: {
                console.warn("Year Changed: " + currentIndex)
                timerAddItem.stop()
                contactModel.clear()
                variable.jahr = currentIndex
                timerAddItem.interval = 100
                variable.startBewegung = 0
                timerAddItem.start()
                warningText.visible = true
                warningText.text = "Achtung ! Datum wurde geändert \n\n" + variable.tag + "/" + variable.monat + "/" + variable.jahr
            }
        }
        MouseArea {
            anchors.top: topBar.top
            anchors.left: tumblerYear.left
            anchors.right: tumblerYear.right
            height: topBar.height / 3
            onClicked: {tumblerYear.currentIndex -= 1}
        }
        MouseArea {
            anchors.bottom: topBar.bottom
            anchors.left: tumblerYear.left
            anchors.right: tumblerYear.right
            height: topBar.height / 3
            onClicked: {tumblerYear.currentIndex += 1}
        }
        Rectangle {
            id: rectangleStatus
            anchors.left: tumblerYear.right
            height: topBar.height / 2
            width: height
            y: topBar.height / 2 - height / 2
            radius: 50
            color: "red"
        }

        Label {
            anchors.left: rectangleStatus.right
            text: variable.startBewegung
        }

        ComboBox {
            id: combo_set_categorie
            anchors.right: checkboxSingleScan.left
            y: parent.height/2 - height/2
            width: 250

            ListModel {
                id: categorie_Model
            }

            textRole: "name"
            model: categorie_Model
        }

        CheckBox {
            id: checkboxSingleScan
            height: topBar.height
            anchors.right: topBar.right
            text: "Anzahl = 1"
            checked: variable.singelscan
            onToggled: {
                variable.singelscan = checkboxSingleScan.checked
                console.warn("variable.singlescan: " + checkboxSingleScan.checked)
            }

        }
    }


    Rectangle {
        id: bottomBar
        height: 50
        width: window.width
        border.width: 2
        anchors.bottom: window.bottom

        TextField {
            id: textScanBarcode
            width: window.width / 4 * 3
            height: parent.height
            placeholderText: "Identification / Barcode"
            selectByMouse: true
            inputMethodHints: Qt.ImhDigitsOnly
            onAccepted: {
                warningText.visible = false
                if (variable.singelscan == true) {
                    send(textScanBarcode.text, 1)
                    textScanBarcode.text = ""
                    textScanAnzahl.text = ""
                    textScanBarcode.forceActiveFocus()
                } else {
                    textScanAnzahl.forceActiveFocus()
                }
            }
        }
        TextField {
            id: textScanAnzahl
            anchors.left: textScanBarcode.right
            width: window.width / 4
            height: parent.height
            placeholderText: "Anzahl"
            selectByMouse: true
            inputMethodHints: Qt.ImhDigitsOnly
            onAccepted: {
                if (text.length > 6) {
                    send(textScanBarcode.text, 1)
                    textScanBarcode.text = text
                    textScanAnzahl.text = ""
                    textScanAnzahl.forceActiveFocus()
                } else {
                    if (text == "") {text = "1"}
                    send(textScanBarcode.text, textScanAnzahl.text)
                    textScanBarcode.text = ""
                    textScanAnzahl.text = ""
                    textScanBarcode.forceActiveFocus()
                }
            }
        }
    }

    ListView {
        id: liste
        anchors.top: topBar.bottom
        anchors.bottom: bottomBar.top
        width: window.width

        focus: true
        highlightMoveDuration: 20
        //highlight: Rectangle { color: "green"; width: window.width}

        clip: true

        ScrollBar.vertical: ScrollBar {
            active: true;
            //policy: ScrollBar.AlwaysOn
            width: window.width / 35//vars.scrollbarWidth
        }

        ListModel {
            id: contactModel
        }
        Component {
            id: contactDelegate
            Item {
                id: itemListe
                width: liste.width
                height: vars.listItemHeight

                Label {
                    id: textIdentification
                    text: bcode
                    width: window.width / 10
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.warn("clicked: " + bcode)
                            python.call("os.system", ["python3 /etc/zk-data/qml/ChangeStock.py " + vars.serverIP + " " + vars.secure_key + " " + bcode], function() {})
                        }
                    }
                }

                Label {
                    id: textAnzahl
                    text: "x" + anzahl
                    width: window.width / 10
                    anchors.left: textIdentification.right
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                }

                Label {
                    id: textName
                    text: name_de
                    anchors.left: textAnzahl.right
                    anchors.right: textEKP.left
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                }

                Label {
                    id: textCategorie
                    text: "cat: " + categorie
                    x: window.width / 10 * 7
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 15
                }

                Label {
                    id: textEK
                    text: preisek
                    x: window.width / 10 * 8
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                }

                Label {
                    id: textVK
                    text: preisvk
                    //anchors.right: liste.right
                    x: window.width / 10 * 9
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                }

                Label {
                    id: textUser
                    text: user
                    anchors.right: textAnzahl.left
                }
            }
        }

        model: contactModel
        delegate: contactDelegate
    }

    Timer {
        id : timerAddItem
        interval: 2000; running: false; repeat: false
        onTriggered: {
            console.warn("timerAddItem")

            python.call("libs.send.getInventar", [vars.serverIP, vars.secure_key, variable.tag, variable.monat, variable.jahr, variable.startBewegung], function(item) {
                console.warn("item: " + item["name_de"])
                if ( item["name_de"] === undefined ) {
                    interval = 2000
                    timerAddItem.start()
                    rectangleStatus.color = "orange"
                } else {
                    addItemToList([item])
                    interval = 0
                    variable.startBewegung = item["identification"]
                    timerAddItem.start()
                    rectangleStatus.color = "green"
                }
            });

        }
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.BlueFunc', function () {});
            importModule('os', function () {});

            call("libs.BlueFunc.date_year", [], function(date) {
                variable.jahr = date; warningText.visible = false

                call("libs.BlueFunc.date_month", [], function(date) {
                    variable.monat = date; warningText.visible = false
                    call("libs.BlueFunc.date_day", [], function(date) {variable.tag = date; warningText.visible = false});
                });
            });
            warningText.visible = false


            categorie_Model.clear()
            for (var i=0; i<vars.article_categories.length; i++) {
                categorie_Model.append(vars.article_categories[i])
            }
            combo_set_categorie.currentIndex = 0

            //call("Einstellungen.main.GetName", [], function(namePC) {vars.namePC = namePC});

            //call("KundenSuchen.main.SearchKunden", [textKundenSuchenIdentification.text, textKundenSuchenName.text], function() {});
        }
    }
}
