#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os

import sys
sys.path.append("/etc/zk-data-libs/")

from libs.appjar0900 import gui
#from libs.appjar0_94 import gui

import libs.RoundUp
import libs.BlueFunc
import libs.send
import libs.barcode

import urllib.parse
import urllib.request
import requests 

print(" ".join(sys.argv))
# .ChangeStock.py IP SECURE_KEY ID
ip = sys.argv[1]
secure_key = sys.argv[2]
ID = sys.argv[3]


def getMove():
    os.system("/etc/zk-data-libs/libs/get_move.py " + ip + " " + str(secure_key) + " " + str(ID))

def search_stihl():
    print("search_stihl()")
    
    artikel = appChange.getEntry("Artikel")
    lieferant = str(appChange.getEntry("Lieferant")).upper()
    url = "https://ssc.stihl.com/#/ssc/sso/fr/catalog/search/" + artikel + "?reset=true"
    
    if lieferant == "STIHL":
        cmd = "firefox -new-tab " + url
        os.system(cmd + " &")
    
def Save():
    global DATA
    print("Save")
    DATA["name_de"] = appChange.getEntry("Name")
    DATA["name_fr"] = appChange.getEntry("NameFR")
    DATA["lieferant"] = appChange.getEntry("Lieferant")
    DATA["artikel"] = appChange.getEntry("Artikel")
    DATA["artikel2"] = appChange.getEntry("Artikel2")
    DATA["artikel3"] = appChange.getEntry("Artikel3")
    DATA["artikel4"] = appChange.getEntry("Artikel4")
    DATA["preisek"] = appChange.getEntry("Einkaufspreis")
    DATA["tva"] = appChange.getEntry("MwSt")  
    DATA["preisvkh"] = appChange.getEntry("VerkaufspreisHTVA")
    DATA["preisvk"] = appChange.getEntry("VerkaufspreisTVAC")  
    DATA["minimum"] = appChange.getEntry("Minimum")
    DATA["barcode"] = appChange.getEntry("Barcode")
    DATA["ort"] = appChange.getEntry("Ort")
    DATA["bild"] = appChange.getEntry("Bild")
    DATA["groesse"] = appChange.getEntry("Grösse")
    DATA["categorie"] = categorie_list.index(appChange.getOptionBox("Kategorie"))
    #DATA["categorie"] = appChange.getRadioButton("categorie").split(" ")[0]
    
    new_timestamp = str(libs.send.GetArt(ip, secure_key, ID)["lastchange"])
    old_timestamp = str(DATA["lastchange"])
    if not new_timestamp == old_timestamp:
        appChange.infoBox("Achtung", "Dieser Artikel wurde gerade von einem anderen ort aus geändert", parent=None)
        return False
    else:
        print("Send Data to Server")
        sucess = libs.send.set_article(ip, secure_key, DATA)
        print("sucess: " + str(sucess))
        if sucess:
            # # add_to_prediction(prediction_name, answer, word_data)
            if not DATA["name_de"] == "":
                if not DATA["lieferant"] == "":
                    libs.send.add_to_prediction(ip, secure_key, "predict_article_suplier", DATA["lieferant"], DATA["name_de"])
                    
                    libs.send.add_to_prediction(ip, secure_key, "predict_article_suplier_with_part", DATA["lieferant"], " ".join(list(DATA["lieferant"])))
                    
                    libs.send.add_to_prediction(ip, secure_key, "predict_article_categorie", DATA["categorie"], DATA["name_de"] + " " + DATA["lieferant"])
                    libs.send.add_to_prediction(ip, secure_key, "predict_article_taxe", float(DATA["tva"]), DATA["name_de"] + " " + DATA["lieferant"])
                if not DATA["groesse"] == "":
                    libs.send.add_to_prediction(ip, secure_key, "predict_article_size", DATA["groesse"], DATA["name_de"])
            return True
        else:
            appChange.infoBox("Achtung", "Artikel konnte nicht gespeichert werden", parent=None)
            return False


def VerifyInputFloat(Entry):
    #print("VerifyInputFloat")
    #print("Verify this input " + str(appChange.getEntry(Entry)))
    myFloat = appChange.getEntry(Entry)
    myFloat = myFloat.replace(",", ".")
    myFloat = myFloat.replace("..", ".")
    myFloat = myFloat.replace(".0.", ".")
    try:
        if Entry == "MwSt":
            #print("MwSt")
            myFloat = libs.RoundUp.RoundUp05(myFloat)
            myFloat = float(myFloat)
        if Entry == "Einkaufspreis":
            #print("Einkaufspreis")
            myFloat = libs.RoundUp.RoundUp0000(myFloat)
            myFloat = float(myFloat)
        if Entry == "VerkaufspreisHTVA":
            #print("VerkaufspreisHTVA")
            myFloat = libs.RoundUp.RoundUp0000(myFloat)
            mwst = 1 + (float(appChange.getEntry("MwSt"))/100)
            appChange.setEntry("VerkaufspreisTVAC", libs.RoundUp.RoundUp05(myFloat*mwst), callFunction=False)
            myFloat = libs.RoundUp.RoundUp0000(float(appChange.getEntry("VerkaufspreisTVAC"))/mwst)
            myFloat = float(myFloat)
        if Entry == "VerkaufspreisTVAC":
            #print("VerkaufspreisTVAC")
            myFloat = libs.RoundUp.RoundUp05(myFloat)
            mwst = 1 + (float(appChange.getEntry("MwSt"))/100)
            appChange.setEntry("VerkaufspreisHTVA", libs.RoundUp.RoundUp0000(myFloat/mwst), callFunction=False)
            myFloat = libs.RoundUp.RoundUp05(float(appChange.getEntry("VerkaufspreisHTVA"))*mwst)
            myFloat = float(myFloat)
            
    except:
        myFloat = 0.0
    appChange.setEntry(Entry, myFloat)
    
    # Set prozente Label
    # 10 * (1-20/100) = 8
    # vkh * (1-proz/100) = ek
    # prozente = -(preisek/preisvkh - 1)*100
    
    try:
        preisek = float(appChange.getEntry("Einkaufspreis"))
        preisvkh = float(appChange.getEntry("VerkaufspreisHTVA"))    
        prozente = -(preisek/preisvkh - 1)*100
        prozente = libs.RoundUp.RoundUp05(prozente)
        appChange.setLabel("Prozente", "prozente: " + str(prozente) + "%")
    except: True
        
def VerifyInputInt(Entry):
    #print("VerifyInputInt")
    #print("Verify this input " + str(appChange.getEntry(Entry)))
    myInt = appChange.getEntry(Entry)
    appChange.setEntryMaxLength(Entry, 13)
    try:
        appChange.setEntry(Entry, int(myInt))
        if Entry == "Barcode":
            if not len(appChange.getEntry(Entry)) == 13:
                appChange.setEntry(Entry, libs.barcode.IDToBarcode(ID))
                appChange.infoBox("Achtung", "Dieser Barcode ist ungültig und wird jetzt neu generiert")
    except:
        if Entry == "Barcode":
            appChange.setEntry(Entry, libs.barcode.IDToBarcode(ID))
        else:
            appChange.setEntry(Entry, "0")

def Translate(Entry):
    print("Translate()")

    if Entry == "Name":
        translated_text = libs.BlueFunc.translate("DE", "FR", appChange.getEntry(Entry))
        translated_text = translated_text["text"]
        appChange.setEntry("NameFR", translated_text)
    else:
        translated_text = libs.BlueFunc.translate("FR", "DE", appChange.getEntry(Entry))
        translated_text = translated_text["text"]
        appChange.setEntry("Name", translated_text)


def VerifyInputChar(Entry):
    #print("VerifyInputChar")
    #print("Verify this input " + str(appChange.getEntry(Entry)))
    
    ##appChange.setEntry(Entry, appChange.getEntry(Entry).replace("?", ""))
    if Entry == "Lieferant":
        try:
            myString = appChange.getEntry("Lieferant")
            if " " in myString:
                myString_as_list = list(myString.replace(" ", ""))
                myString = " ".join(myString_as_list)
                predict_article_suplier = libs.send.make_prediction(ip, secure_key, "predict_article_suplier_with_part", myString)
                appChange.setEntry("Lieferant", predict_article_suplier)
        except: True
    
    if Entry == "Name":
        # predict Lieferant ?
        True
        #try:
        #    myString_name = appChange.getEntry("Name")
        #    if appChange.getEntry("Lieferant") == "" and " " in myString_name: #and len(myString_name) > 4:
        #        predict_article_suplier = libs.send.make_prediction(ip, secure_key, "predict_article_suplier", myString_name)
        #        appChange.setEntry("Lieferant", predict_article_suplier)
        #    #else:
        #    #    myString = appChange.getEntry("Name") + " " + appChange.getEntry("Lieferant")
        #    #    predict_article_categorie = libs.send.make_prediction(ip, secure_key, "predict_article_categorie", myString)
        #    #    appChange.setOptionBox("Kategorie", categorie_list[int(predict_article_categorie)])
        #        
        #    #if appChange.getEntry("Groesse") == "" and len(myString_name) > 4:
        #    #    predict_article_size = libs.send.make_prediction(ip, secure_key, "predict_article_size", myString_name)
        #    #    appChange.setEntry("Groesse", predict_article_size)
        #    
        #except: True
            
        #libs.send.add_to_prediction(ip, secure_key, "predict_article_suplier", DATA["lieferant"], DATA["name_de"])
        #libs.send.add_to_prediction(ip, secure_key, "predict_article_taxe", float(DATA["tva"]), DATA["name_de"] + " " + DATA["lieferant"])
        #libs.send.add_to_prediction(ip, secure_key, "predict_article_categorie", DATA["categorie"], DATA["name_de"] + " " + DATA["lieferant"])
        #libs.send.add_to_prediction(ip, secure_key, "predict_article_size", DATA["groesse"], DATA["name_de"])

    if Entry == "Ort":
        myString = appChange.getEntry(Entry).upper()
        myString = myString.replace(",", ".")
        myString = myString.replace(" ", "")
        appChange.setEntry(Entry, myString)

    if Entry == "Artikel" or Entry == "Artikel2" or Entry == "Artikel3" or Entry == "Artikel4" or Entry == "Lieferant":
        myString = appChange.getEntry(Entry).upper()
        EndString = ""
        for character in myString:
            if character.isalpha() or character.isdigit():
                EndString = EndString + str(character)

        appChange.setEntry(Entry, EndString)


def VerifyChanges():
    #print("VerifyChanges")
    UserMadeChanges = True
    ID = appChange.getLabel("Title")
    if UserMadeChanges:
        if appChange.yesNoBox("Speichern", "Wollen sie speichern?", parent=None):
            if Save():
                return True
            else:
                appChange.infoBox("Speichern", "änderungen wurden nicht gespeichert", parent=None)
                return False
        else:
            return True

DATA = libs.send.get_article(ip, secure_key, ID)

appChange = gui("Stock ändern", "800x800", handleArgs=False)
appChange.setIcon("/usr/share/pixmaps/zk-data-stock.gif")
appChange.setBg("#3399ff")
appChange.setFont(12)

appChange.addLabel("Title", str(DATA["identification"]), 0, 1, 5, 0)

appChange.addLabelEntry("Name", 1, 1, 2, 0)
appChange.setEntryChangeFunction("Name", VerifyInputChar)
appChange.setEntrySubmitFunction("Name", Translate)
appChange.setEntry("Name", DATA["name_de"])
appChange.setFocus("Name")

appChange.addLabelEntry("NameFR", 2, 1, 2, 0)
appChange.setEntryChangeFunction("NameFR", VerifyInputChar)
appChange.setEntrySubmitFunction("NameFR", Translate)
appChange.setEntry("NameFR", DATA["name_fr"])

appChange.addLabelEntry("Lieferant", 1, 4, 2, 0)
appChange.setEntryChangeFunction("Lieferant", VerifyInputChar)
DATA["lieferant"] = str(DATA["lieferant"]).split("_")[0]
appChange.setEntry("Lieferant", DATA["lieferant"])

#appChange.addLabel("leer1", "", 2, 0, 2, 0)

appChange.addLabelEntry("Artikel", 5, 1, 2, 0)
appChange.setEntryChangeFunction("Artikel", VerifyInputChar)
appChange.setEntry("Artikel", DATA["artikel"])

appChange.addLabelEntry("Artikel2", 5, 4, 2, 0)
appChange.setEntryChangeFunction("Artikel2", VerifyInputChar)
appChange.setEntry("Artikel2", DATA["artikel2"])

appChange.addLabelEntry("Artikel3", 6, 1, 2, 0)
appChange.setEntryChangeFunction("Artikel3", VerifyInputChar)
appChange.setEntry("Artikel3", DATA["artikel3"])

appChange.addLabelEntry("Artikel4", 6, 4, 2, 0)
appChange.setEntryChangeFunction("Artikel4", VerifyInputChar)
appChange.setEntry("Artikel4", DATA["artikel4"])

#appChange.addLabel("leer2", "", 6, 0, 1, 0)

categorie_list = []
for cat in libs.send.get_article_categories(ip, secure_key):
    categorie_list.append(cat["name"])
    #if cat["enable"]:

appChange.addLabelOptionBox("Kategorie", categorie_list, 7, 1, 2, 0)
try: appChange.setOptionBox("Kategorie", categorie_list[int(DATA["categorie"])])
except: appChange.setOptionBox("Kategorie", 0)
#appChange.setRadioButton(str(categorie["identification"]), 1)

appChange.addLabelEntry("MwSt", 7, 4, 2, 0)
appChange.setEntryChangeFunction("MwSt", VerifyInputFloat)
appChange.setEntry("MwSt", DATA["tva"])

appChange.addLabelEntry("Einkaufspreis", 8, 1, 2, 0)
appChange.setEntryChangeFunction("Einkaufspreis", VerifyInputFloat)
appChange.setEntry("Einkaufspreis", DATA["preisek"])

appChange.addLabel("Prozente", "prozente: ?", 8, 4, 2, 0)


appChange.addLabelEntry("VerkaufspreisHTVA", 9, 1, 2, 0)
appChange.setEntryChangeFunction("VerkaufspreisHTVA", VerifyInputFloat)
appChange.setEntry("VerkaufspreisHTVA", DATA["preisvkh"])

appChange.addLabelEntry("VerkaufspreisTVAC", 9, 4, 2, 0)
appChange.setEntryChangeFunction("VerkaufspreisTVAC", VerifyInputFloat)
appChange.setEntry("VerkaufspreisTVAC", DATA["preisvk"])


#appChange.addLabel("leer3", "", 10, 0, 1, 0)
appChange.addLabelEntry("Minimum", 10, 4, 2, 0)
appChange.setEntryChangeFunction("Minimum", VerifyInputFloat)
appChange.setEntry("Minimum", DATA["minimum"])

appChange.addLabelEntry("Barcode", 11, 1, 2, 0)
appChange.setEntryChangeFunction("Barcode", VerifyInputInt)
appChange.setEntry("Barcode", DATA["barcode"])
eigener_barcode_festlegen = libs.BlueFunc.get_data("eigener_barcode_festlegen")
if str(eigener_barcode_festlegen) == "None":
    if appChange.yesNoBox("Barcode festlegen", "Wollen sie eigene Barcode einstellen können ?", parent=None):
        eigener_barcode_festlegen = True
    else:
        eigener_barcode_festlegen = False
    libs.BlueFunc.set_data("eigener_barcode_festlegen", eigener_barcode_festlegen)
if not bool(eigener_barcode_festlegen) == True:
    appChange.setEntryState("Barcode", "disabled")

appChange.addLabelEntry("Ort", 11, 4, 2, 0)
appChange.setEntryChangeFunction("Ort", VerifyInputChar)
appChange.setEntry("Ort", DATA["ort"])

appChange.addLabelEntry("Bild", 12, 1, 2, 0)
appChange.setEntryChangeFunction("Bild", VerifyInputFloat)
appChange.setEntry("Bild", DATA["bild"])

appChange.addLabelEntry("Grösse", 12, 4, 2, 0)
appChange.setEntryChangeFunction("Grösse", VerifyInputFloat)
appChange.setEntry("Grösse", DATA["groesse"])

def StopWindow(btn):
    appChange.stop()

appChange.setStopFunction(VerifyChanges)
#appChange.addLabel("Info", "<F5> = Speichern und Schliesen", 15, 0, 5, 0)
appChange.bindKey("<F5>", StopWindow)
appChange.bindKey("<F4>", getMove)

appChange.bindKey("<F7>", search_stihl)
appChange.go()

