import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.0

Rectangle {
    id: window
    width: 800//mainWindow.width
    //anchors.bottom: keyboard.top
    height: 800//mainWindow.height

    //color: "blue"//vars.colorBackground

    Rectangle {
        id: rectange_start
        width: parent.width
        border.width: 1
        height: comboBox_start_jahr.y * 2 + comboBox_start_jahr.height

        Label {
            id: label_start
            x: parent.width / 2 - width / 2
            text: "Inventar Start"
        }


        Label {
            id: label_start_jahr
            anchors.top: label_start.bottom
            text: "Jahr"
        }
        ComboBox {
            id: comboBox_start_jahr
            enabled: false
            anchors.left: label_start_jahr.right
            anchors.top: label_start.bottom
            width: 200
            model: ListModel {
                id: model_start_jahr
            }
            onActivated: {
                comboBox_start_monat.enabled = false
                comboBox_start_tag.enabled = false
                comboBox_end_jahr.enabled = false
                comboBox_end_monat.enabled = false
                comboBox_end_tag.enabled = false

                model_start_monat.clear()
                model_start_tag.clear()
                model_end_jahr.clear()
                model_end_monat.clear()
                model_end_tag.clear()

                get_start_month(1)
            }
        }

        Label {
            id: label_start_monat
            anchors.top: label_start.bottom
            anchors.left: comboBox_start_jahr.right
            text: "Monat"
        }
        ComboBox {
            id: comboBox_start_monat
            enabled: false
            anchors.left: label_start_monat.right
            anchors.top: label_start.bottom
            width: 200
            model: ListModel {
                id: model_start_monat
            }
            onActivated: {
                comboBox_start_tag.enabled = false
                comboBox_end_jahr.enabled = false
                comboBox_end_monat.enabled = false
                comboBox_end_tag.enabled = false

                model_start_tag.clear()
                model_end_jahr.clear()
                model_end_monat.clear()
                model_end_tag.clear()

                get_start_day(1)
            }
        }


        Label {
            id: label_start_tag
            anchors.top: label_start.bottom
            anchors.left: comboBox_start_monat.right
            text: "Tag"
        }
        ComboBox {
            id: comboBox_start_tag
            enabled: false
            anchors.left: label_start_tag.right
            anchors.top: label_start.bottom
            width: 200
            model: ListModel {
                id: model_start_tag
            }
            onActivated: {
                comboBox_end_jahr.enabled = false
                comboBox_end_monat.enabled = false
                comboBox_end_tag.enabled = false

                model_end_jahr.clear()
                model_end_monat.clear()
                model_end_tag.clear()

                get_end_year(parseInt(comboBox_start_jahr.currentText))
            }
        }
    }


    Rectangle {
        id: rectangle_end
        anchors.top: rectange_start.bottom
        width: parent.width
        border.width: 1
        height: comboBox_end_jahr.y * 2 + comboBox_end_jahr.height

        Label {
            id: label_end
            x: parent.width / 2 - width / 2
            text: "Inventar End"
        }


        Label {
            id: label_end_jahr
            anchors.top: label_end.bottom
            text: "Jahr"
        }
        ComboBox {
            id: comboBox_end_jahr
            enabled: false
            anchors.left: label_end_jahr.right
            anchors.top: label_end.bottom
            width: 200
            model: ListModel {
                id: model_end_jahr
            }
            onActivated: {
                comboBox_end_monat.enabled = false
                comboBox_end_tag.enabled = false

                model_end_monat.clear()
                model_end_tag.clear()

                if (comboBox_start_jahr.currentText == comboBox_end_jahr.currentText) {
                    get_end_month(parseInt(comboBox_start_monat.currentText))
                } else {
                    get_end_month(1)
                }
            }
        }

        Label {
            id: label_end_monat
            anchors.top: label_end.bottom
            anchors.left: comboBox_end_jahr.right
            text: "Monat"
        }
        ComboBox {
            id: comboBox_end_monat
            enabled: false
            anchors.left: label_end_monat.right
            anchors.top: label_end.bottom
            width: 200
            model: ListModel {
                id: model_end_monat
            }
            onActivated: {
                comboBox_end_tag.enabled = false

                model_end_tag.clear()


                if (comboBox_start_jahr.currentText == comboBox_end_jahr.currentText) {
                    if (comboBox_start_monat.currentText == comboBox_end_monat.currentText) {
                        get_end_day(parseInt(comboBox_start_tag.currentText))
                    } else {
                        get_end_day(1)
                    }

                } else {
                    get_end_day(1)
                }

            }
        }


        Label {
            id: label_end_tag
            anchors.top: label_end.bottom
            anchors.left: comboBox_end_monat.right
            text: "Tag"
        }
        ComboBox {
            id: comboBox_end_tag
            enabled: false
            anchors.left: label_end_tag.right
            anchors.top: label_end.bottom
            width: 200
            model: ListModel {
                id: model_end_tag
            }

            onActivated: {
                inventar_is_loading.visible = true

                comboBox_start_tag.enabled = false
                comboBox_start_monat.enabled = false
                comboBox_start_tag.enabled = false
                comboBox_end_jahr.enabled = false
                comboBox_end_monat.enabled = false
                comboBox_end_tag.enabled = false

                python.call("libs.send.print_inventar", [vars.serverIP, vars.secure_key, {"start_jahr": comboBox_start_jahr.currentText, "start_monat": comboBox_start_monat.currentText, "start_tag": comboBox_start_tag.currentText, "end_jahr": comboBox_end_jahr.currentText, "end_monat": comboBox_end_monat.currentText, "end_tag": comboBox_end_tag.currentText}], function(answer) {
                    inventar_is_loading.visible = false

                    comboBox_start_tag.enabled = true
                    comboBox_start_monat.enabled = true
                    comboBox_start_tag.enabled = true
                    comboBox_end_jahr.enabled = true
                    comboBox_end_monat.enabled = true
                    comboBox_end_tag.enabled = true

                    var timestamp = answer.split("|")[0]
                    Qt.openUrlExternally("http://192.168.1.35:8000/inventar_" + timestamp + "_" + "0.csv")
                    Qt.openUrlExternally("http://192.168.1.35:8000/inventar_" + timestamp + "_" + "1.csv")
                    Qt.openUrlExternally("http://192.168.1.35:8000/inventar_" + timestamp + "_" + "2.csv")
                    Qt.openUrlExternally("http://192.168.1.35:8000/inventar_" + timestamp + "_" + "3.csv")
                    Qt.openUrlExternally("http://192.168.1.35:8000/inventar_" + timestamp + "_" + "4.csv")
                    Qt.openUrlExternally("http://192.168.1.35:8000/inventar_" + timestamp + "_" + "5.csv")
                    Qt.openUrlExternally("http://192.168.1.35:8000/inventar_" + timestamp + "_" + "6.csv")
                    Qt.openUrlExternally("http://192.168.1.35:8000/inventar_" + timestamp + "_" + "7.csv")
                    Qt.openUrlExternally("http://192.168.1.35:8000/inventar_" + timestamp + "_" + "8.csv")
                });
            }
        }
    }

    function get_start_year (jahr) {
        inventar_is_loading.visible = true
        python.call("libs.send.getInventar", [vars.serverIP, vars.secure_key, 0, 0, jahr, 0], function(inventory_dict) {
            console.warn(jahr + " inventory_dict: " + inventory_dict)

            if ( inventory_dict == "") {
                //console.warn("")
            } else {
                model_start_jahr.append({"text": jahr.toString()})
            }

            if (jahr < 2030) {
                get_start_year(jahr + 1)
            } else {
                comboBox_start_jahr.enabled = true
                inventar_is_loading.visible = false
            }
        });
    }

    function get_start_month (monat) {
        inventar_is_loading.visible = true
        python.call("libs.send.getInventar", [vars.serverIP, vars.secure_key, 0, monat, comboBox_start_jahr. currentText, 0], function(inventory_dict) {
            console.warn(monat + " inventory_dict: " + inventory_dict)

            if ( inventory_dict == "") {
                //console.warn("")
            } else {
                model_start_monat.append({"text": monat.toString()})
            }

            if (monat < 13) {
                get_start_month(monat + 1)
            } else {
                comboBox_start_monat.enabled = true
                inventar_is_loading.visible = false
            }
        });
    }

    function get_start_day (tag) {
        inventar_is_loading.visible = true
        python.call("libs.send.getInventar", [vars.serverIP, vars.secure_key, tag, comboBox_start_monat.currentText, comboBox_start_jahr.currentText, 0], function(inventory_dict) {
            console.warn(tag + " inventory_dict: " + inventory_dict)

            if ( inventory_dict == "") {
                //console.warn("")
            } else {
                model_start_tag.append({"text": tag.toString()})
            }

            if (tag < 32) {
                get_start_day(tag + 1)
            } else {
                comboBox_start_tag.enabled = true
                inventar_is_loading.visible = false
            }
        });
    }



    function get_end_year (jahr) {
        inventar_is_loading.visible = true
        python.call("libs.send.getInventar", [vars.serverIP, vars.secure_key, 0, 0, jahr, 0], function(inventory_dict) {
            console.warn(jahr + " inventory_dict: " + inventory_dict)

            if ( inventory_dict == "") {
                //console.warn("")
            } else {
                model_end_jahr.append({"text": jahr.toString()})
            }

            if (jahr < 2030) {
                get_end_year(jahr + 1)
            } else {
                comboBox_end_jahr.enabled = true
                inventar_is_loading.visible = false
            }
        });
    }

    function get_end_month (monat) {
        inventar_is_loading.visible = true
        python.call("libs.send.getInventar", [vars.serverIP, vars.secure_key, 0, monat, comboBox_end_jahr. currentText, 0], function(inventory_dict) {
            console.warn(monat + " inventory_dict: " + inventory_dict)

            if ( inventory_dict == "") {
                //console.warn("")
            } else {
                model_end_monat.append({"text": monat.toString()})
            }

            if (monat < 13) {
                get_end_month(monat + 1)
            } else {
                comboBox_end_monat.enabled = true
                inventar_is_loading.visible = false
            }
        });
    }

    function get_end_day (tag) {
        inventar_is_loading.visible = true
        python.call("libs.send.getInventar", [vars.serverIP, vars.secure_key, tag, comboBox_end_monat.currentText, comboBox_end_jahr.currentText, 0], function(inventory_dict) {
            console.warn(tag + " inventory_dict: " + inventory_dict)

            if ( inventory_dict == "") {
                //console.warn("")
            } else {
                model_end_tag.append({"text": tag.toString()})
            }

            if (tag < 32) {
                get_end_day(tag + 1)
            } else {
                comboBox_end_tag.enabled = true
                inventar_is_loading.visible = false
            }
        });
    }

    BusyIndicator{
        id: inventar_is_loading
        visible: false
        width: parent.width / 2
        x: parent.width / 2 - width / 2
        anchors.top: rectangle_end.bottom
        height: parent.height - rectangle_end.y - rectangle_end.height
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.BlueFunc', function () {});

            get_start_year(2000)
        }
    }
}
