import QtQuick 2.7
import QtQuick.Controls 2.0
import QtCharts 2.0

Rectangle {
    id: root_chart_frame_diagramm

    property var x_names: ["2007", "2008", "2009", "2010", "2011", "2012"]
    property var list_of_points: [[3, 2, 3, 4, 5, 6], [3, 2, 3, 4, 5, 6], [3, 2, 3, 4, 5, 6]]
    property var titles: ["Bobi", "Peter", "Anna"]

    height: 500; width: 500

    ChartView {
        id: chart
        title: "Stacked Bar series"
        anchors.fill: parent
        legend.alignment: Qt.AlignBottom
        antialiasing: true

        StackedBarSeries {
            id: mySeries
            axisX: BarCategoryAxis { categories: x_names }

            BarSet { label: "Bob"; values: [2, 2, 3, 4, 5, 6] }
            BarSet { label: "Susan"; values: [5, 1, 2, 4, 1, 7] }
            BarSet { label: "James"; values: [3, 5, 8, 13, 5, 8] }
        }

        Component.onCompleted: {
            for (var i = 0; i < titles.length; ++i) {
                chart
            }
        }
    }


}
