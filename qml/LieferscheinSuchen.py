#!/usr/bin/env python3

import os
import sys
sys.path.append("/etc/zk-data-libs/")
import threading
import time

try: import pyotherside
except: True
import libs.send
import libs.BlueFunc

class Main:    
    def __init__(self):
        print("init")

        pyotherside.send("setInfoLabelText", "LieferscheinSuchen.py init")
            
        self.busy(False)
        pyotherside.send("setInfoLabelText", "") 

    def GetLieferscheine(self, secure_key, identification, kunde, fertige, eigene):   
        print("Lieferscheine") 
        Dict = {}
        Dict["identification"] = str(identification)
        Dict["kunde_id"] = str(kunde)
        Dict["fertig"] = bool(fertige)
        Dict["eigene"] = bool(eigene)
        DieseSuche = {"identification": identification, "kunde": kunde, "fertige": fertige, "eigene": eigene}
        print("DieseSuche = " + str(DieseSuche))
        self.busy(True)
        
        try: 
            pyotherside.send("setInfoLabelText", "libs.send.searchLieferschein(" + str(Dict) + ")")
            listeDerElemente = libs.send.SearchLieferschein(secure_key, Dict)
            pyotherside.send("setInfoLabelText", "")
        except: 
            listeDerElemente = []
            os.system("xcowsay 'Fehler (SearchLieferschein)'")
            pyotherside.send("setInfoLabelText", "Fehler in libs.send.SearchLieferschein")
        Antwort = []    
        for item in listeDerElemente:
            try: 
                pyotherside.send("setInfoLabelText", "libs.send.GetLieferschein(" + str(item) + ")")
                DATA = libs.send.GetLieferschein(secure_key, str(item))
                pyotherside.send("setInfoLabelText", "")                
            except: 
                DATA = {}
                os.system("xcowsay 'Fehler (GetLieferschein)'")
                pyotherside.send("setInfoLabelText", "Fehler in libs.send.GetLieferschein")
            Antwort.append(DATA)

        pyotherside.send("antwortSearchLieferscheine", Antwort)    
        self.busy(False)

    def busy(self, status):
        status = bool(status)
        print("busy = " + str(status))
        pyotherside.send("busy", status)
        
main = Main()

