import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.0

Rectangle {
    id: componentRoot
    //property string text
    //y: mainWindow.height - height
    x: 0
    //border.width: 2
    height: componentLabel.height
    width: componentLabel.width

    color: "transparent"
    Label {
        id: componentLabel
        y: componentRoot.height / 2 - height / 2
        text: vars.infoLabelText//componentRoot.text
        //onTextChanged: {console.warn(text)}
    }
}

