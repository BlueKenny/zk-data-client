import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0

import "../../zk-data-libs/qml" as ZKLibs

Rectangle {
    id: window
    width: mainWindow.width
    height: mainWindow.height

    color: vars.colorBackground

    property bool show_warning: false

    Keys.onPressed: {
        if ( event.key === Qt.Key_F1 ) {
            key_F1()
        }
        if ( event.key === Qt.Key_F2 ) {
            //if (vars.userData["permission_invoice"] > 0) {
            view.push(frameRechnungSuchen)
            //}
        }
        if ( event.key === Qt.Key_F3 ) {
            //if (vars.userData["permission_invoice_e"] > 0) {
            view.push(frameRechnungESuchen)
            //}
        }
        if ( event.key === Qt.Key_F4 ) {
            python.call("libs.send.send_and_receive", [vars.serverIP, vars.secure_key, {"mode": "set_user_page_settings", extras_delivery_id: vars.lieferscheinID}], function(answer) {
                var new_url = "http://" + vars.serverIP + ":51515/testing/extras/index.html"
                python.call("os.system", ["firefox", new_url], function(answer) {
                    // open browser on http://127.0.0.1:51515/testing/extras/index.html    gutschein_erstellen.html
                })
            })
        }
        if ( event.key === Qt.Key_F5 ) {
            console.warn("K5")

            if(stock_article_search.visible == true) {
                stock_article_search.visible = false
            } else {
                stock_article_search.visible = true
            }
        }
        if ( event.key === Qt.Key_F6 ) {
            python.call("libs.send.make_prediction", [vars.serverIP, vars.secure_key, "predict_delivery_cost", lieferscheinInfoText.text + " " + textLieferscheinAnzeigenMaschine.text], function(prediction) {
                infoBox.text = "Schätzung der Kosten : mehr als " + prediction + "€"
                infoBox.color = "green"
                infoBox.show = true
            })
        }
        if ( event.key == Qt.Key_Escape ) {
            view.push(frameLieferscheinSuchen)
        }
        if ( event.key === Qt.Key_F12 ) {
            python.call("libs.send.get_delivery_rest", [vars.serverIP, vars.secure_key, vars.lieferscheinID], function(rest) {
                infoBox.text = "Gewinn\n" + rest + "€"
                if (rest < 0.0) {
                    infoBox.color = "orange"
                } else {
                    infoBox.color = "green"
                }
                infoBox.show = true
            })
        }
        if ( event.key === Qt.Key_F7 ) {
            python.call("libs.send.get_logs", [vars.serverIP, vars.secure_key, "delivery" , vars.lieferscheinID], function(file) {
                python.call("os.system", ["wget http://" + vars.serverIP.split(":")[0] + file + " -O /tmp/delivery_log.csv"], function() {
                    python.call("os.system", ["libreoffice /tmp/delivery_log.csv &"], function() {})
                })
            })
        }
    }

    Item {
        id: variable
        property int listeIndex: 0//to remove ?
        property bool showInfo: true
        property int oldCurrentIndex: 0
        property bool setFocusOnBarcode: true//to remove
        property bool enabled: true

    }
/*
    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }*/

    function scanFehler(fehlercode) {
        infoBox.text = "Fehler\n" + fehlercode
        infoBox.color = "orange"
        infoBox.show = true
    }

    function set_delivery_line(index, quantity, barcode, name, price, tva) {
        python.call("libs.send.set_delivery_line", [vars.serverIP, vars.secure_key, vars.lieferscheinID, index, quantity, barcode, name, price, tva], function(sucess) {
            console.warn("sucess: " + sucess)
            if (sucess == true) {
                variable.listeIndex = index + 1//liste.currentIndex + 1
                if (name === "[DELETE]") {
                    view.push(frameLieferscheinSuchen)
                } else {
                    get_delivery()
                }
            } else {
                scanFehler("Artikel exisitiert nicht oder server nicht erreichbar")
                variable.listeIndex = liste.currentIndex
                get_delivery()
            }
        })
    }

    function get_delivery() {
        vars.busy = true
        vars.login_timeout = vars.login_max_timeout

        python.call("libs.send.get_delivery", [vars.serverIP, vars.secure_key, vars.lieferscheinID], function(dict) {
            //dict["rappel"] = 2.1
            labelInfo.text = ""

            var worker_list = dict["user"].split("|")
            console.warn("worker_list: " + worker_list)
            button_arbeiter.text = "Arbeiter: ?"

            if (worker_list.length == 1) {
                button_arbeiter.text = "Arbeiter: " + worker_list[0]
            } else {
                button_arbeiter.text = "Arbeiter: "
                for (var i=0; i < worker_list.length; i++) {
                    if (worker_list[i] === "") {
                    } else {
                        button_arbeiter.text = button_arbeiter.text + worker_list[i][0]
                    }
                }
            }

            if (show_warning == true) {
                python.call('libs.send.get_client', [vars.serverIP, vars.secure_key, dict["kunde_id"]], function(client) {
                    if (client["warning"] == "") {} else {
                        warningText.text = client["warning"]
                        warningText.border_color = "orange"
                        warningText.visible = true
                    }
                    show_warning = false
                });
            }

            if (dict["rechnung"] == "") {
                variable.enabled = true
                if (dict["angebot"] == true) {
                    button_rechnung_kasse.visible = false
                    labelLieferscheinAnzeigenTitle.text = "Angebot: " + vars.lieferscheinID
                } else {
                    if (dict["bestellung"] == true) {
                        button_rechnung_kasse.visible = false
                        labelLieferscheinAnzeigenTitle.text = "Bestellung: " + vars.lieferscheinID
                    } else {
                        button_rechnung_kasse.visible = true
                        labelLieferscheinAnzeigenTitle.text = "Lieferschein: " + vars.lieferscheinID

                         python.call("libs.send.get_client", [vars.serverIP, vars.secure_key, dict["kunde_id"]], function(client) {
                             if (client["identification"] == "0") {
                                client["rappel"] = 10
                             }

                            if (client["rappel"] > 0) {
                                button_rechnung_kasse.text = "Kassenverkauf"
                                button_rechnung_kasse.color = "orange"
                            }
                         })
                    }
                }
            } else {
                variable.enabled  = false

                if (dict["rechnung"].indexOf("RV") === 0) {
                    button_rechnung_kasse.text = "Vorlage: " + dict["rechnung"]                    
                    variable.enabled = true
                }
                else {
                    if (dict["rechnung"].indexOf("R") === 0) {
                        button_rechnung_kasse.text = "Rechnung: " + dict["rechnung"]
                        variable.enabled = false
                    } else {
                        if (dict["rechnung"].indexOf("K") === 0) {
                            button_rechnung_kasse.text = "Kassenverkauf: " + dict["rechnung"]
                            variable.enabled = false
                        }
                    }
                }
            }

            contactModel.clear()

            textLieferscheinAnzeigenKundeID.text = dict["kunde_id"]
            labelLieferscheinAnzeigenKundeName.text = dict["kunde_name"]
            textLieferscheinAnzeigenMaschine.text = dict["maschine"]
            calendar.selected_date = dict["datum_todo"]
            switchFinish.checked = dict["fertig"]

            //labelTotal.text = "HTVA = " +  dict["total_htva"] + " €"
            labelTotal.text = "HTVA = " +  dict["total_ohne_steuern"] + " €"

            labelTotal.text = labelTotal.text + "            |            TVA: " + dict["steuern"]

            labelTotal.text = labelTotal.text + "            |            total = " + dict["total"] + " €"


            if (dict["dealer"] == true) {
                labelTotal.text += "          [ Händler ]"
            }

            lieferscheinInfoText.text = dict["info"]
            if ( dict["info"].length > 0) {
                buttonInfo.text = "Info ..."
            } else {
                buttonInfo.text = "Info"
            }

            var linie_pos = dict["linien"].split("|")
            var linie_anzahl = dict["anzahl"].split("|")
            var linie_bcode = dict["bcode"].split("|")
            var linie_name = dict["name"].split("|")
            var linie_preis = dict["preis"].split("|")
            var linie_tva = dict["tva"].split("|")
            var linie_preis_htva = dict["preis_htva"].split("|")

            for (var i=0; i<linie_pos.length; i++) {
                var linie_isInfo = false
                var linie_barcodeEnabled = false

                if (linie_bcode[i] == "") {
                    //console.warn("[" + linie_bcode[i] + "] ist empty")
                    linie_isInfo = true
                    linie_barcodeEnabled = true
                }

                contactModel.append({
                                        "anzahl": linie_anzahl[i],
                                        "bcode": linie_bcode[i],
                                        "name": linie_name[i],
                                        "preis": linie_preis[i],
                                        "tva": linie_tva[i],//tva,
                                        "preis_htva": linie_preis_htva[i],
                                        "is_info": linie_isInfo,
                                        "barcode_is_enabled": linie_barcodeEnabled
                                    })
            }
            //console.warn("set focus on line " + variable.listeIndex)
            liste.currentIndex = variable.listeIndex

            vars.busy = false
        })
    }

    ZKLibs.ArticleSearch {
        id: stock_article_search
        width: window.width*0.8
        height: window.height*0.8
        x: window.width / 2 - width/2
        y: window.height / 2 - height/2

        visible: false

        ip: vars.serverIP
        secure_key: vars.secure_key

        //color: ""
        z: window.z + 100

        onSelected_articleChanged: {
            python.call("libs.send.add_delivery_article", [vars.serverIP, vars.secure_key, vars.lieferscheinID, selected_article], function(sucess) {
                //infoBox.text = "OK"
                //infoBox.color = "green"
                //infoBox.show = true
                get_delivery()
            })

            stock_article_search.visible = false
        }
    }

    Label {
        id: labelLieferscheinAnzeigenTitle
        text: "Lieferschein: " + vars.lieferscheinID
        font.pixelSize: vars.fontTitle//vars.isPhone ? mainWindow.width / 20 : mainWindow.width / 50
        x: mainWindow.width / 2 - width / 2
        y: vars.pageTitleY//buttonBack.height / 2 - height / 2
        z: 2
        ToolTip.timeout: 5000

        onTextChanged: {
            if (focus == true) {
                get_delivery()
            }
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onContainsMouseChanged: {
                if (containsMouse == true) {
                    ToolTip.visible = true
                    ToolTip.text = "Links klick um diesen Text zu Kopieren"
                }
            }
            onClicked: {
                var delivery_prefix_letter = "L"
                if (labelLieferscheinAnzeigenTitle.text == "Angebot: " + vars.lieferscheinID) {
                    delivery_prefix_letter = "A"
                } else {
                    if (labelLieferscheinAnzeigenTitle.text == "Bestellung: " + vars.lieferscheinID) {
                        delivery_prefix_letter = "B"
                    }
                }


                python.call("libs.BlueFunc.setClipboard", [delivery_prefix_letter + vars.lieferscheinID], function() {
                    infoBox.text = "text \"" + delivery_prefix_letter + vars.lieferscheinID + "\" kopiert"
                    infoBox.color = "green"
                    infoBox.show = true
                })
            }
        }
    }

    Label {
        id: labelLieferscheinAnzeigenDatum
        text: vars.lieferscheinDatum
        font.pixelSize: vars.fontText
        x: mainWindow.width / 2 - width / 2
        anchors.top: labelLieferscheinAnzeigenTitle
        z: 2
    }

    Label {
        id: labelLieferscheinAnzeigenKundeID
        text: "Kunde : "
        font.pixelSize: vars.fontText//vars.isPhone ? mainWindow.width / 20 : mainWindow.width / 50
        x: vars.isPhone ? mainWindow.width / 10 * 6 - width / 2: mainWindow.width / 10 * 7 - width / 2
        //y: vars.isPhone ? (labelTitle1.y - labelLieferscheinAnzeigenTitle.y) / 2 : labelLieferscheinAnzeigenTitle.y
        anchors.top: vars.isPhone ? labelLieferscheinAnzeigenTitle.bottom : mainWindow.top
        anchors.topMargin: vars.isPhone ? 20 : 0
    }

    ButtonSelect {
        id: button_kunde_entfernen
        text: "X"
        y: labelLieferscheinAnzeigenKundeID.y + labelLieferscheinAnzeigenKundeID.height - height / 2
        x: labelLieferscheinAnzeigenKundeID.x + labelLieferscheinAnzeigenKundeID.width
        height: textLieferscheinAnzeigenKundeID.height
        width: 50
        visible: variable.enabled
        onClicked: {
            python.call("libs.send.set_delivery_client", [vars.serverIP, vars.secure_key, vars.lieferscheinID, "0"], function() {
                get_delivery()
            })
        }
    }

    TextField {
        id: textLieferscheinAnzeigenKundeID
        text: vars.lieferscheinAnzeigenKundeID
        font.pixelSize: vars.fontText
        //x: mainWindow.width / 10 * 8
        y: labelLieferscheinAnzeigenKundeID.y// - height / 2
        //width: mainWindow.width / 5
        anchors.left: button_kunde_entfernen.right
        anchors.right: button_kunde_anzeigen.left
        enabled: variable.enabled
        MouseArea {
            anchors.fill: textLieferscheinAnzeigenKundeID
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            onClicked: {
                if (mouse.button == Qt.LeftButton) {
                    vars.kundenSuchenVorherigeAnsicht = "frameLieferscheinAnzeigen"
                    vars.lieferscheinAnzeigenSetKundeID = true
                    view.push(frameKundenSuchen)
                } /*else { //TODO fix, error, client id is set on wrong Delivery !
                    vars.kundenSuchenVorherigeAnsicht = "frameLieferscheinAnzeigen"
                    vars.kundeAnzeigenKundeID = textLieferscheinAnzeigenKundeID.text
                    vars.lieferscheinAnzeigenSetKundeID = true
                    view.push(frameKundenAnzeigen)
                }*/
            }
        }
        onTextChanged: {
            if (text == "0") {
                parent.color = "lightblue"
            } else {
                parent.color = vars.colorBackground
            }
        }
        //onTextChanged: {
        //    if (vars.lieferscheinAnzeigenSetKundeID == true) {
        //        vars.lieferscheinAnzeigenSetKundeID = false
        //        python.call('LieferscheinAnzeigen.main.SetKunde', [text], function(kunde_name) {labelLieferscheinAnzeigenKundeName.text = kunde_name});
        //    }
        //}
    }    
    ButtonSelect {
        id: button_kunde_anzeigen
        text: "Anzeigen"
        y: labelLieferscheinAnzeigenKundeID.y + labelLieferscheinAnzeigenKundeID.height - height / 2//labelLieferscheinAnzeigenKundeID.y
        anchors.right: parent.right
        height: textLieferscheinAnzeigenKundeID.height
        //width: textLieferscheinAnzeigenKundeID.width / 3
        visible: variable.enabled
        onClicked: {
            vars.kundeAnzeigenKundeID = textLieferscheinAnzeigenKundeID.text
            vars.kundenSuchenVorherigeAnsicht = "frameLieferscheinAnzeigenKunde"
            view.push(frameKundenAnzeigen)

        }
    }

    Text {
        id: labelLieferscheinAnzeigenKundeName
        text: ""
        font.pixelSize: vars.fontText//vars.isPhone ? mainWindow.width / 20 : mainWindow.width / 50
        x: mainWindow.width / 10 * 8
        y: textLieferscheinAnzeigenKundeID.y + textLieferscheinAnzeigenKundeID.height
        width: mainWindow.width / 5
    }

    Label {
        id: labelLieferscheinAnzeigenMaschine
        text: "Maschine : "
        anchors.top: labelLieferscheinAnzeigenKundeName.bottom
        anchors.right: labelLieferscheinAnzeigenKundeName.left
        font.pixelSize: vars.fontText
    }
    TextField {
        id: textLieferscheinAnzeigenMaschine
        anchors.top: labelLieferscheinAnzeigenKundeName.bottom
        anchors.left: labelLieferscheinAnzeigenKundeName.left
        width: mainWindow.width - x
        font.pixelSize: vars.fontText
        text: ""
        selectByMouse: true
        onTextChanged: {if (focus == true) {color = "red"}}
        //enabled: variable.enabled // always enable
        onAccepted: {
            python.call('libs.send.set_delivery_engine', [vars.serverIP, vars.secure_key, vars.lieferscheinID, text], function(sucess) {
                if (sucess == true) {
                    color = "black"
                }
            }

            );}
    }

    ButtonSelect {
        id: buttonInfo
        text: "Info"
        height: window.height / 18
        width: window.width / 8
        anchors.top: buttonBack.bottom
        anchors.topMargin: 20
        z: labelLieferscheinAnzeigenTitle.z

        onClicked: {
            console.warn("infoText")
            lieferscheinInfoText.visible = true
        }
    }

    ButtonSelect {
        id: buttonDatum_todo
        text: "Fertigstellen bis: " + calendar.selected_date
        height: window.height / 18
        width: vars.isPhone ? mainWindow.width / 2 : mainWindow.width / 6
        anchors.top: buttonBack.bottom
        anchors.topMargin: 20
        anchors.left: buttonInfo.right
        anchors.leftMargin: 20
        z: labelLieferscheinAnzeigenTitle.z
        enabled: variable.enabled
        onClicked: {
            console.warn("Datum_todo show")
            calendar.visible = true
        }
    }

    Label {
        id: labelTitle1
        text: "Anzahl"
        font.pixelSize: vars.fontText//labelLieferscheinAnzeigenTitle.font.pixelSize
        x: window.width / 10 - width / 2//window.width / 5 - width / 2
        //y: window.height / 10
        anchors.top: buttonInfo.bottom
        anchors.topMargin: 20
        z: labelLieferscheinAnzeigenTitle.z
    }
    Label {
        id: labelTitle2
        text: "Barcode"
        font.pixelSize: vars.fontText//labelLieferscheinAnzeigenTitle.font.pixelSize
        x: labelTitle1.x + labelTitle1.width + mainWindow.width / 20//window.width / 5 * 2 - width / 2
        y: labelTitle1.y
        z: labelLieferscheinAnzeigenTitle.z
    }
    Label {
        id: labelTitle3
        text: "Name"
        font.pixelSize: vars.fontText//labelLieferscheinAnzeigenTitle.font.pixelSize
        x: mainWindow.width / 2//window.width / 5 * 3 - width / 2
        y: labelTitle1.y
        z: labelLieferscheinAnzeigenTitle.z
    }
    Label {
        id: labelTitle4
        text: "ohne MwSt"
        font.pixelSize: vars.fontText
        //x: labelPreisHTVA.x
        x: window.width / 5 * 4 - width / 2
        //width: comboTVA.x - labelPreisHTVA.x
        y: labelTitle1.y
        z: labelLieferscheinAnzeigenTitle.z
    }
    Label {
        id: labelTitle5
        text: "MwSt"
        font.pixelSize: vars.fontText
        anchors.left: labelTitle4.right
        anchors.leftMargin: 10
        //width: textPreis.x - comboTVA.x
        y: labelTitle1.y
        z: labelLieferscheinAnzeigenTitle.z
    }
    Label {
        id: labelTitle6
        text: "inkl. MwSt"
        font.pixelSize: vars.fontText
        anchors.right: parent.right
        anchors.rightMargin: 10
        //width: parent.width - comboTVA.x
        y: labelTitle1.y
        z: labelLieferscheinAnzeigenTitle.z
    }

    Rectangle {
        border.width: 1
        //y: window.height / 10 * 2
        z: labelLieferscheinAnzeigenTitle.z - 1
        width: window.width
        //height: window.height * 0.5
        anchors.top: labelTitle1.bottom
        anchors.bottom: labelInfo.top
        color: "transparent"

        ListView {
            id: liste
            focus: true
            highlightMoveDuration: 0
            highlight: Rectangle { color: "lightsteelblue"; width: window.width; border.width: 1}

            onCurrentIndexChanged: {
                //console.warn("liste.currentIndex: " + currentIndex)
                //console.warn("variable.oldCurrentIndex: " + variable.oldCurrentIndex)

                //console.warn(liste)

                variable.oldCurrentIndex = currentIndex
            }

            ScrollBar.vertical: ScrollBar {
                active: true;
                //policy: ScrollBar.AlwaysOn
                width: vars.scrollbarWidth
            }

            clip: true


            //y: parent.y
            z: parent.z
            anchors.fill: parent
            //height: parent.height

            ListModel {
                id: contactModel
            }

            Component {
                id: contactDelegate
                Item {
                    id: itemListe
                    property int currentIndex: index // store item index
                    property bool visibleOnPrint: true

                    property int dragOldIndex: -1
                    property int dragNewIndex: -1

                    property int dragStartYPos: -1
                    property int dragEndYPos: -1

                    property bool isInfo: false

                    width: window.width
                    height: vars.listItemHeight

                    onYChanged: {dragEndYPos = y}

                    enabled: variable.enabled

                    onActiveFocusChanged: {
                        if (variable.setFocusOnBarcode == true) {
                            textBarcode.forceActiveFocus()
                        }
                    }
                    ButtonSelect {
                        id: buttonOption
                        text: "X"
                        width: 2 * vars.fontText //parent.width / 20
                        height: 2 * vars.fontText //parent.height * 0.8
                        y: parent.height / 2 - height / 2
                        //font.pixelSize: vars.fontText
                        visible: variable.enabled
                        onClicked: {
                            liste.currentIndex = itemListe.currentIndex;
                            python.call('libs.send.delete_delivery_line', [vars.serverIP, vars.secure_key, vars.lieferscheinID, liste.currentIndex], function(sucess) {
                                variable.listeIndex = liste.currentIndex
                                get_delivery()
                            });
                        }
                    }
                    TextField {
                        id: textAnzahl
                        onEditingFinished: {
                            deselect()
                        }
                        onActiveFocusChanged: {
                            deselect()
                            if (focus == true) {selectAll()}
                        }
                        font.pixelSize: vars.fontText
                        width: parent.width / 15
                        x: window.width / 15 - width / 2
                        y: parent.height / 2 - height / 2
                        text: is_info ? "0.0" : anzahl
                        inputMethodHints: Qt.ImhDigitsOnly
                        horizontalAlignment: TextEdit.AlignHCenter
                        verticalAlignment: TextEdit.AlignVCenter
                        selectByMouse: true
                        enabled: is_info ? false : true

                        focus: false

                        onTextChanged: {if (focus == true) {color = "red"}}
                        onAccepted: {
                            deselect();
                            if(barcode_is_enabled == true) {
                                textBarcode.forceActiveFocus()
                            } else {
                                textName.forceActiveFocus()
                            }
                        }

                        onFocusChanged: {
                            if(focus) {
                                console.warn("focus " + index)
                                liste.currentIndex = index;

                                textAnzahl.forceActiveFocus()
                                textAnzahl.selectAll();
                            } else {
                                deselect()
                            }
                        }
                    }

                    TextField {
                        id: textBarcode

                        Keys.onPressed: {
                            if ( event.key === Qt.Key_F4 ) {
                                console.warn("F4 Pressed")
                                python.call("os.system", ["/etc/zk-data-libs/libs/get_move.py " + vars.serverIP + " " + vars.secure_key + " " + text], function() {})
                            }
                        }
                        onEditingFinished: {
                            deselect()
                        }
                        onActiveFocusChanged: {
                            deselect()
                            if (focus == true) {
                                selectAll()
                            }
                        }
                        ToolTip.timeout: 20000

                        font.pixelSize: vars.fontText
                        width: window.width / 10
                        x: vars.isPhone ? textAnzahl.x + textAnzahl.width : textAnzahl.x + textAnzahl.width
                        y: vars.isPhone ? buttonOption.y : parent.height / 2 - height / 2
                        text: bcode
                        focus: true
                        horizontalAlignment: TextEdit.AlignHCenter
                        font.capitalization: Font.AllUppercase
                        selectByMouse: true
                        enabled: barcode_is_enabled
                        onTextChanged: {if (focus == true) {

                                if (text.length < 4) {
                                    /*
                                    python.call("libs.send.get_barcode_helper", [vars.serverIP, vars.secure_key, text], function(answer) {
                                        if (answer == "") {
                                            console.warn("No answer")
                                        } else {
                                            ToolTip.visible = true
                                            ToolTip.text = answer
                                        }

                                    })
                                    */
                                    ToolTip.text = "F5 Drücken \n Um einen Artikel zu suchen ohne die Artikelnummer kennen"
                                    ToolTip.visible = true
                                }

                                color = "red"
                                if (text == "") {
                                    is_info = true
                                } else {
                                    is_info = false
                                    if (textAnzahl.text == "0.0") {
                                        textAnzahl.text = "1.0"
                                    }
                                }

                                if (text == "*") {
                                    deselect()
                                    textAnzahl.forceActiveFocus()
                                    textAnzahl.selectAll()
                                } else {
                                    if (text.indexOf("*") != -1) {
                                        console.warn("substring is *")
                                        text = text.replace("*", "")
                                        if (is_info == false) {
                                            console.warn("* pressed, ForceActiveFocus() on Quantity")
                                            textAnzahl.forceActiveFocus()
                                            textAnzahl.selectAll()
                                        }
                                    }
                                }
                            }}

                        onAccepted: {
                            deselect();

                            if (textBarcode.text == "") {
                                textName.forceActiveFocus()
                            } else {
                                set_delivery_line(index, textAnzahl.text, textBarcode.text, textName.text, textPreis.text, comboTVA.currentText)
                            }
                        }
                        onFocusChanged: {
                            if(focus) {
                                console.warn("focus " + index)

                                liste.currentIndex = index;
                                //liste.currentIndex = itemListe.currentIndex;

                                textBarcode.forceActiveFocus()
                                textBarcode.selectAll();
                            } else {
                                deselect()
                            }
                        }
                    }
                    Image {
                        id: articleImage
                        //width: 50
                        height: textName.height
                        y: textName.y
                        anchors.left: textBarcode.right
                        fillMode: Image.PreserveAspectFit
                        asynchronous: true
                        //mipmap: true
                        smooth: true
                        Component.onCompleted: {
                            python.call("libs.send.get_article", [vars.serverIP, vars.secure_key, textBarcode.text], function(answer) {
                                if (answer == []) {
                                    console.warn("No answer")
                                } else {
                                    articleImage.source = answer["bild"]
                                }
                            })
                        }
                    }
                    TextField {
                        id: textName
                        onEditingFinished: {
                            deselect()
                        }
                        onActiveFocusChanged: {
                            deselect()
                            if (focus == true) {selectAll()}
                        }
                        font.pixelSize: vars.fontText
                        anchors.left: articleImage.right
                        anchors.right: labelPreisHTVA.left
                        y: vars.isPhone ? parent.height / 2 : parent.height / 2 - height / 2
                        text: name
                        horizontalAlignment: TextEdit.AlignHCenter
                        verticalAlignment: TextEdit.AlignVCenter
                        selectByMouse: true
                        maximumLength: 50
                        onTextChanged: {if (focus == true) {color = "red"}}

                        onAccepted: {
                            deselect();

                            if (textBarcode.text == "") {
                                set_delivery_line(index, textAnzahl.text, textBarcode.text, textName.text, textPreis.text, comboTVA.currentText)
                            } else {
                                textPreis.forceActiveFocus()
                            }
                        }
                        onFocusChanged: {
                            if(focus) {
                                console.warn("focus " + index)

                                liste.currentIndex = index;

                                textName.forceActiveFocus()
                                textName.selectAll();
                            } else {
                                deselect()
                            }
                        }
                    }

                    Label {
                        id: labelPreisHTVA
                        anchors.right: comboTVA.left
                        y: parent.height / 2 - height / 2
                        text: preis_htva
                    }
                    ComboBox {
                        id: comboTVA
                        //anchors.left: labelPreisHTVA.right
                        anchors.right: textPreis.left
                        y: parent.height / 2 - height / 2
                        model: vars.mwst
                        //currentIndex: tva

                        onCurrentIndexChanged: {
                            if (focus == true) {
                                set_delivery_line(index, textAnzahl.text, textBarcode.text, textName.text, textPreis.text, comboTVA.textAt(currentIndex))
                            }
                        }

                        Component.onCompleted: {
                            console.warn("tva: " + tva)
                            console.warn("tva set to index: " + comboTVA.find(tva, Qt.MatchExactly))
                            comboTVA.currentIndex = comboTVA.find(tva, Qt.MatchExactly)
                        }
                    }
                    TextField {
                        id: textPreis
                        onEditingFinished: {
                            deselect()
                        }
                        onActiveFocusChanged: {
                            deselect()
                            if (focus == true) {selectAll()}
                        }

                        font.pixelSize: vars.fontText
                        width: window.width / 10
                        //anchors.left: comboTVA.right
                        anchors.right: parent.right
                        y: vars.isPhone ? buttonOption.y : parent.height / 2 - height / 2
                        z: labelLieferscheinAnzeigenTitle.z
                        text: is_info ? "0.0" : preis
                        inputMethodHints: Qt.ImhDigitsOnly
                        horizontalAlignment: TextEdit.AlignHCenter
                        selectByMouse: true
                        onTextChanged: {
                            if (focus == true) {color = "red"}
                        }
                        enabled: is_info ? false : true

                        onAccepted: {
                            deselect();
                            set_delivery_line(index, textAnzahl.text, textBarcode.text, textName.text, textPreis.text, comboTVA.currentText)
                        }
                        onFocusChanged: {
                            if(focus) {
                                console.warn("focus " + index)

                                liste.currentIndex = index;
                                //liste.currentIndex = itemListe.currentIndex;

                                textPreis.forceActiveFocus()
                                textPreis.selectAll();

                                python.call("libs.send.get_article", [vars.serverIP, vars.secure_key, textBarcode.text], function(answer) {
                                    if (answer == []) {
                                        console.warn("No answer")
                                    } else {
                                        if (text == answer["preisvk"]) {} else {
                                            var aktueller_preis = text
                                            var original_preis = answer["preisvk"]

                                            var prozente = (( 1 - (aktueller_preis / original_preis)) * 100).toPrecision(2)

                                            ToolTip.visible = true
                                            ToolTip.text = answer["preisvk"] + " €    " + prozente + " %"
                                        }
                                    }
                                })


                            } else {
                                deselect()
                            }


                        }


                    }
                }
            }

            model: contactModel
            delegate: contactDelegate
        }
    }

    Label {
        id: labelInfo
        text: "Information: ?"
        font.pixelSize: vars.fontTitle
        x: window.width / 2 - width / 2
        anchors.bottom: labelTotal.top
        anchors.bottomMargin: 10
    }

    Label {
        id: labelTotal
        text: "?"
        font.pixelSize: vars.fontTitle//window.height / 20
        x: window.width / 2 - width / 2
        //y: window.height * 0.8
        anchors.bottom: switchFinish.top
    }

    ButtonSelect {
        id: buttonBack
        text: "Zurück"
        height: window.height / 15
        width: window.width / 5
        //x: window.width / 3 - width / 2
        //y: window.height * 0.9
        //font.pixelSize: vars.fontText

        onClicked: {
            //python.call('LieferscheinAnzeigen.main.Ok', [], function() {});
            view.push(frameLieferscheinSuchen)
        }
    }

    ButtonSelect {
        id: send_sms
        height: window.height / 15
        width: switchFinish.width / 2
        anchors.right: switchFinish.left
        anchors.rightMargin: 10
        anchors.bottom: parent.bottom
        visible: false
        text: "SMS senden"
        onClicked: {
            python.call("libs.send.send_sms", [vars.serverIP, vars.secure_key, "0471520101", "TEST"], function() {
                send_sms.visible = false
            })
        }

    }

    Switch {//ButtonSelect {
        id: switchFinish
        height: window.height / 15
        width: window.width / 5
        x: mainWindow.width / 2 - width / 2//window.width / 11 * 5 - width / 2
        //y: window.height * 0.9
        anchors.bottom: parent.bottom
        z: labelLieferscheinAnzeigenTitle.z
        text: checked ? "Fertig" : "Nicht Fertig" //variable.switchFinishChecked ? "Fertig" : "Nicht Fertig"
        //font.pixelSize: vars.fontText
        checked: false
        enabled: variable.enabled

        onClicked: {
            text = checked ? "Fertig" : "Nicht Fertig"
            python.call("libs.send.set_delivery_finish", [vars.serverIP, vars.secure_key, vars.lieferscheinID, switchFinish.checked], function() {});
            get_delivery()
        }
    }
    ButtonSelect {
        id: button_drucken
        text: "Drucken"
        height: window.height / 15
        width: window.width / 5
        anchors.right: window.right
        anchors.bottom: parent.bottom
        z: labelLieferscheinAnzeigenTitle.z

        onClicked: {
            vars.busy = true
            python.call("libs.send.get_delivery_pdf", [vars.serverIP, vars.secure_key, vars.lieferscheinID], function(pdf_path){
            var delivery_prefix_letter = "L"
                if (labelLieferscheinAnzeigenTitle.text == "Angebot: " + vars.lieferscheinID) {
                    delivery_prefix_letter = "A"
                } else {
                    if (labelLieferscheinAnzeigenTitle.text == "Bestellung: " + vars.lieferscheinID) {
                        delivery_prefix_letter = "B"
                    }
                }

                python.call("os.system", ["wget http://" + vars.serverIP + ":51515" + pdf_path + " -O /tmp/" + vars.serverNAME + "/" + delivery_prefix_letter + vars.lieferscheinID + ".pdf && evince /tmp/" + vars.serverNAME + "/" + delivery_prefix_letter + vars.lieferscheinID + ".pdf &"], function() {
                    vars.busy = false
                })
            })

        }
    }
    ButtonSelect {
        id: button_rechnung_kasse
        text: "Rechnung / Kasse"
        height: window.height / 15
        width: window.width / 5
        //x: window.width / 11 * 9 - width / 2
        //y: window.height * 0.9
        anchors.right: button_drucken.left
        anchors.bottom: parent.bottom
        z: labelLieferscheinAnzeigenTitle.z
        //font.pixelSize: vars.fontText

        onClicked: {
            python.call("libs.send.create_rechnung_vorlage", [vars.serverIP, vars.secure_key, vars.lieferscheinID], function(answer) {
                vars.rechnungID = answer
                view.push(frameRechnungAnzeigen)
            })
        }
    }

    ButtonSelect {
        id: button_arbeiter
        text: "Arbeiter"
        height: window.height / 15
        width: window.width / 5
        x: window.width / 11 * 1 - width / 2
        //y: window.height * 0.9
        anchors.bottom: parent.bottom
        z: labelLieferscheinAnzeigenTitle.z
        //font.pixelSize: vars.fontText
        //visible: variable.enabled

        onClicked: {
            python.call('libs.send.get_delivery_user', [vars.serverIP, vars.secure_key, vars.lieferscheinID], function(dictList) {
                arbeiterListe.items = dictList
                arbeiterListe.visible = true
            });
        }
    }

    CheckList {
        id: arbeiterListe
        text: "Arbeiter Liste"
        items: []
        onCheckedChanged: {
            console.warn("CheckList item changed: " + changingItem + ": " + changingState)
            python.call('libs.send.set_delivery_user', [vars.serverIP, vars.secure_key, vars.lieferscheinID, changingItem, changingState], function(sucess) {
                get_delivery()
            })
        }
    }
    FocusText {
        id: lieferscheinInfoText
        text: ""
        visible: false
        enabled_text: variable.enabled
        onClosed: {
            if (variable.enabled == true) {
                python.call("libs.send.set_delivery_info", [vars.serverIP, vars.secure_key, vars.lieferscheinID, text], function(sucess) {
                    get_delivery()
                })
            }
        }
    }

    Popup {
        id: popupKundenInfoAnzeigen
        x: parent.width / 2 - width / 2
        y: parent.height / 2 - height / 2
        width: parent.width - 400
        height: parent.height - 400
        modal: true
        Label {
            id: label_kunden_info_title
            text: "Info: "
            x: parent.width / 2 - width / 2
        }
        Label {
            id: label_kunden_info_text
            text: "?"
            anchors.top: label_kunden_info_title.bottom
            anchors.bottom: parent.bottom
            width: parent.width
        }
    }

    InfoLabel {}
    function setInfoLabelText(text) {vars.infoLabelText = text}
    Busy {}
    function busy(status) {vars.busy = status}
    InfoBox {id: infoBox}
    FocusText {
        id: warningText
        text: ""
        visible: false
        enabled_text: false
    }

    Kalender {
        id: calendar
        onSet_date: {
            python.call("libs.send.set_delivery_date_todo", [vars.serverIP, vars.secure_key, vars.lieferscheinID, selected_date], function(sucess) {})
        }
        /*
        onSelected_dateChanged: {
            python.call("libs.send.set_delivery_date_todo", [vars.serverIP, vars.secure_key, vars.lieferscheinID, selected_date], function(sucess) {})
        }
        */
    }

    Component.onCompleted: {
        show_warning = true
        get_delivery()
    }

    /*
    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});

            get_delivery()

        }
    }
    */
}
