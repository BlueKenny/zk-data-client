import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0

//import Fluid.Controls 1.0 as FluidControls

Rectangle {
    id: window
    color: vars.colorBackground

    property var index: 0
    property bool is_searching: false
    property int process: 0

    property int max_index: 50

    Keys.onPressed: {
        console.warn("key " + event.key + " pressed")

        if ( event.key === Qt.Key_F2 ) {
            console.warn("userData: permission_invoice: " + vars.userData["permission_invoice"])
            //if (vars.userData["permission_invoice"] > 0) {
                view.push(frameRechnungSuchen)
            //}
        }
        if ( event.key === Qt.Key_F3 ) {
            buttonNew.clicked()
        }

        if (event.key === Qt.Key_Up) {
            rechnung_e_liste.decrementCurrentIndex()
        }
        if (event.key === Qt.Key_Down) {
            rechnung_e_liste.incrementCurrentIndex()
        }
        if (event.key === Qt.Key_Enter) {
            process = process + 1

            vars.rechnungID_e = rechnung_model.get(rechnung_e_liste.currentIndex).identification
            vars.rechnungSuchenEIndex = rechnung_e_liste.currentIndex

            view.push(frameRechnungEAnzeigen)

        }
    }

    function rechnung_open_pdf(identification) {
        vars.busy = true        
        vars.login_timeout = vars.login_max_timeout

        var identification_to_string = identification.toString().replace(".0", "")
        python.call("libs.send.get_invoice_e_pdf", [vars.serverIP, vars.secure_key, identification_to_string], function(pdf_path){
            console.warn("pdf_path: " + pdf_path)
            python.call("os.system", ["wget -q -N http://" + vars.serverIP + ":51515" + pdf_path + " -P /tmp/ && evince /tmp/RE" + identification_to_string + ".pdf &"], function() {
                vars.busy = false
            })
        })

        /*
        python.call("PrinterSelect.main.Drucken", [0, identification, true, 1], function(sucess) {
            if (sucess == true) {
                python.call("os.system",["evince http://" + vars.serverIP + "/zk-data/Rechnung/" + identification + ".pdf &"], function() { });
                vars.busy = false
            }
        });
        */
    }

    function suchen(process_id) {
        is_searching = true
        vars.login_timeout = vars.login_max_timeout

        console.warn("rechnung suchen()")
        //search_rechnung_e(identification, kunde, datum, index)
        python.call("libs.send.search_rechnung_e", [vars.serverIP, vars.secure_key, text_rechnung_identification.text, text_dokument_id.text, text_kunde_identification.text, checkbox_nicht_bezahlt.checked, "", index], function(answer) {
            console.warn("Search Rechnung: " + answer)
            if (answer == "-1" || answer == "") { // or
                is_searching = false
            } else {
                console.warn("rechnung_e: " + answer)
                python.call("libs.send.get_rechnung_e", [vars.serverIP, vars.secure_key, answer], function(rechnung) {
                    index = index + 1
                    if (rechnung == -1) {

                    } else {

                        if (rechnung["rest"] == 0) {
                            rechnung["ist_bezahlt"] = "black"
                        } else {
                            rechnung["ist_bezahlt"] = "red"
                        }

                        if (process == process_id) {
                            rechnung_model.append(rechnung)

                            if (rechnung_model.count < max_index) {
                                suchen(process_id)
                            } else {
                                rechnung_model.append({"identification": -1, "lieferant_id": "0", "datum": "Weitere Rechnungen Laden - Zu viele ergebnise !                   ", "ist_bezahlt": "red", "dokument_datum": "", "dokument_id": "", "rest": 0.0})
                                is_searching = false
                            }

                            rechnung_e_liste.currentIndex = vars.rechnungSuchenEIndex

                        }
                    }

                })

            }
        })
    }

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        onClicked: {
            view.push(frameSelect)
        }
    }    

    ButtonSelect {
        id: buttonNew
        text: "Neue Rechnung einscannen"
        height: vars.backButtonHeight
        width: vars.backButtonWidth

        anchors.right: parent.right

        onClicked: {
            vars.rechnungID_e = "-1"
            view.push(frameRechnungEAnzeigen)
        }
    }

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    Label {
        id: label_title
        text: "Rechnung Suchen (Eingang)"
        font.pixelSize: vars.fontTitle
        x: parent.width / 2 - width / 2
        y: buttonBack.height / 2 - height / 2
    }

    Rectangle {
        id: rectangle_rechnung_identification
        width: label_rechnung_identification.width + text_rechnung_identification.width
        height: text_rechnung_identification.height

        color: "transparent"
        //border.width: 1
        anchors.top: buttonBack.bottom
        anchors.topMargin: 10
        x: parent.width / 2 - width / 2

        Label {
            id: label_rechnung_identification
            text: "Rechnung n° "
            y: parent.height / 2 - height / 2

            //anchors.top: buttonBack.bottom
            //anchors.topMargin: 10
        }
        TextField {
            id: text_rechnung_identification
            //anchors.top: buttonBack.bottom
            //anchors.topMargin: 10
            selectByMouse: true
            anchors.left: label_rechnung_identification.right
            //enabled: is_searching ? false : true

            onTextChanged: {
                vars.rechnungSuchenEIdentification = text
            }

            onEditingFinished: {
                python.call("libs.BlueFunc.ultra_simple_text", [text], function(txt) {
                    if(focus == true) {
                        vars.rechnungSuchenEIdentification = txt

                        index = 0
                        process = process + 1
                        max_index = 50
                        rechnung_model.clear()
                        suchen(process)
                    }

                })
            }/*
            onAccepted: {
                index = 0
                process = process + 1
                rechnung_model.clear()
                suchen(process)
            }*/
        }

    }

    Rectangle {
        id: rectangle_dokument_id
        width: label_dokument_id.width + text_dokument_id.width
        height: text_dokument_id.height

        color: "transparent"
        anchors.top: rectangle_rechnung_identification.bottom
        anchors.topMargin: 10
        x: parent.width / 2 - width / 2

        Label {
            id: label_dokument_id
            text: "Dokument n° "
            y: parent.height / 2 - height / 2

            //anchors.top: buttonBack.bottom
            //anchors.topMargin: 10
        }
        TextField {
            id: text_dokument_id
            //anchors.top: buttonBack.bottom
            //anchors.topMargin: 10
            selectByMouse: true
            anchors.left: label_dokument_id.right
            //enabled: is_searching ? false : true

            onTextChanged: {
                vars.rechnungSuchenEDokumentID = text
            }

            onEditingFinished: {
                if(focus == true) {
                    python.call("libs.BlueFunc.ultra_simple_text", [text], function(txt) {
                        vars.rechnungSuchenEDokumentID = txt

                        index = 0
                        process = process + 1
                        max_index = 50
                        rechnung_model.clear()
                        suchen(process)
                    })
                }
            }/*
            onAccepted: {
                index = 0
                process = process + 1
                rechnung_model.clear()
                suchen(process)
            }*/
        }
    }


    Rectangle {
        id: rectangle_kunde_identification
        width: label_kunde_identification.width + text_kunde_identification.width
        height: text_kunde_identification.height

        color: "transparent"
        anchors.top: rectangle_dokument_id.bottom
        anchors.topMargin: 10
        x: parent.width / 2 - width / 2

        Label {
            id: label_kunde_identification
            text: "Lieferant "
            y: parent.height / 2 - height / 2

            //anchors.top: buttonBack.bottom
            //anchors.topMargin: 10
        }
        TextField {
            id: text_kunde_identification
            //anchors.top: buttonBack.bottom
            //anchors.topMargin: 10
            selectByMouse: true
            anchors.left: label_kunde_identification.right
            //enabled: is_searching ? false : true

            onTextChanged: {
                vars.rechnungSuchenEKunde = text
            }

            onEditingFinished: {
                if(focus == true) {
                    python.call("libs.BlueFunc.ultra_simple_text", [text], function(txt) {
                        vars.rechnungSuchenEKunde = txt

                        index = 0
                        process = process + 1
                        max_index = 50
                        rechnung_model.clear()
                        suchen(process)
                    })
                }
            }
        }
    }    

    CheckBox {
        id: checkbox_nicht_bezahlt
        anchors.top: rectangle_kunde_identification.bottom
        anchors.topMargin: 10
        text: "Nur Offene Rechnungen Anzeigen"
        checked: vars.rechnungSuchenEOffene
        //enabled: is_searching ? false : true
        onCheckedChanged: {
            if(focus == true) {
                vars.rechnungSuchenEOffene = checked

                index = 0
                process = process + 1
                max_index = 50
                rechnung_model.clear()
                suchen(process)
            }
        }
    }

    BusyIndicator {
        anchors.left: checkbox_nicht_bezahlt.right
        anchors.leftMargin: 10
        anchors.bottom: rectangle_rechnungen.top
        anchors.bottomMargin: 10
        visible: is_searching
    }


    Label {
        text: "Ergebnise: " + rechnung_model.count
        //anchors.top: rectangle_kunde_identification.bottom
        //anchors.topMargin: 10
        anchors.right: parent.right

        anchors.top: rectangle_kunde_identification.bottom
        anchors.topMargin: 10
    }

    Rectangle {
        id: rectangle_rechnungen
        border.width: 1

        anchors.top: checkbox_nicht_bezahlt.bottom
        anchors.topMargin: 10
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        color: "transparent"

        ListView {
            id: rechnung_e_liste

            anchors.fill: parent
            clip: true

            highlight: Rectangle {
                color: "lightblue"
                width: rectangle_rechnungen.width
                height: 30
            }


            ListModel {
                id: rechnung_model
            }

            onAtYEndChanged: {
                if (atYEnd == true) {
                    console.warn("Load more !!")

                    var last_line_identification = rechnung_model.get(rechnung_model.count - 1).identification

                    if (last_line_identification == -1) {
                        rechnung_model.remove(max_index, 1)
                        max_index = max_index + 50

                        process = process + 1
                        suchen(process)
                    } else {
                        console.warn("Nothing to load")
                    }

                }
            }

            Component {
                id: rechnung_delegate
                Item {
                    height: 40
                    width: rectangle_rechnungen.width

                    Rectangle {
                        id: rectangle_line
                        height: 30
                        width: parent.width

                        border.width: 1
                        color: "transparent"


                        MouseArea {
                            id: mousearea_of_rechnung_e
                            anchors.fill: parent
                            acceptedButtons: Qt.LeftButton | Qt.RightButton

                            onClicked: {
                                rechnung_e_liste.currentIndex = index
                                vars.rechnungSuchenEIndex = rechnung_e_liste.currentIndex

                                if (dokument_id == -1) {
                                    rechnung_model.remove(max_index, 1)
                                    max_index = max_index + 50

                                    process = process + 1
                                    suchen(process)
                                } else {
                                    if(mouse.button & Qt.RightButton) {
                                        rechnung_open_pdf(identification)


                                    } else {
                                        process = process + 1

                                        vars.rechnungID_e = identification
                                        view.push(frameRechnungEAnzeigen)

                                    }

                                }
                            }
                        }
                        Label {
                            id: label_rechnung_id
                            text: identification
                            anchors.left: parent.left
                            anchors.leftMargin: 20
                            font.pixelSize: vars.fontTitle
                        }
                        Label {
                            id: label_rechnung_kunde
                            text: lieferant_id + ": " + lieferant_name
                            anchors.left: label_rechnung_id.right
                            anchors.leftMargin: 20
                            anchors.right: label_rechnung_total.left
                            anchors.rightMargin: 20
                            font.pixelSize: vars.fontTitle
                        }

                        Label {
                            id: label_rechnung_total
                            text: total + " €"
                            x: parent.width / 2 - width / 2
                            font.pixelSize: vars.fontTitle
                            color: ist_bezahlt
                        }

                        Label {
                            id: label_rechnung_document_id
                            text: dokument_id
                            anchors.right: label_rechnung_dokument_datum.left
                            anchors.rightMargin: 20
                            font.pixelSize: vars.fontTitle
                        }

                        Label {
                            id: label_rechnung_dokument_datum
                            text: datum
                            anchors.right: label_rechnung_datum.left
                            anchors.rightMargin: 20
                            font.pixelSize: vars.fontTitle
                        }

                        Label {
                            id: label_rechnung_datum
                            text: dokument_datum

                            anchors.right: parent.right
                            anchors.rightMargin: 10
                            font.pixelSize: vars.fontTitle
                        }




                    }
                }
            }
            model: rechnung_model
            delegate: rechnung_delegate

        }
    }


    Component.onCompleted: {
        text_rechnung_identification.text = vars.rechnungSuchenEIdentification
        text_kunde_identification.text = vars.rechnungSuchenEKunde
        text_dokument_id.text = vars.rechnungSuchenEDokumentID

        text_rechnung_identification.forceActiveFocus()

        index = 0
        process = process + 1
        rechnung_model.clear()
        suchen(process)
    }
    /*
    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.BlueFunc', function () {});
            importModule('PrinterSelect', function () {});
            importModule('os', function () {});

            index = 0
            rechnung_model.clear()

            text_rechnung_identification.text = vars.rechnungSuchenEIdentification
            text_kunde_identification.text = vars.rechnungSuchenEKunde
            text_dokument_id.text = vars.rechnungSuchenEDokumentID
        }
    }
    */
}
