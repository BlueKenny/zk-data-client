#!/usr/bin/env python3

import os
import sys
sys.path.append("/etc/zk-data-libs/")
import threading
import time

try: import pyotherside
except: True
import libs.send
import libs.BlueFunc

import os
# get clipboard -> xclip -o
# set clipboard -> echo "TEXT" | xclip -selection clipboard 

sonderzeichen = [" ", "-", ",", "&", ":", ";"]

class Main:    

    def checkClipboard(self):
        print("checkClipboard")
        try: newclipboard = os.popen("xclip -o").readlines()[0]
        except: newclipboard = ""

        print("newclipboard: " + str(newclipboard))
        print("self.oldclipboard: " + str(self.oldclipboard))

        if not self.oldclipboard == newclipboard:
            self.oldclipboard = newclipboard
            for each in sonderzeichen:
                newclipboard = newclipboard.replace(each, "")
            Liste = []
            ArtList = libs.send.SearchArt({"suche": str(newclipboard)})
            print("ArtList: " + str(ArtList))
            for eachID in ArtList:
                ThisArtikel = libs.send.GetArt(eachID)
                
                ThisArtikel["average"] = int(libs.send.get_article_average(eachID))
                
                #if not "P" in ThisArtikel["identification"]:
                Liste.append(ThisArtikel)
               
            pyotherside.send("antwortClipboard", Liste)
            print(Liste)
            
    
    def busy(self, status):
        status = bool(status)
        print("busy = " + str(status))
        pyotherside.send("busy", status)
        
main = Main()

