import QtQuick 2.15
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.0

Rectangle {
    id: window_rechnung_e_anzeigen
    color: vars.colorBackground

    property var mail_to: ""
    property var rechnung_title: ""

    property var rechnung_lieferant_suche: ""

    property var rechnung_rest: 0.0

    property var list_of_files: []

    property var old_key: ""

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    Keys.onPressed: {
        console.warn("key " + event.key + " pressed")
        if ( event.key === Qt.Key_F1 ) {
            vars.lieferscheinSuchen_Angebote = false
            //key_F1()
            view.push(frameLieferscheinSuchen)
        }
        if ( event.key === Qt.Key_F2 ) {
            view.push(frameRechnungSuchen)
        }

        if ( event.key === Qt.Key_F3 ) {
            view.push(frameRechnungESuchen)
        }

        if (old_key === Qt.Key_Control && event.key === Qt.Key_P) {
            var list_files = [];

            for (var i = 0; i < rechnung_e_Model.count; i++) {
                list_files.push(rechnung_e_Model.get(i)["rechnung_pdf"])
            }

            python.call("libs.BlueFunc.create_pdf", [list_files], function(pdf_file) {
                python.call("os.system", ["evince " + pdf_file], function() {

                });
            });
        }

        old_key = event.key
    }


    function load_next_konto(index) {
        for (var i = 0; i < vars.transaction_konto.length; i++) {
            var konto = vars.transaction_konto[i]
            console.warn(i + ": " + konto)
            konto["value"] = konto["identification"]

            if (konto !== -1) {// todo show all if admin
                if (konto["enable"] == true) {
                transaktion_add_Model.append(konto)
                }
            }
            if (transaktion_add_Model.count > 0) {
                if (transaktion_add_Model.count > vars.rechnungAnzeigenZahlungsart) {
                    console.warn("vars.rechnungAnzeigenZahlungsart: " + vars.rechnungAnzeigenZahlungsart)
                    combobox_new_transaktion.currentIndex = vars.rechnungAnzeigenZahlungsart
                }
            }
        }

    }

    Label {
        id: label_rechnung_e_id
        text: vars.rechnungID_e
        x: window_rechnung_e_anzeigen.width / 2 - width / 2
        y: buttonBack.y + buttonBack.height / 2 - height / 2

    }

    ButtonSelect {
        id: buttonBack
        text: "Zurück"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        onClicked: {
            view.push(frameRechnungESuchen)
        }
    }

    Label {
        id: label_rechnung_erstellt_von_benutzer
        text: ""
        anchors.top: parent.top
        anchors.right: button_upload.left
        font.pixelSize: vars.fontText
    }

    ButtonSelect {
        id: button_upload
        text: "Rechnung ?"
        height: vars.backButtonHeight
        width: vars.backButtonWidth

        anchors.right: window_rechnung_e_anzeigen.right
        anchors.rightMargin: 10

        onEnabledChanged: {
            if (enabled == true) {
                button_upload_busy.visible = false
            } else {
                button_upload_busy.visible = true
            }
        }

        BusyIndicator {
            id: button_upload_busy
            anchors.fill: parent
            visible: false
        }

        onClicked: {
            button_upload.enabled = false

            if (vars.rechnungID_e === "-1") {
                // create list of files
                for (var i = 0; i < rechnung_e_Model.count; i++) {
                    console.warn("rechnung_pdf: " + rechnung_e_Model.get(i)["rechnung_pdf"] )
                    list_of_files.push(rechnung_e_Model.get(i)["rechnung_pdf"])
                }
                // use new function libs.BlueFunc.create_pdf(list) to get path of new pdf
                python.call("libs.BlueFunc.create_pdf", [list_of_files], function(pdf_file) {

                    python.call("libs.send.send_file", [vars.serverIP, vars.secure_key, pdf_file], function(pdf_file_link) {

                        //lieferant_model.get(lieferant.currentIndex)["identification"]

                        if (pdf_file_link === "None") {
                            vars.busy = false
                            button_upload.enabled = true
                        } else {                        // create_rechnung_e with lieferant, datum, doc_id and total
                            python.call("libs.send.create_rechnung_e", [vars.serverIP, vars.secure_key, lieferant_model.get(lieferant.currentIndex)["identification"], text_datum.text, text_dokument_id.text, text_total.text, pdf_file_link], function(invoice_dict) {
                                invoice_dict["total_steuern"] = text_total_steuern.text
                                invoice_dict["total_htva"] = text_total_htva.text

                                invoice_dict["ware"] = text_ware.text
                                invoice_dict["unkosten"] = text_unkosten.text
                                invoice_dict["investition"] = text_investition.text
                                invoice_dict["privat"] = text_privat.text

                                invoice_dict["steuern_abziebar"] = text_steuern_abziebar.text
                                invoice_dict["steuern_privat"] = text_steuern_privat.text

                                invoice_dict["info"] = text_info.text

                                // set_rechnung_e
                                python.call("libs.send.set_rechnung_e", [vars.serverIP, vars.secure_key, invoice_dict], function() {
                                    // get_rechnung_e (reload page with blocked data)
                                    vars.rechnungID_e = invoice_dict["identification"]
                                    get_rechnung()
                                    //button_upload.enabled = true
                                })
                            })

                        }

                    })


                });
            } else {
                python.call("libs.send.get_rechnung_e", [vars.serverIP, vars.secure_key, vars.rechnungID_e], function(invoice_dict) {
                    invoice_dict["total_steuern"] = text_total_steuern.text
                    invoice_dict["total_htva"] = text_total_htva.text

                    invoice_dict["ware"] = text_ware.text
                    invoice_dict["unkosten"] = text_unkosten.text
                    invoice_dict["investition"] = text_investition.text
                    invoice_dict["privat"] = text_privat.text

                    invoice_dict["steuern_abziebar"] = text_steuern_abziebar.text
                    invoice_dict["steuern_privat"] = text_steuern_privat.text

                    invoice_dict["info"] = text_info.text

                    // set_rechnung_e
                    python.call("libs.send.set_rechnung_e", [vars.serverIP, vars.secure_key, invoice_dict], function() {
                        // get_rechnung_e (reload page with blocked data)
                        vars.rechnungID_e = invoice_dict["identification"]
                        get_rechnung()
                        //button_upload.enabled = true
                    })

                })
            }
        }
    }


    function get_rechnung() {
        vars.busy = true
        vars.login_timeout = vars.login_max_timeout

        rechnung_e_Model.clear()

        transaktion_add_Model.clear()

        rechnung_rest = 0

        if (vars.rechnungID_e === "-1") {
            button_upload.text = "Rechnung erstellen"
            button_upload.enabled = true

            button_upload.visible = false
            // neue rechnung
            button_datei_scannen.visible = true
            button_datei_scannen.enabled = true

            button_datei_laden.visible = true
            button_datei_laden.enabled = true

            lieferant.visible = true

            lieferant_suche();

            text_lieferant_suche.text = ""

            text_datum.text = ""
            text_dokument_id.text = ""
            text_total.text = "0.0"
            text_total_steuern.text = "0.0"
            text_total_htva.text = "0.0"
            text_ware.text = "0.0"
            text_unkosten.text = "0.0"
            text_investition.text = "0.0"
            text_privat.text = "0.0"
            text_steuern_privat.text = "0.0"
            text_steuern_abziebar.text = "0.0"
            text_info.text = ""

            text_lieferant_suche.enabled = true
            text_datum.enabled = true
            text_dokument_id.enabled = true
            text_total.enabled = true

            dropArea.enabled = true

            rectangle_transaktionen.visible = false

            label_rechnung_erstellt_von_benutzer.text = ""
            label_rechnung_erstellt_von_benutzer.visible = false

            vars.busy = false
        } else {
            button_upload.text = "Rechnung speichern"

            // rechnung laden
            button_datei_scannen.visible = false
            button_datei_scannen.enabled = false

            button_datei_laden.visible = false
            button_datei_laden.enabled = false

            lieferant.visible = false

            text_lieferant_suche.enabled = false
            text_datum.enabled = false
            text_dokument_id.enabled = false
            text_total.enabled = false

            dropArea.enabled = false

            python.call("libs.send.get_rechnung_e", [vars.serverIP, vars.secure_key, vars.rechnungID_e], function(rechnung_e_dict) {                
                text_lieferant_suche.text = rechnung_e_dict["lieferant_id"]

                text_datum.text = rechnung_e_dict["dokument_datum"]
                text_dokument_id.text = rechnung_e_dict["dokument_id"]
                text_total.text = rechnung_e_dict["total"]
                text_total_steuern.text = rechnung_e_dict["total_steuern"]
                text_total_htva.text = rechnung_e_dict["total_htva"]
                text_ware.text = rechnung_e_dict["ware"]
                text_unkosten.text = rechnung_e_dict["unkosten"]
                text_investition.text = rechnung_e_dict["investition"]
                text_privat.text = rechnung_e_dict["privat"]
                text_steuern_privat.text = rechnung_e_dict["steuern_privat"]
                text_steuern_abziebar.text = rechnung_e_dict["steuern_abziebar"]
                text_info.text = rechnung_e_dict["info"]

                rechnung_rest = -rechnung_e_dict["rest"]

                label_rechnung_erstellt_von_benutzer.text = "Erstellt von " + rechnung_e_dict["created_by"]
                label_rechnung_erstellt_von_benutzer.visible = true

                python.call("libs.send.GetKunden", [vars.serverIP, vars.secure_key, rechnung_e_dict["lieferant_id"]], function(client) {
                    var sepa = ""
                    if (client["sepa"] == true) {
                        sepa = "SEPA"
                    } else {
                        sepa = ""
                    }
                    // pdf laden
                    var identification_to_string = rechnung_e_dict["identification"].toString().replace(".0", "")
                    python.call("libs.send.get_invoice_e_pdf", [vars.serverIP, vars.secure_key, identification_to_string], function(link) {
                        python.call("libs.BlueFunc.pdf_to_ppm", ["http://" + vars.serverIP + ":51515" + link, vars.rechnungID_e + " " + sepa], function(answer_list) {
                            console.warn("answer_list: " + answer_list)

                            for (var i = 0; i < answer_list.length; i++) {
                                rechnung_e_Model.append({"rechnung_pdf": answer_list[i]})
                            }
                            button_upload.enabled = true
                            vars.busy = false
                        });
                    })


                    /*
                    python.call("libs.send.print_invoice_e", [vars.serverIP, vars.secure_key, rechnung_e_dict["identification"]], function(link) {
                        python.call("libs.BlueFunc.pdf_to_ppm", [link, vars.rechnungID_e + " " + sepa], function(answer_list) {
                            console.warn("answer_list: " + answer_list)

                            for (var i = 0; i < answer_list.length; i++) {
                                rechnung_e_Model.append({"rechnung_pdf": answer_list[i]})
                            }
                            button_upload.enabled = true
                            vars.busy = false
                        });
                    })
                    */


                })


                text_new_transaktion_wert.enabled = true
                text_new_transaktion_pin.enabled = true



            });

            load_next_konto()
            rectangle_transaktionen.visible = true

            transaktion_Model.clear()
            python.call("libs.send.search_transaktion_by_rechnung", [vars.serverIP, vars.secure_key, "RE" + vars.rechnungID_e], function(answer_list) {
                answer_list = answer_list.split("|")

                for (var i = 0; i < answer_list.length; i++) {
                    print("i: " + answer_list[i])

                    python.call("libs.send.get_transaktion", [vars.serverIP, vars.secure_key,  answer_list[i] ], function(transaktion_item) {
                        console.warn("transaktion_item: " + transaktion_item["identification"])
                        transaktion_item["identification"] = transaktion_item["identification"].toString()
                        transaktion_Model.append(transaktion_item)
                    })
                }
                transaktion_Model.append({"datum": "",
                                             "identification": "",
                                             "uhrzeit": "",
                                             "konto_name": "Restbetrag (zu bekommen): ",
                                             "betrag": rechnung_rest,
                                             "user": "",
                                             "mitteilung": ""
                                         })
            })

        }
    }

    function lieferant_suche() {
        lieferant_model.clear()
        text_lieferant_suche.enabled = false

        python.call("libs.send.SearchKunden", [vars.serverIP, vars.secure_key, {"supplier": true, "name": rechnung_lieferant_suche}], function(list_of_ids) {

            if (list_of_ids.length < 25) {
                for (var i = 0; i < list_of_ids.length; i++) {
                    text_lieferant_suche.enabled = false
                    python.call("libs.send.GetKunden", [vars.serverIP, vars.secure_key, list_of_ids[i]], function(answer_kunden) {
                        lieferant_model.append(answer_kunden)


                        if (list_of_ids.length === 1) {
                            text_lieferant_suche.text = answer_kunden["name"]
                           // lieferant.displayText = answer_kunden["name"]
                            lieferant.currentIndex = 0

                        }

                        if (list_of_ids.length === i-1) {
                            text_lieferant_suche.enabled = true

                        }

                    });

                }

            }

            text_lieferant_suche.enabled = true

        });
    }



    FileDialog {
        id: fileDialog
        title: "Datei hinzufügen"
        folder: shortcuts.home

        selectMultiple: false
        nameFilters: [ "pdf (*.pdf *.PDF)"]

        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrl)

            python.call("libs.BlueFunc.pdf_to_ppm", [fileDialog.fileUrl.toString(), ""], function(answer_list) {
                console.warn("answer_list: " + answer_list)

                for (var i = 0; i < answer_list.length; i++) {
                    rechnung_e_Model.append({"rechnung_pdf": answer_list[i]})
                }
            });
        }
    }

    DropArea {
            id: dropArea;
            anchors.fill: parent
            enabled: false
            onEntered: {
                rectangle_rechnungen.color = "lightgreen";
                drag.accept (Qt.LinkAction);
            }
            onDropped: {
                console.log(drop.urls)
                rectangle_rechnungen.color = "white"

                for (var i = 0; i < drop.urls.length; i++) {
                    console.log("You chose: " + drop.urls[i])

                    python.call("libs.BlueFunc.pdf_to_ppm", [drop.urls[i].toString(), ""], function(answer_list) {
                        console.warn("answer_list: " + answer_list)

                        for (var i = 0; i < answer_list.length; i++) {
                            rechnung_e_Model.append({"rechnung_pdf": answer_list[i]})
                        }
                    });
                }
            }
            onExited: {
                rectangle_rechnungen.color = "white";
            }
    }

    Rectangle {
        id: rectangle_rechnungen
        width: parent.width / 100 * vars.rechnungSuchenEZoom
        //height: window_rechnung_e_anzeigen.height
        anchors.top: buttonBack.bottom
        anchors.topMargin: 10
        anchors.bottom: window_rechnung_e_anzeigen.bottom


        border.width: 1

        ListView {
            id: list_rechnungen
            anchors.fill: parent

            clip: true

            ListModel {
                id: rechnung_e_Model

                onCountChanged: {

                    if (vars.rechnungID_e == -1) {
                        // create list of files
                       if (count == 1) {
                           var new_list = []
                           for (var i = 0; i < rechnung_e_Model.count; i++) {
                               new_list.push(rechnung_e_Model.get(i)["rechnung_pdf"])
                           }

                            python.call("libs.rechnung_e_scan.tesseract_txt", [new_list], function(answer_txt) {
                                if (answer_txt == "") {
                                    console.warn("Error, tesseract not working ?")
                                } else {
                                    python.call("libs.rechnung_e_scan.scan", [answer_txt], function(answer_list) {
                                        text_lieferant_suche.forceActiveFocus()
                                        text_lieferant_suche.text = answer_list[0]

                                        text_datum.text = answer_list[1]

                                        text_dokument_id.forceActiveFocus()
                                        text_dokument_id.text = answer_list[2]

                                        text_total.text = answer_list[3]

                                        text_total_steuern.forceActiveFocus()
                                        text_total_steuern.text = answer_list[4]


                                        if (text_lieferant_suche.text == "?") {
                                            text_lieferant_suche.forceActiveFocus()
                                        }

                                    })
                                }
                            })
                       }

                       if (count == 0) {
                           button_upload.visible = false
                       } else {
                           button_upload.visible = true
                       }
                    }

                }
            }


            delegate: rechnung_e_Delegate
            model: rechnung_e_Model

            //highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
            focus: true

            Component {
                id: rechnung_e_Delegate
                Item {
                    id: rechnung_e_item
                    width: rectangle_rechnungen.width
                    height: width*1.7

                    Text {
                        id: rechnung_e_text
                        anchors.left: rechnung_e_item.left
                        anchors.leftMargin: 10
                        text: rechnung_pdf
                    }
                    Button {
                        id: button_delete

                        anchors.top: rechnung_e_text.bottom
                        anchors.topMargin: 10
                        anchors.left: rechnung_e_item.left
                        anchors.leftMargin: 10

                        width: 100
                        text: "Löschen"

                        onClicked: {
                            rechnung_e_Model.remove(index)
                        }

                    }
                    Button {
                        id: button_up

                        text: "⬆"
                        width: 100

                        anchors.top: rechnung_e_text.bottom
                        anchors.topMargin: 10
                        anchors.left: button_delete.right
                        anchors.leftMargin: 10

                        onClicked: {
                            console.warn("index: " + index)
                            console.warn("move up")
                            rechnung_e_Model.move(index, index - 1, 1)
                        }
                    }
                    Button {
                        id: button_down

                        text: "⬇"
                        width: 100

                        anchors.top: rechnung_e_text.bottom
                        anchors.topMargin: 10
                        anchors.left: button_up.right
                        anchors.leftMargin: 10

                        onClicked: {
                            console.warn("index: " + index)
                            console.warn("move down")
                            rechnung_e_Model.move(index, index + 1, 1)
                        }
                    }

                    Image {
                        id: rechnung_e_img
                        anchors.fill: rechnung_e_item
                        fillMode: Image.PreserveAspectFit

                        source: rechnung_pdf //"/etc/zk-data/qml/DATA/stock.png"
                        // pdftoppm 1.pdf 1.png
                    }

                    Component.onCompleted: {
                        if (vars.rechnungID_e === "-1") {
                            button_delete.visible = true
                            button_up.visible = true
                            button_down.visible = true
                        } else {
                            button_delete.visible = false
                            button_up.visible = false
                            button_down.visible = false
                        }

                    }


                }
            }
        }
    }

    Rectangle {
        id: rectangle_data
        anchors.left: rectangle_rechnungen.right
        anchors.right: window_rechnung_e_anzeigen.right

        //height: window_rechnung_e_anzeigen.height
        anchors.top: buttonBack.bottom
        anchors.topMargin: 10
        anchors.bottom: window_rechnung_e_anzeigen.bottom

        border.width: 1

        color: "transparent"

        onWidthChanged: {

        }


        Label {
            id: text_lieferant
            text: "Lieferant: "
            anchors.left: rectangle_data.left
            anchors.leftMargin: 10
            anchors.top: rectangle_data.top
            anchors.topMargin: 10
            font.pixelSize: vars.fontTitle
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    text_lieferant_suche.forceActiveFocus()
                }
            }
        }


        TextInput {
            id: text_lieferant_suche
            width: 350
            //background: Rectangle {anchors.fill: parent; color: "white"}
            selectByMouse: true
            anchors.left: text_lieferant.right
            anchors.leftMargin: 10
            anchors.top: rectangle_data.top
            anchors.topMargin: 10
            activeFocusOnTab: true

            onFocusChanged: {
                if (focus == true) {
                    selectAll()
                }
            }

            onTextChanged: {
                if (focus == true) {
                    if (text.length > 2) {
                        rechnung_lieferant_suche = text
                        lieferant_suche();
                    }
                }
            }
        }


        ComboBox {
            id: lieferant
            model: lieferant_model
            textRole: "name"
            ListModel {
                id: lieferant_model
            }
            anchors.left: text_lieferant_suche.left
            anchors.leftMargin: 10
            anchors.right: rectangle_data.right
            anchors.rightMargin: 10
            anchors.top: text_lieferant_suche.bottom
            anchors.topMargin: 10

            onDisplayTextChanged: {
                text_lieferant_suche.text = displayText
                rechnung_lieferant_suche = displayText

            }

        }


        Label {
            id: label_datum
            text: "Datum: "
            anchors.left: rectangle_data.left
            anchors.leftMargin: 10
            anchors.top: lieferant.bottom
            anchors.topMargin: 10
        }
        TextInput {
            id: text_datum
            width: 150
            //background: Rectangle {anchors.fill: parent; color: "white"}
            selectByMouse: true
            anchors.left: label_datum.right
            anchors.leftMargin: 10
            anchors.top: lieferant.bottom
            anchors.topMargin: 10
            activeFocusOnTab: true

            onFocusChanged: {
                if (focus == true) {
                    selectAll()
                }
            }
        }

        Label {
            id: label_dokument_id
            text: "Dokument n°: "
            anchors.left: rectangle_data.left
            anchors.leftMargin: 10
            anchors.top: text_datum.bottom
            anchors.topMargin: 10
        }
        TextInput {
            id: text_dokument_id
            width: 150
            //background: Rectangle {anchors.fill: parent; color: "white"}
            selectByMouse: true
            anchors.left: label_dokument_id.right
            anchors.leftMargin: 10
            anchors.top: text_datum.bottom
            anchors.topMargin: 10
            activeFocusOnTab: true

            onFocusChanged: {
                if (focus == true) {
                    selectAll()
                }
            }

            onTextChanged: {
                text_dokument_id.enabled = false
                python.call("libs.BlueFunc.ultra_simple_text", [text_dokument_id.text], function(txt) {
                    text_dokument_id.text = txt


                    python.call("libs.send.search_invoice_e", [vars.serverIP, vars.secure_key, "", text_dokument_id.text, text_lieferant_suche.text, false, "", 0], function(answer) {
                        if (answer == -1) {
                            text_dokument_id.color = "black"
                        } else {
                            text_dokument_id.color = "red"
                        }
                        text_dokument_id.enabled = true
                    });
                    //text_dokument_id.cursorPosition = -1
                    //text_dokument_id.
                });
            }
        }

        Label {
            id: label_total
            text: "Summe: "
            anchors.left: rectangle_data.left
            anchors.leftMargin: 10
            anchors.top: text_dokument_id.bottom
            anchors.topMargin: 50
        }
        TextInput {
            id: text_total
            width: 150
            //background: Rectangle {anchors.fill: parent; color: "white"}
            selectByMouse: true
            anchors.left: label_total.right
            anchors.leftMargin: 10
            anchors.top: text_dokument_id.bottom
            anchors.topMargin: 50
            text: "0"
            activeFocusOnTab: true

            onFocusChanged: {
                if (focus == true) {
                    selectAll()
                }
            }
        }

          ////////////////////////////////////////////////////////

        Rectangle {
            id: rectangle_trennung
            height: 1

            anchors.top: text_total.bottom
            anchors.topMargin: 10

            anchors.left: rectangle_data.left
            anchors.leftMargin: 10
            anchors.right: rectangle_data.right
            anchors.rightMargin: 10
            color: "red"
        }

        ScrollView {
            id: details

            clip: true

            anchors.top: rectangle_trennung.bottom
            anchors.topMargin: 10

            anchors.bottom: button_datei_laden.top
            anchors.bottomMargin: 10

            width: parent.width

            contentWidth: width
            contentHeight: text_new_transaktion_pin.y + text_new_transaktion_pin.height + listview_transaktion.height + 400

            ScrollBar.vertical: ScrollBar {
                width: 15
                height: details.height
                anchors.right: parent.right
                policy: ScrollBar.AlwaysOn
            }

            Label {
                id: label_total_steuern
                text: "Steuern: "
                anchors.left: parent.left//rectangle_data.left
                anchors.leftMargin: 10
                anchors.top: rectangle_trennung.bottom
                anchors.topMargin: 10
            }
            TextInput {
                id: text_total_steuern
                width: 150
                //background: Rectangle {anchors.fill: parent; color: "white"}
                selectByMouse: true
                anchors.left: label_total_steuern.right
                anchors.leftMargin: 10
                anchors.top: rectangle_trennung.bottom
                anchors.topMargin: 10
                text: "0"
                activeFocusOnTab: true

                onFocusChanged: {
                    if (focus == true) {
                        selectAll()
                    }
                }

                onTextChanged: {
                    if (focus == true) {
                        text_total_htva.text = (parseFloat(text_total.text) - parseFloat(text_total_steuern.text)).toFixed(2)
                        text_steuern_abziebar.text = text_total_steuern.text
                        text_steuern_privat.text = "0"
                    }
                    text_ware.text = (parseFloat(text_total_htva.text) - parseFloat(text_unkosten.text) - parseFloat(text_investition.text) - parseFloat(text_privat.text)).toFixed(2)
                }
            }
            Label {
                id: label_total_htva
                text: "Summe ohne MwSt: "
                anchors.left: rectangle_data.left
                anchors.leftMargin: 10
                anchors.top: text_total_steuern.bottom
                anchors.topMargin: 10
            }
            TextInput {
                id: text_total_htva
                width: 150
                //background: Rectangle {anchors.fill: parent; color: "white"}
                selectByMouse: true
                anchors.left: label_total_htva.right
                anchors.leftMargin: 10
                anchors.top: text_total_steuern.bottom
                anchors.topMargin: 10
                text: "0"
                activeFocusOnTab: true

                onFocusChanged: {
                    if (focus == true) {
                        selectAll()
                    }
                }

                onTextChanged: {
                    if (focus == true) {
                        text_total_steuern.text = (parseFloat(text_total.text) - parseFloat(text_total_htva.text)).toFixed(2)
                    }
                }
            }


            Label {
                id: label_ware
                text: "Summe ohne MwSt: Ware"
                anchors.left: rectangle_data.left
                anchors.leftMargin: 10
                anchors.top: text_total_htva.bottom
                anchors.topMargin: 50
            }
            TextInput {
                id: text_ware
                width: 150
                //background: Rectangle {anchors.fill: parent; color: "white"}
                selectByMouse: true
                anchors.left: label_ware.right
                anchors.leftMargin: 10
                anchors.top: text_total_htva.bottom
                anchors.topMargin: 50
                text: "0"
                activeFocusOnTab: true
                enabled: false

                onTextChanged: {
                    if (focus == true) {
                        text_ware.text = (parseFloat(text_total_htva.text) - parseFloat(text_unkosten.text) - parseFloat(text_investition.text) - parseFloat(text_privat.text)).toFixed(2)
                    }
                }

            }
            Label {
                id: label_unkosten
                text: "Summe ohne MwSt: Unkosten"
                anchors.left: rectangle_data.left
                anchors.leftMargin: 10
                anchors.top: text_ware.bottom
                anchors.topMargin: 10
            }
            TextInput {
                id: text_unkosten
                width: 150
                //background: Rectangle {anchors.fill: parent; color: "white"}
                selectByMouse: true
                anchors.left: label_unkosten.right
                anchors.leftMargin: 10
                anchors.top: text_ware.bottom
                anchors.topMargin: 10
                text: "0"
                activeFocusOnTab: true

                onFocusChanged: {
                    if (focus == true) {
                        selectAll()
                    }
                }

                onTextChanged: {
                    if (focus == true) {
                        text_ware.text = (parseFloat(text_total_htva.text) - parseFloat(text_unkosten.text) - parseFloat(text_investition.text) - parseFloat(text_privat.text)).toFixed(2)
                    }
                }
            }
            Label {
                id: label_investition
                text: "Summe ohne MwSt: Investition"
                anchors.left: rectangle_data.left
                anchors.leftMargin: 10
                anchors.top: text_unkosten.bottom
                anchors.topMargin: 10
            }
            TextInput {
                id: text_investition
                width: 150
                //background: Rectangle {anchors.fill: parent; color: "white"}
                selectByMouse: true
                anchors.left: label_investition.right
                anchors.leftMargin: 10
                anchors.top: text_unkosten.bottom
                anchors.topMargin: 10
                text: "0"
                activeFocusOnTab: true

                onFocusChanged: {
                    if (focus == true) {
                        selectAll()
                    }
                }

                onTextChanged: {
                    if (focus == true) {
                        text_ware.text = (parseFloat(text_total_htva.text) - parseFloat(text_unkosten.text) - parseFloat(text_investition.text) - parseFloat(text_privat.text)).toFixed(2)
                    }
                }
            }
            Label {
                id: label_privat
                text: "Summe ohne MwSt: Privat"
                anchors.left: rectangle_data.left
                anchors.leftMargin: 10
                anchors.top: text_investition.bottom
                anchors.topMargin: 10
            }
            TextInput {
                id: text_privat
                width: 150
                selectByMouse: true
                anchors.left: label_privat.right
                anchors.leftMargin: 10
                anchors.top: text_investition.bottom
                anchors.topMargin: 10
                text: "0"
                activeFocusOnTab: true

                onFocusChanged: {
                    if (focus == true) {
                        selectAll()
                    }
                }

                onTextChanged: {
                    if (focus == true) {
                        text_ware.text = (parseFloat(text_total_htva.text) - parseFloat(text_unkosten.text) - parseFloat(text_investition.text) - parseFloat(text_privat.text)).toFixed(2)
                    }
                }
            }

            Label {
                id: label_steuern_privat
                text: "Steuern: nicht abziebar"
                anchors.left: rectangle_data.left
                anchors.leftMargin: 10
                anchors.top: text_privat.bottom
                anchors.topMargin: 50
            }
            TextInput {
                id: text_steuern_privat
                width: 150
                //background: Rectangle {anchors.fill: parent; color: "white"}
                selectByMouse: true
                anchors.left: label_steuern_privat.right
                anchors.leftMargin: 10
                anchors.top: text_privat.bottom
                anchors.topMargin: 50
                text: "0"
                activeFocusOnTab: true

                onFocusChanged: {
                    if (focus == true) {
                        selectAll()
                    }
                }

                onTextChanged: {
                    if (focus == true) {
                        text_steuern_abziebar.text = (parseFloat(text_total_steuern.text) - parseFloat(text_steuern_privat.text)).toFixed(2)
                    }
                }
            }
            Label {
                id: label_steuern_abziebar
                text: "Steuern: abziebar"
                anchors.left: rectangle_data.left
                anchors.leftMargin: 10
                anchors.top: text_steuern_privat.bottom
                anchors.topMargin: 10
            }
            TextInput {
                id: text_steuern_abziebar
                width: 150
                //background: Rectangle {anchors.fill: parent; color: "white"}
                selectByMouse: true
                anchors.left: label_steuern_abziebar.right
                anchors.leftMargin: 10
                anchors.top: text_steuern_privat.bottom
                anchors.topMargin: 10
                text: "0"
                activeFocusOnTab: true

                onFocusChanged: {
                    if (focus == true) {
                        selectAll()
                    }
                }

                onTextChanged: {
                    if (focus == true) {
                        text_steuern_privat.text = (parseFloat(text_total_steuern.text) - parseFloat(text_steuern_abziebar.text)).toFixed(2)
                    }
                }
            }

            Label {
                id: label_info
                text: "Notiz / Info"
                anchors.left: rectangle_data.left
                anchors.leftMargin: 10
                anchors.top: text_steuern_abziebar.bottom
                anchors.topMargin: 50
            }
            TextInput {
                id: text_info
                //background: Rectangle {anchors.fill: parent; color: "white"}
                selectByMouse: true
                anchors.left: label_info.right
                anchors.leftMargin: 10
                anchors.right: rectangle_data.right
                anchors.top: text_steuern_abziebar.bottom
                anchors.topMargin: 50
                activeFocusOnTab: true
                height: 80

                onFocusChanged: {
                    if (focus == true) {
                        selectAll()
                    }
                }
            }

            Rectangle {
                id: rectangle_transaktionen
                height: 400

                anchors.top: text_info.bottom
                anchors.topMargin: 50

                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                anchors.rightMargin: 10


                onWidthChanged: {
                    var test_width = label_transaktion_add_title.width + combobox_new_transaktion.width + text_new_transaktion_wert.width + text_new_transaktion_pin.width + 50

                    if (width < test_width) {
                        combobox_new_transaktion.anchors.top = label_transaktion_add_title.bottom
                        combobox_new_transaktion.anchors.topMargin = 10
                        combobox_new_transaktion.anchors.left = rectangle_transaktionen.left
                        combobox_new_transaktion.anchors.leftMargin = 10
                        combobox_new_transaktion.anchors.rght = rectangle_transaktionen.right
                        combobox_new_transaktion.anchors.rightMargin = 10

                        text_new_transaktion_wert.anchors.left = rectangle_transaktionen.left
                        text_new_transaktion_wert.anchors.leftMargin = 10
                        text_new_transaktion_wert.anchors.top = combobox_new_transaktion.bottom
                        text_new_transaktion_wert.anchors.topMargin = 10

                        text_new_transaktion_pin.anchors.left = rectangle_transaktionen.left
                        text_new_transaktion_pin.anchors.leftMargin = 10
                        text_new_transaktion_pin.anchors.top = text_new_transaktion_wert.bottom
                        text_new_transaktion_pin.anchors.topMargin = 10

                    } else {
                        combobox_new_transaktion.anchors.top = rectangle_transaktionen.top
                        combobox_new_transaktion.anchors.topMargin = 10
                        combobox_new_transaktion.anchors.left = label_transaktion_add_title.right
                        combobox_new_transaktion.anchors.leftMargin = 10
                        combobox_new_transaktion.anchors.rght = rectangle_transaktionen.right
                        combobox_new_transaktion.anchors.rightMargin = 10

                        text_new_transaktion_wert.anchors.left = combobox_new_transaktion.right
                        text_new_transaktion_wert.anchors.leftMargin = 10
                        text_new_transaktion_wert.anchors.top = rectangle_transaktionen.top
                        text_new_transaktion_wert.anchors.topMargin = 10

                        text_new_transaktion_pin.anchors.left = text_new_transaktion_wert.right
                        text_new_transaktion_pin.anchors.leftMargin = 10
                        text_new_transaktion_pin.anchors.top = rectangle_transaktionen.top
                        text_new_transaktion_pin.anchors.topMargin = 10

                    }
                }

                Label {
                    id: label_transaktion_add_title
                    text: "Neue Zahlung hinzufügen"
                    anchors.left: rectangle_transaktionen.left
                    anchors.leftMargin: 10
                    anchors.top: rectangle_transaktionen.top
                    anchors.topMargin: 10
                }

                ComboBox {
                    id: combobox_new_transaktion
                    model: transaktion_add_Model//["Barzahlung", "Bankkontakt"]
                    y: parent.height / 2 - height / 2
                    height: 40//button_vorschau.height / 2
                    textRole: "name"

                    ListModel {
                        id: transaktion_add_Model
                    }
                    onCurrentIndexChanged: {
                        if (focus == true) {
                            text_new_transaktion_wert.forceActiveFocus()
                            vars.rechnungAnzeigenZahlungsart = currentIndex
                            console.warn("vars.rechnungAnzeigenZahlungsart: " + vars.rechnungAnzeigenZahlungsart)
                        }
                    }
                }

                TextField {
                    id: text_new_transaktion_wert
                    y: parent.height / 2 - height / 2
                    selectByMouse: true
                    placeholderText: "Summe"
                    width: 80
                    inputMethodHints: Qt.ImhDigitsOnly
                    enabled: false
                    //validator: DoubleValidator {}
                    onAccepted: {
                        if (text == "") {
                            text = rechnung_rest
                        }
                        text_new_transaktion_pin.forceActiveFocus()
                    }
                }
                TextField {
                    id: text_new_transaktion_pin
                    y: parent.height / 2 - height / 2
                    placeholderText: "Pin"
                    selectByMouse: true
                    width: 80
                    echoMode: TextInput.Password
                    enabled: false
                    //visible: text_new_transaktion_wert.acceptableInput ? true : false
                    onVisibleChanged: {
                        text_new_transaktion_pin.text = ""
                    }
                    onAccepted: {
                        text_new_transaktion_wert.enabled = false
                        text_new_transaktion_pin.enabled = false
                        python.call("libs.send.create_transaktion", [vars.serverIP, vars.secure_key, transaktion_add_Model.get(combobox_new_transaktion.currentIndex).identification, text_new_transaktion_wert.text, "RE" + vars.rechnungID_e, text_new_transaktion_pin.text], function(sucess) {
                            if (sucess == true) {
                                text_new_transaktion_pin.clear()
                                text_new_transaktion_wert.clear()
                                get_rechnung()
                            } else {
                                text_new_transaktion_pin.clear()
                            }
                        })
                    }
                }



                ListView {
                    id: listview_transaktion

                    clip: true

                    anchors.top: text_new_transaktion_pin.bottom
                    anchors.topMargin: 10

                    width: rectangle_transaktionen.width
                    height: 200

                    model: transaktion_Model
                    delegate: transaktion_Delegate
                    ListModel {
                        id: transaktion_Model
                    }

                    Component {
                        id: transaktion_Delegate
                        Item {
                            height: 35
                            width: parent.width

                            Label {
                                id: label_transaktion_identification
                                text: "T" + identification.toString()
                                font.pixelSize: vars.fontText + 5
                                anchors.left: parent.left
                                anchors.leftMargin: 10
                            }
                            Label {
                                id: label_transaktion_datum
                                text: datum + " - " + uhrzeit
                                font.pixelSize: vars.fontText + 5
                                anchors.left: label_transaktion_identification.right
                                anchors.leftMargin: 50
                            }
                            Label {
                                id: label_transaktion_konto_name
                                text: konto_name
                                anchors.left: label_transaktion_datum.right
                                anchors.leftMargin: 50
                                font.pixelSize: vars.fontText + 5
                            }
                            Label {
                                id: label_transaktion_betrag
                                text: betrag + " €"
                                anchors.left: label_transaktion_konto_name.right
                                anchors.leftMargin: 50
                                font.pixelSize: vars.fontText + 5
                            }
                            Label {
                                id: label_transaktion_mitteilung
                                text: mitteilung
                                anchors.left: label_transaktion_betrag.right
                                anchors.leftMargin: 50
                                font.pixelSize: vars.fontText + 5
                            }
                            Label {
                                id: label_transaktion_user
                                text: user
                                anchors.left: label_transaktion_mitteilung.right
                                anchors.leftMargin: 50
                                font.pixelSize: vars.fontText + 5
                            }
                        }
                    }
                }




            }

        }

        Button {
            id: button_datei_laden
            text: "Datei hinzufügen"

            anchors.bottom: text_zoom.top
            anchors.bottomMargin: 10
            anchors.right: button_datei_scannen.left
            anchors.rightMargin: 10

            onClicked: {
                fileDialog.visible = true
            }
        }
        Button {
            id: button_datei_scannen
            text: "Datei Scannen"

            anchors.bottom: text_zoom.top
            anchors.bottomMargin: 10
            anchors.right: rectangle_data.right
            anchors.rightMargin: 10
            enabled: false

            onEnabledChanged: {
                if (enabled == true) {
                    button_datei_scannen_busy.visible = false
                } else {
                    button_datei_scannen_busy.visible = true
                }
            }

            BusyIndicator {
                id: button_datei_scannen_busy
                anchors.fill: parent
            }
            onClicked: {
                button_datei_scannen.enabled = false
                vars.busy = true

                python.call("libs.BlueFunc.timestamp", [], function(timestmp) {
                    python.call("libs.BlueFunc.BlueMkDir", ["/tmp/zk-data-scanned-file/"], function() {
                        python.call("libs.scan.scan_to_png", [vars.scanner_name, "/tmp/zk-data-scanned-file/" + timestmp + ".png"], function() {
                            rechnung_e_Model.append({"rechnung_pdf": "/tmp/zk-data-scanned-file/" + timestmp + ".png"})
                            button_datei_scannen.enabled = true
                            vars.busy = false
                        });
                    });
                });
            }
        }

        Text {
            id: text_zoom

            text: "Zoom"

            anchors.bottom: slider_zoom.top
            anchors.bottomMargin: 10

            anchors.left: slider_zoom.left
            anchors.leftMargin: 10
        }

        Slider {
            id: slider_zoom

            live: true
            from: 10
            to: 70

            value: vars.rechnungSuchenEZoom

            anchors.bottom: rectangle_data.bottom
            anchors.bottomMargin: 10

            width: window_rechnung_e_anzeigen.width / 10 * 1.8

            anchors.right: rectangle_data.right
            anchors.rightMargin: 10

            onValueChanged: {
                vars.rechnungSuchenEZoom = value

            }
        }

    }

    Busy {anchors.fill: parent}

    Component.onCompleted: {
        get_rechnung()
        vars.busy = false
    }
    /*
    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.scan', function () {});
            importModule('libs.BlueFunc', function () {});
            importModule('os', function () {});
            importModule('libs.rechnung_e_scan', function () {});


            get_rechnung()

            vars.busy = false
        }
    }*/

}
