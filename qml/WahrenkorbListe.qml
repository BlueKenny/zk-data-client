import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.0
import QtQuick.Window 2.0
import QtQuick.Dialogs 1.0// NEW


Rectangle {
    id: window
    height: Screen.height; width: Screen.width

    color: colorDialog.color//"white"//vars.colorBackground

    Item {
        id: variable
        property int pos : 0
        property bool highlighter: false
    }

    //gradient: Gradient {
    //    GradientStop { position: 0.2; color: "lightsteelblue" }
    //    GradientStop { position: 1.5; color: "steelblue" }
    //}

    function addItemToList(item) {
        for (var i=0; i<item.length; i++) {
            console.warn("addItemToList: " + item[i]["name_de"])
            contactModel.append(item[i]);
            liste.currentIndex = liste.count - 1
        }
    }

    function changeStock(item, quantity) {
        console.warn("changeStock(" + item + ", " + quantity + ")")
        python.call("libs.send.AddArt", [vars.serverIP, vars.secure_key, item, quantity], function(sucess) {
            if (sucess == true) {
                if (quantity == "-1") {contactModel.get(liste.currentIndex).anzahl -= 1}
                if (quantity == "+1") {contactModel.get(liste.currentIndex).anzahl += 1}
            }
        })

    }

    ListView {
        id: liste
        anchors.top: buttonColor.bottom
        anchors.topMargin: 10
        height: parent.height - y
        width: parent.width

        focus: true
        highlightMoveDuration: 0
        highlight: Rectangle { color: "lightgrey"; width: window.width; visible: variable.highlighter ? true : false}

        clip: true
            /*
        ScrollBar.vertical: ScrollBar {
            active: true;
            //policy: ScrollBar.AlwaysOn
            width: window.width / 35//vars.scrollbarWidth
        }*/

        ListModel {
            id: contactModel
        }
        Component {
            id: contactDelegate
            Item {
                id: itemListe
                width: liste.width
                height: liste.height / 20//vars.listItemHeight

                PropertyAnimation {
                    id: animationStockAdd
                    target: textAnzahl
                    property: "scale"
                    to: 0
                    duration: 200
                    onStopped: animationStockAdded.start()
                }
                PropertyAnimation {
                    id: animationStockAdded
                    target: textAnzahl
                    property: "scale"
                    to: 1
                    duration: 200
                }

                Keys.onPressed: {
                    if (variable.highlighter == true) {
                        if ( event.key === Qt.Key_F1 ) {
                            timerAddItem.stop(); timerWait.stop();timerWait.start()
                            liste.currentIndex = index
                            variable.highlighter = true
                            changeStock(identification, "-1")
                            animationStockAdd.start()
                        }
                        if ( event.key === Qt.Key_F2 ) {
                            timerAddItem.stop(); timerWait.stop();timerWait.start()
                            liste.currentIndex = index
                            variable.highlighter = true
                            changeStock(identification, "+1")
                            animationStockAdd.start()
                        }
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        timerAddItem.stop()
                        timerWait.stop()
                        timerWait.start()
                        liste.currentIndex = index
                        variable.highlighter = true
                    }
                }

                Label {
                    id: textIdentification
                    text: identification
                    width: liste.width / 20
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                    color: user_color
                }

                Label {
                    id: textAnzahl
                    text: anzahl + "x"
                    width: liste.width / 10
                    x: window.width / 10 * 2 - width / 2
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                    color: user_color
                }

                Label {
                    id: textName
                    text: name_de
                    width: liste.width / 10
                    x: window.width / 10 * 4 - width / 2
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                    color: user_color
                }

                Label {
                    id: textArtikel
                    text: artikel
                    width: liste.width / 10
                    x: window.width / 10 * 8 - width / 2
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                    color: user_color
                }

                Label {
                    id: textOrt
                    text: ort
                    width: liste.width / 10
                    x: window.width - width
                    y: parent.height / 2 - height / 2
                    font.pixelSize: 20//vars.fontText
                    color: user_color
                }


            }
        }
        model: contactModel
        delegate: contactDelegate
    }

    Timer {
        id : timerWait
        interval: 5000; running: false; repeat: false
        onTriggered: {
            console.warn("timerWait")
            variable.highlighter = false
            timerAddItem.start()
        }
    }

    Timer {
        id : timerAddItem
        interval: 1000; running: true; repeat: false
        onTriggered: {
            console.warn("timerAddItem")

            python.call("libs.send.getWahrenkorbListe", [vars.serverIP, vars.secure_key, variable.pos], function(item) {
                console.warn("item: " + item["name_de"])


                //indexEnd
                if ( item["name_de"] === undefined ) {
                    console.warn("restart...")
                    variable.pos = 0
                    contactModel.clear()
                    timerAddItem.interval = 2000
                    timerAddItem.start()
                } else {
                    if ( item["name_de"] === "indexEnd" ) {
                        console.warn("waiting, nothing new")
                        timerAddItem.interval = 2000
                        timerAddItem.start()
                    } else {
                        addItemToList([item])
                        variable.pos += 1
                        timerAddItem.interval = 250
                        timerAddItem.start()
                    }
                }
            });

        }
    }

    ButtonSelect {
        id: buttonColor
        anchors.right: window.right
        anchors.top: window.top
        height: liste.height / 10
        width: height
        text: "Color: " + colorDialog.color

        onClicked: {colorDialog.visible = true}

        ColorDialog {
            id: colorDialog
            color: "white"
            title: "Please choose a color"
        }
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.BlueFunc', function () {});
            importModule('os', function () {});

        }
    }
}
