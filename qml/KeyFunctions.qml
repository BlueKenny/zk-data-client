import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0


Item {
    function pressed_key(key) {
        console.warn("key " + key + " pressed")
    }

    Keys.onPressed: {
        console.warn("key " + event.key + " pressed")
        if ( event.key === Qt.Key_F1 ) {
            vars.lieferscheinSuchen_Angebote = false
            key_F1()
        }/* Evtl Left + Right next rechnung ?
        if ( event.key === Qt.Key_Up ) {
            listLieferscheinSuchen.decrementCurrentIndex()
            vars.lieferscheinSuchen_Last_Selection = listLieferscheinSuchen.currentIndex
        }
        if ( event.key === Qt.Key_Down ) {
            listLieferscheinSuchen.incrementCurrentIndex()
            vars.lieferscheinSuchen_Last_Selection = listLieferscheinSuchen.currentIndex
        }*/
        if ( event.key === Qt.Key_F2 ) {
            view.push(frameRechnungSuchen)
        }
    }
}
