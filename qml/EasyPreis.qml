import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0

ApplicationWindow {
    id: mainWindow
    height: 500; width: 500
    title: qsTr("EasyPreis")
    color: "lightsteelblue"

    Item {
        id: variable
        property int font: 35
        property var dict: {"identification": ""}
    }

    TextField {
        id: textName
        width: parent.width
        placeholderText: "Name"
        font.pixelSize: variable.font
    }

    TextField {
        id: textPreis
        width: parent.width
        placeholderText: "Preis"
        anchors.top: textName.bottom
        font.pixelSize: variable.font
        validator: DoubleValidator {bottom: 0.05; decimals: 2;}
        onTextChanged: {
            text = text.replace(".", ",")
        }
    }

    TextField {
        id: textCoef
        width: parent.width
        placeholderText: "Coef"
        anchors.top: textPreis.bottom
        font.pixelSize: variable.font
    }

    Label {
        id: textID
        text: "Identification " + variable.dict["identification"]
        x: parent.width / 2 - width / 2
        font.pixelSize: variable.font
        anchors.top: textCoef.bottom
    }

    Button {
        id: buttonCreate
        text: "NEU"
        anchors.top: textID.bottom
        onClicked: {
            console.warn("ButtonCreate")
            python.call('libs.send.GetID', [vars.serverIP, vars.secure_key], function (dict) {
                variable.dict = dict
                console.warn("dict: " + variable.dict["identification"])
                console.warn(variable.dict)
                textName.text = ""
                textPreis.text = ""
                textCoef.text = "1,8"
                textName.forceActiveFocus()
                //GetID()
                //GetArt(ID)
            });


        }
    }
    Button {
        id: buttonPrint
        text: "Drucken + Speichern"
        anchors.top: textID.bottom
        anchors.right: parent.right
        onClicked: {
            if (textName.text.length > 0) {
                if (textPreis.text.length > 0) {
                    if (textCoef.text.length > 0) {
                        console.warn("buttonPrint")
                        variable.dict["name_de"] = textName.text
                        variable.dict["preisvk"] = textPreis.text.replace(",", ".")
                        variable.dict["preisvkh"] = variable.dict["preisvk"].replace(",", ".") / 1.21
                        variable.dict["preisek"] = variable.dict["preisvk"].replace(",", ".") / textCoef.text.replace(",", ".")
                        variable.dict["anzahl"] += 1
                        python.call('libs.send.SetArt', [vars.serverIP, vars.secure_key, variable.dict], function () {});
                        //python.call('libs.send.AddArt', [vars.serverIP, vars.secure_key, variable.dict["identification"], 1], function () {});
                        python.call('libs.barcode.PrintBarcode', ["CUPS", variable.dict["identification"], variable.dict["barcode"], variable.dict["name_de"], variable.dict["preisvk"]], function () {});


                        //GetArt(ID)
                    }
                }
            }
        }
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.barcode', function () {});

            //setHandler("busy", busy);
        }
    }
}

