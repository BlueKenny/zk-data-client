import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0
//import QtQuick.Controls.Styles 1.2

Rectangle {
//ScrollView {
    id: window
    height: mainWindow.height
    width: mainWindow.width

    gradient: Gradient {
        GradientStop { position: 0.5; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    color: vars.colorBackground


    Keys.onPressed: {
        if ( event.key == Qt.Key_Escape ) {
            vars.login_timeout = 0
        }
    }

    Label {
        id: label_pcname
        text: "Benutzer: " + vars.namePC + " [ " + vars.login_timeout + " ] " + "\nServer version: " + vars.serverVERSION + " \n(" + vars.pixelDensity + ")"
        font.pixelSize: vars.fontTitle
        MouseArea {
            anchors.fill: parent
            onClicked: {
                vars.login_timeout = 0
            }
        }
    }

    Label {
        id: labelSelectTitle
        text: "Hauptmenu"
        font.pixelSize: vars.fontTitle//vars.isPhone ? mainWindow.width / 20 : mainWindow.width / 80
        x: mainWindow.width / 2 - width / 2
        y: vars.pageTitleY//mainWindow.height / 15 / 2 - height / 2
    }

    GridView {
        id: buttonList
        width: window.width
        anchors.top: labelSelectTitle.bottom
        anchors.topMargin: 20
        anchors.bottom: window.bottom
        clip: true

        cellHeight: vars.backButtonHeight * 2.5
        cellWidth: vars.backButtonWidth * 2//1.5

        ScrollBar.vertical: ScrollBar {
            active: true
            //policy: ScrollBar.AlwaysOn
            width: vars.scrollbarWidth
        }


        Component {
            id: buttonListDelegate
            Item {
                id: buttonListItem
                width: vars.backButtonWidth
                height: vars.backButtonHeight * 2

                ButtonSelect {
                    //anchors.fill: parent
                    width: buttonListItem.width
                    height: buttonListItem.height
                    //x: vars.isPhone ? buttonListItem.width / 4 : buttonListItem.width
                    text: name
                    x: parent.width / 2
                    onClicked: {
                        if(name == "Angebote") {
                            vars.lieferscheinSuchen_Angebote = true
                            vars.lieferscheinSuchen_Bestellungen = false
                        } else {
                            if (name == "Bestellungen") {
                                vars.lieferscheinSuchen_Angebote = false
                                vars.lieferscheinSuchen_Bestellungen = true

                            } else {
                                vars.lieferscheinSuchen_Angebote = false
                                vars.lieferscheinSuchen_Bestellungen = false
                            }
                        }


                        if( name=="KundenSuchen") {
                            vars.kundenSuchenVorherigeAnsicht = "frameKundenSuchen"
                        }

                        view.push(frame)
                    }
                }
            }
        }

        model: menu_listModel
        delegate: buttonListDelegate

    }

    InfoLabel {}
    function setInfoLabelText(text) {vars.infoLabelText = text}
    //Busy {}
    //function busy(status) {vars.busy = status}
    InfoBox {id: infoBox}

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('os', function () {});
            importModule('time', function () {});
            importModule('libs.send', function () {});

            call("libs.BlueFunc.get_version", [], function(version) {
                vars.version = version
            })




        }
    }

}
