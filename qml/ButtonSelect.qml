import QtQuick 2.7
import QtQuick.Controls 2.0

Item {
    id: root

    property string text
    property string fontColor: "black"
    property bool enabled: true
    property string color:  "whitesmoke"
    property string color_hover:  "#fbf7f3" //"#ffffe0"
    property string tooltip_text: ""
    signal clicked()

    width: 150 //mainWindow.width / 6
    height: 50 //mainWindow.height / 5

    ToolTip {
        id: rootToolTip
        text: tooltip_text
    }

    Rectangle {
        id: rootRectangle
        border.width: mouseArea.containsMouse ? 1.5 : 1//1
        radius: mouseArea.containsMouse ? 50 : 35//35

        width: root.width
        height: root.height
        color: mouseArea.containsMouse ? root.color_hover : root.color

        PropertyAnimation {
            id: animationClicked
            target: rootRectangle
            property: "border.width"
            to: 3
            duration: 50
            onStopped: {animationReleased.running = true}
        }
        PropertyAnimation {
            id: animationReleased
            target: rootRectangle
            property: "border.width"
            to: 1
            duration: 50
        }

        Text {
            y: root.height / 2 - height / 2//rootRectangle.height / 2 - height / 2
            x: root.width / 2 - width / 2 //rootRectangle.width / 2 - width / 2
            text: root.text
            font.pixelSize: mouseArea.containsMouse ? vars.fontText + 2 : vars.fontText
            wrapMode: Text.WordWrap
            color: root.fontColor
            font.strikeout: root.enabled ? false : true

        }

        MouseArea {
            id: mouseArea
            anchors.fill : parent
            onClicked: {
                if (root.enabled == true) {
                    Qt.inputMethod.hide()
                    animationClicked.running = true
                    root.clicked()
                    root.forceActiveFocus()
                }
            }
            hoverEnabled: true

            onContainsMouseChanged: {
                if(containsMouse) {
                }
            }
            //onEntered: {
            //    rootToolTip.visible = true
            //}
            //onExited: {
            //    rootToolTip.visible = true
            //}
        }
    }
}











