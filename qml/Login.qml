import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0
import QtQuick.Window 2.12

Rectangle {
    id: window
    height: mainWindow.height
    width: mainWindow.width

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    color: vars.colorBackground

    function load_server_list() {
        server_ip_model.clear()

        vars.busy = true
        buttonLogin.enabled = false

        python.call("libs.send.get_server_list", [], function(server_liste) {
            //console.warn("server_liste: " + server_liste)

            for (var i = 0; i < server_liste.length; i++) {
                console.warn("found server " + server_liste[i][0] + ": " + server_liste[i][1])

                server_ip_model.append({"name": server_liste[i][0], "ip": server_liste[i][1]})

                if (vars.serverNAME == server_liste[i][0]) {
                    text_server_ip.currentIndex = i
                }
            }
            vars.busy = false
            buttonLogin.enabled = true
        })

        //python.call("libs.BlueFunc.online_monitor", [], function() {})
    }

    function transaction_load_next_konto(index) {
        python.call("libs.send.get_konto", [vars.serverIP, vars.secure_key, index], function(konto) {
            if (konto !== -1) {// todo show all if admin
                vars.transaction_konto.push(konto)

                transaction_load_next_konto(index + 1)
            } else {
                view.push(frameSelect)
                vars.busy = false
                buttonLogin.enabled = true

                if (vars.scanner_name == "NULL") {
                    python.call("libs.scan.get_scanner", [], function(scanner_list) {
                        vars.scanner_name = scanner_list[0]
                    })
                }

            }
        })

    }


    Label {
        id: label_ip
        text: "Server IP: "
        anchors.left: window.left
        anchors.leftMargin: 50
        anchors.top: window.top
        anchors.topMargin: 50
    }

    ComboBox {
        id: text_server_ip
        model: server_ip_model
        textRole: "name"

        ListModel {
            id: server_ip_model
        }
        anchors.left: label_ip.right
        anchors.leftMargin: 10
        anchors.top: window.top
        anchors.topMargin: 50


        onDisplayTextChanged: {
            vars.serverIP = server_ip_model.get(text_server_ip.currentIndex).ip

            text_login_key.forceActiveFocus()
        }

    }



    Label {
        id: label_login
        text: "Login passwort: "
        anchors.top: text_server_ip.bottom
        anchors.topMargin: 10
        anchors.left: window.left
        anchors.leftMargin: 50
    }

    TextField {
        id: text_login_key
        anchors.left: label_login.right
        anchors.leftMargin: 10
        anchors.top: text_server_ip.bottom
        anchors.topMargin: 10
        selectByMouse: true
        echoMode: TextInput.Password

        onFocusChanged: {
            if (focus == true) {timer_login_set_focus.stop()}
        }
        onAccepted: {
            if (vars.busy == false) {
                buttonLogin.clicked()
            }
        }
    }

    ButtonSelect {
        id: buttonLogin
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        anchors.left: window.left
        anchors.leftMargin: 50

        text: "Login"

        anchors.top: text_login_key.bottom
        anchors.topMargin: 10

        onClicked: {
            console.warn("secure_key test: " + text_login_key.text)

            menu_listModel.clear()
            vars.transaction_konto = []
            vars.busy = true

            buttonLogin.enabled = false

            python.call("libs.send.getUserData_from_key", [vars.serverIP, text_login_key.text], function(userDATA) {
                console.warn("userDATA: \"" + userDATA + "\"")
                if (userDATA == "" || userDATA == "-1") {
                    buttonLogin.enabled = true
                    text_login_key.text = ""
                    vars.busy = false

                } else {
                    console.warn("Login...")
                    vars.secure_key = text_login_key.text
                    vars.namePC = userDATA["name"]

                    vars.userData = []
                    vars.userData["admin"] = userDATA["admin"]
                    vars.userData["permission_cashbook"] = userDATA["permission_cashbook"]
                    vars.userData["permission_stats"] = userDATA["permission_stats"]
                    vars.userData["permission_invoice"] = userDATA["permission_invoice"]
                    vars.userData["permission_invoice_e"] = userDATA["permission_invoice_e"]
                    vars.userData["permission_delivery"] = userDATA["permission_delivery"]
                    vars.userData["permission_client"] = userDATA["permission_client"]

                    transaction_load_next_konto(0)

                    if (userDATA["admin"] === true) {
                        menu_listModel.insert(0, {"name": "Administrator Tools", "frame": "AdminTools.qml"})
                    }
                    if (userDATA["permission_cashbook"] > 0) {
                        menu_listModel.insert(0, {"name": "Transaktionen", "frame": "Transaktionen.qml"})
                        menu_listModel.insert(0, {"name": "Kassenbuch", "frame": "Kassenbuch.qml"})
                    }

                    if (userDATA["permission_stats"] > 0) {
                        menu_listModel.insert(0, {"name": "Statistik4", "frame": "Statistik4.qml"})
                        menu_listModel.insert(0, {"name": "Statistik", "frame": "Statistik3.qml"})
                    }

                    if (userDATA["permission_inventory"] > 0) {
                        menu_listModel.insert(0, {"name": "Inventar", "frame": "Inventar.qml"})
                    }

                    python.call("libs.send.search_belt", [vars.serverIP, vars.secure_key, 5, 30, 0, 3000, 0], function(answer) {
                        if (answer["identification"] == "-1") {
                        } else {
                            menu_listModel.insert(5, {"name": "Riemen Suche", "frame": "BeltSearch.qml"})
                        }
                    })

                    if (userDATA["permission_invoice"] > 0) {
                        menu_listModel.insert(0, {"name": "Rechnungen (Ausgang)", "frame": "RechnungSuchen.qml"})
                    }

                    if (userDATA["permission_invoice_e"] > 0) {
                        menu_listModel.insert(0, {"name": "Rechnungen (Eingang)", "frame": "RechnungESuchen.qml"})
                    }

                    if (userDATA["permission_delivery"] > 0) {
                        menu_listModel.insert(0, {"name": "Lieferscheine", "frame": "LieferscheinSuchen.qml"})
                        menu_listModel.insert(0, {"name": "Bestellungen", "frame": "LieferscheinSuchen.qml"})
                        menu_listModel.insert(0, {"name": "Angebote", "frame": "LieferscheinSuchen.qml"})
                    }

                    if (userDATA["permission_client"] > 0) {
                        menu_listModel.insert(0, {"name": "Kunden", "frame": "KundenSuchen.qml"})
                    }

                    python.call("libs.send.get_server_version", [vars.serverIP, vars.secure_key], function(version) {
                        vars.serverVERSION = version
                    })
                    python.call("libs.send.get_server_name", [vars.serverIP, vars.secure_key], function(name) {
                        vars.serverNAME = name
                        python.call("os.system", ["mkdir /tmp/" + name], function() {})
                    })
                    python.call("libs.send.get_article_categories", [vars.serverIP, vars.secure_key], function(categories) {
                        vars.article_categories = categories
                    })

                    vars.mwst = []
                    python.call("libs.send.get_mwst", [vars.serverIP, vars.secure_key], function(mwst) {
                        console.warn("mwst: " + mwst)
                        vars.mwst = mwst
                    })

                    vars.login_max_timeout = userDATA["timeout"]
                    vars.login_timeout = userDATA["timeout"]
                    timer_timeout.start()

                    python.call("libs.send.clear_cache", [], function() {})
                    vars.busy = false
                }
            });
        }
    }

    Timer {
        id: timer_login_set_focus
        interval: 1000; running: true; repeat: true
        onTriggered: {
            console.warn("timer_login_set_focus")
            text_login_key.forceActiveFocus()
        }
    }

    Timer {
        id: timer_timeout
        interval: 5000; running: false; repeat: true
        onTriggered: {
            console.warn("timer_timeout: " + vars.login_timeout)

            if (vars.login_timeout - timer_timeout.interval/1000 < 0) {
                vars.login_timeout = vars.login_max_timeout
                timer_timeout.stop()

                timer_login_set_focus.start()
                view.push(frameLogin)

                load_server_list()
            } else {
                vars.login_timeout = vars.login_timeout - timer_timeout.interval/1000
            }

        }
    }

    Busy {}

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));

            importModule('libs.send', function () {});
            importModule('libs.BlueFunc', function () {});
            importModule('libs.scan', function () {});

            load_server_list()



        }

    }


}
