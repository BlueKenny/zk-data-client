import QtQuick 2.15
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.0// NEW

import QtCharts 2.0
import QtWebView 1.15

//Qt.openUrlExternally()

ScrollView {
    id: statsPage

    height: mainWindow.height
    width: mainWindow.width

    contentWidth: mainWindow.width
    contentHeight: label_test.y + 50

    property int umsatz: 0.0
    property int lieferungen: 0
    property int kunden: 0

    function reload_stats() {
        python.call("libs.BlueFunc.code_to_dates", [combobox_zeitraum.currentIndex], function(dates) {
            python.call("libs.BlueFunc.date_reversed", [dates[0]], function(date_start_reversed) {
                python.call("libs.BlueFunc.date_reversed", [dates[1]], function(date_end_reversed) {
                    label_selected_dates.text = date_start_reversed + " bis " + date_end_reversed

                    python.call("libs.send.get_stats_umsatz", [vars.serverIP, vars.secure_key, dates[0], dates[1]], function(answer) {
                        chart_netto_umsatz.x_names = answer[0]
                        chart_netto_umsatz.points = answer[1]
                        chart_netto_umsatz.redraw()

                        statsPage.umsatz = 0
                        for (var i = 0; i < answer[1].length; i++) {
                            statsPage.umsatz = statsPage.umsatz + answer[1][i]
                        }
                    })
                    python.call("libs.send.get_stats_delivery", [vars.serverIP, vars.secure_key, dates[0], dates[1]], function(answer) {
                        chart_lieferungen.x_names = answer[0]
                        chart_lieferungen.points = answer[1]
                        chart_lieferungen.redraw()

                        statsPage.lieferungen = 0
                        for (var i = 0; i < answer[1].length; i++) {
                            statsPage.lieferungen = statsPage.lieferungen + answer[1][i]
                        }
                    })
                    python.call("libs.send.get_stats_clients", [vars.serverIP, vars.secure_key, dates[0], dates[1]], function(answer) {
                        chart_kunden.x_names = answer[0]
                        chart_kunden.points = answer[1]
                        chart_kunden.redraw()

                        statsPage.kunden = 0
                        for (var i = 0; i < answer[1].length; i++) {
                            statsPage.kunden = statsPage.kunden + answer[1][i]
                        }
                    })


                })
            })


        })
    }

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        onClicked: {
            view.push(frameSelect)
        }
    }

    Label {
        id: label_combo_zeitraum
        text: "Zeitraum"
        anchors.top: buttonBack.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
    }

    ComboBox {
        id: combobox_zeitraum
        model: combobox_model
        ListModel {id: combobox_model}

        anchors.left: label_combo_zeitraum.right
        anchors.leftMargin: 10
        anchors.top: label_combo_zeitraum.top
        width: 200
        textRole: "name"

        onCurrentIndexChanged: {
            reload_stats()
        }
    }
    Label {
        id: label_selected_dates
        anchors.left: combobox_zeitraum.right
        anchors.leftMargin: 10
        anchors.top: label_combo_zeitraum.top
    }

    Rectangle {
        id: line_1
        height: 1
        width: parent.width
        anchors.top: combobox_zeitraum.bottom
        anchors.topMargin: 10
    }

    Label {
        id: label_leistung
        text: "Leistung"
        anchors.top: line_1.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
    }

    Rectangle { //ListView {
        id: liste_leistung

        width: parent.width
        height: 70

        color: "transparent"

        //clip: true

        //orientation: ListView.Horizontal

        anchors.top: label_leistung.bottom
        anchors.topMargin: 10
        /*
        model: ListModel {
            ListElement {
                name: "Nettoumsatz"
                wert: statsPage.umsatz
            }
            ListElement {
                name: "Lieferungen"
                wert: statsPage.lieferungen
            }
            ListElement {
                name: "Kunden"
                wert: statsPage.kunden
            }
        }

        delegate: Component {
            Item {
                id: itemListe
                width: 250
                height: 50

                ButtonSelect { // Switch
                    anchors.centerIn: parent
                    text: name + "\n" + wert

                    width: 200
                    height: 50
                    //font.pixelSize: vars.fontText

                }

            }

        }
        */

        ButtonSelect { // Switch
            id: button_umsatz
            y: parent.height / 2 - height / 2
            anchors.left: label_leistung.right
            anchors.leftMargin: 10
            text: "Nettoumsatz \n " + statsPage.umsatz
            width: 200
            height: 50
        }
        ButtonSelect { // Switch
            id: button_lieferungen
            y: parent.height / 2 - height / 2
            anchors.left: button_umsatz.right
            anchors.leftMargin: 10
            text: "Lieferungen \n " + statsPage.lieferungen
            width: 200
            height: 50
        }
        ButtonSelect { // Switch
            id: button_knden
            y: parent.height / 2 - height / 2
            anchors.left: button_lieferungen.right
            anchors.leftMargin: 10
            text: "Kunden \n " + statsPage.kunden
            width: 200
            height: 50
        }

    }

    Rectangle {
        id: line_2
        height: 1
        width: parent.width
        anchors.top: liste_leistung.bottom
        anchors.topMargin: 10
    }


/*
    ListView {
        id: liste_diagramme

        width: parent.width
        height: 500

        clip: true

        //orientation: ListView.Horizontal

        anchors.top: line_2.bottom
        anchors.topMargin: 10

        model: ListModel {
            ListElement {
                title: "Kunden"
                x_names: ["Januar", "Februar", "März", "April", "Mai", "Juni"]
                points: ["2", "4", "15", "150", "250", "450"]
            }
            ListElement {
                title: "Kunden"
                //x_names: ["Januar", "Februar", "März", "April", "Mai", "Juni"]
                //points: ["2", "4", "15", "150", "250", "450"]
            }
        }

        delegate: Component {
            Item {
                id: itemListe_chart
                width: 500
                height: 500

                Chart {
                    id: chart_clients
                    //anchors.top: line_2.bottom
                    //anchors.topMargin: 10
                    //anchors.left: parent.left
                    //anchors.leftMargin: 10

                    height: 500
                    width: 500

                    title: title
                    //x_names: x_names
                    //points: points
                }


            }



        }
    }
    */

    Chart {
        id: chart_netto_umsatz
        anchors.top: line_2.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10

        height: 600
        width: parent.width

        title: "Nettoumsatz"
        x_names: ["Loading"]
        points: ["0"]
    }
    Chart {
        id: chart_lieferungen
        anchors.top: chart_netto_umsatz.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10

        height: 600
        width: parent.width

        title: "Lieferungen"
        x_names: ["Loading"]
        points: ["0"]
    }
    Chart {
        id: chart_kunden
        anchors.top: chart_lieferungen.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10

        height: 600
        width: parent.width

        title: "Kunden"
        x_names: ["Loading"]
        points: ["0"]
    }
    Chart {
        id: chart_stock_pro_categorie
        anchors.top: chart_kunden.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10

        height: 600
        width: parent.width

        title: "Stock / Kategorie"
        x_names: ["Work in Progress"]
        points: ["0"]
    }





    Label {
        id: label_test
        anchors.top: chart_stock_pro_categorie.bottom
    }

    Busy {}

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.BlueFunc', function () {});
            importModule('libs.send', function () {});

            python.call("libs.BlueFunc.code_to_dates", ["HELP"], function(answer) {
                for (var i = 0; i < answer.length; i++) {
                    combobox_model.append({"name": answer[i]})

                    if (i == 3) {
                        combobox_zeitraum.currentIndex = i
                        reload_stats()
                    }
                }


            })

        }
    }
}
