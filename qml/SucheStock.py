#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os

import sys
sys.path.append("/etc/zk-data-libs/")

from libs.appjar0900 import gui
#from libs.appjar0_94 import gui

#import libs.need
import libs.BlueFunc
import libs.send
import libs.barcode
import csv

ip = sys.argv[1]
secure_key = sys.argv[2]

version = str(open("/etc/zk-data-client/version", "r").read()).rstrip()

EntryList=["Barcode", "Artikel", "Artikel2", "Artikel3", "Lieferant", "Name", "Ort", "PreisEK", "PreisVKH", "PreisVK", "Anzahl"]
appSuche = gui("StockSuche ZK-DATA " + version, "1000x1000", handleArgs=False)
appSuche.setIcon("/usr/share/pixmaps/zk-data-stock.gif")
appSuche.setBg("#3399ff")

IDToChange = 0

eigene_id_festlegen = libs.BlueFunc.get_data("stock_eigene_id_festlegen")
if str(eigene_id_festlegen) == "None":
    if appSuche.yesNoBox("Identification festlegen", "Wollen sie eigene IDs einstellen können ?", parent=None):
        eigene_id_festlegen = True
    else:
        eigene_id_festlegen = False
    libs.BlueFunc.set_data("stock_eigene_id_festlegen", eigene_id_festlegen)

def getMove():
    ID = str(appSuche.getListItems("Suche")[0].split(" | ")[0]).rstrip()
    os.system("/etc/zk-data-libs/libs/get_move.py " + ip + " " + str(secure_key) + " " + str(ID))

def BtnPrintBarcode(btn):
    ID = str(appSuche.getListItems("Suche")[0].split(" | ")[0]).rstrip()
    print("ID " + ID)

    if not "P" in ID:
        Data = libs.send.GetArt(ip, secure_key, ID)
        Anzahl = appSuche.numberBox("Drucken", "Welche Anzahl soll gedruckt werden?")
        for a in range(0, Anzahl):
            libs.barcode.PrintBarcode("", Data["identification"], Data["barcode"], Data["name_de"], Data["preisvk"])
            
def BtnPrintBarcode2(btn):
    ID = str(appSuche.getListItems("Suche")[0].split(" | ")[0]).rstrip()
    print("ID " + ID)

    if not "P" in ID:
        Data = libs.send.get_article(ip, secure_key, ID)
        Anzahl = appSuche.numberBox("Drucken", "Welche Anzahl soll gedruckt werden?")
        
        libs.barcode.PrintBarcode2(ip, secure_key, Data["identification"], Data["name_de"], Data["preisvk"], Anzahl)

def BtnAverage(btn):
    ID = str(appSuche.getListItems("Suche")[0].split(" | ")[0]).rstrip()
    average = libs.send.get_article_average(ip, secure_key, ID)
    appSuche.infoBox("Verkaufszahlen", str(average), parent=None)

def BtnPrintArtikel(btn):
    ID = str(appSuche.getListItems("Suche")[0].split(" | ")[0]).rstrip()
    print("ID " + ID)

    if not "P" in ID:
        Data = libs.send.GetArt(ip, secure_key, ID)
        libs.barcode.PrintArtikel(Data["artikel"], Data["lieferant"])

def BtnPrintOrt(btn):
    ID = str(appSuche.getListItems("Suche")[0].split(" | ")[0]).rstrip()
    print("ID " + ID)

    if not "P" in ID:
        Data = libs.send.GetArt(ip, secure_key, ID)
        libs.barcode.PrintLocation(Data["ort"])

def BtnSettings(btn):     
    path = "/etc/zk-data-libs/database/data.db"
    print("config file path: " + path)
    os.system("sqlitebrowser " + path)

def ArtikelAnzeigen():
    print("ArtikelAnzeigen")
    ID = str(appSuche.getListBox("Suche")[0].split(" | ")[0]).rstrip()
    print("ID:" + ID)
    COMMAND = "python3 /etc/zk-data-client/qml/ChangeStock.py"
    application = os.popen(COMMAND + " " + ip + " " + secure_key + " " + ID).readlines()
    
    appSuche.setEntry("Suche", ID)
    Suche()

def tbFunc(btn):
    global IDToChange
    global appChange

    neue_id = ""

    if btn == "NEU":
        print("eigene_id_festlegen: " + str(eigene_id_festlegen))
        if bool(eigene_id_festlegen):
            new_id = appSuche.numberBox("Artikel ID", "Eigene ID für neuen Artikel festlegen ?")
            print("new_id: " + str(new_id))            
            if str(new_id) == "None":
                object = libs.send.create_article(ip, secure_key, "")
            else:
                object = libs.send.create_article(ip, secure_key, new_id)                
        else:     
            object = libs.send.create_article(ip, secure_key, "")
            
        neue_id = str(object["identification"])
         
        COMMAND = "python3 /etc/zk-data-client/qml/ChangeStock.py"
        COMMAND = COMMAND + " " + ip + " " + secure_key + " " + str(object["identification"])
        application = os.popen(COMMAND).readlines()
        
    if btn == "F1 -": StockChange("<F1>")
    if btn == "F2 +": StockChange("<F2>")
    if btn == "F3 Jahresverkauf": BtnAverage("")
    if btn == "F4 Bewegungen": getMove()
    if btn == "F10 Artikel": BtnPrintArtikel("")
    if btn == "F11 Ort": BtnPrintOrt("")
    if btn == "F12 Barcode": BtnPrintBarcode("")
    if btn == "...": BtnSettings("")
    Delete("")
    
    if not neue_id == "":
        appSuche.setEntry("Suche", neue_id)
        Suche()


tools = ["NEU", "", "F1 -", "F2 +", " ", "F3 Jahresverkauf", "F4 Bewegungen", "  ", "F10 Artikel", "F11 Ort", "F12 Barcode", "   ", "..."]
appSuche.addToolbar(tools, tbFunc, findIcon=False)


#appSuche.addDualMeter("progress", 0, 0)
#appSuche.setMeterFill("progress", ["red", "green"])
#appSuche.setMeter("progress", [0, 0])

appSuche.addLabelEntry("Suche", 1, 0, 1, 0)
#appSuche.addLabelEntry("Ort", 1, 1, 1, 0)
#appSuche.addLabelEntry("Lieferant", 1, 2, 1, 0)

#GridSuche = appSuche.addGrid("Suche", [["Identification", "Artikel", "Lieferant", "Name", "Ort", "Preis", "Anzahl"]],
#                             action=ArtikelAnzeigen,
#                             actionHeading="Informationen",
#                             actionButton="Anzeigen",
#                             showMenu=False)
#appSuche.setGridHeight("Suche", 500)

ListBoxSuche = appSuche.addListBox("Suche", [], 2, 0, 3, 2)
appSuche.setListBoxHeight("Suche", 30)
ListBoxSuche.bind("<Double-1>", lambda *args: ArtikelAnzeigen())# if List Item double click then change

appSuche.addLabel("info1", "", 5, 0, 1, 0)

def Delete(btn):
    appSuche.setEntry("Suche", "")
    #appSuche.setEntry("Ort", "")
    #appSuche.setEntry("Lieferant", "")
    appSuche.setFocus("Suche")


def Suche():
    appSuche.setLabel("info1", "Suche gestartet")
    appSuche.thread(SucheProcess)
    #appSuche.after(500, Suche)

def SucheProcess():
    Suche = appSuche.getEntry("Suche")

    appSuche.clearListBox("Suche")

    EndString = ""
    for character in Suche:
        if character.isalpha() or character.isdigit():
            EndString = EndString + str(character)
    Suche = EndString.upper()
    appSuche.setEntry("Suche", Suche, callFunction=False)


    if Suche == "":
        NichtSuchen = True
    else:
        NichtSuchen = False

    #if len(Suche) == 26:
    #    NichtSuchen = True
    #    Suche = Suche[13:26]
    #    appSuche.setEntry("Suche", Suche, callFunction=True)
        

    if not NichtSuchen:
        AntwortListe = libs.send.SearchArt(ip, secure_key, {"suche":Suche})
        print("AntwortListe: " + str(AntwortListe))

        for ID in AntwortListe:
            if not ID == "":
                print("ID: " + str(ID))
                Art = libs.send.GetArt(ip, secure_key, ID)
                        
                Linie = str(ID)
                Linie = Linie + " | " + str(Art["artikel"])
                Linie = Linie + " | " + str(Art["lieferant"])
                Linie = Linie + " | " + str(Art["name_de"])
                Linie = Linie + " | " + str(Art["ort"])
                Linie = Linie + " | " + str(Art["preisvk"])
                Linie = Linie + " | " + str(Art["anzahl"])
                appSuche.addListItem("Suche", Linie)
                appSuche.setListItemBg("Suche", Linie, "#ffffff")

        appSuche.selectListItemAtPos("Suche", 0, callFunction=False)
    appSuche.setLabel("info1", "Suche Beendet")

def StockChange(btn):
    print("btn: " + str(btn))
    
    try: IDToChange = appSuche.getListBox("Suche")[0].split(" | ")[0].rstrip()
    except: appSuche.errorBox("Fehler", "Bitte wählen sie zuerst einen Artikel aus")
    if not "P" in IDToChange:
        if btn == "<F1>": # MINUS
            Anzahl = appSuche.numberBox("Entfernen", "Wie viele wolen sie ENTFERNEN ?")
            if Anzahl == None:
                appSuche.errorBox("Fehler", "Abgebrochen")
            else:
                try:
                    Check = libs.send.add_article(ip, secure_key, IDToChange, "-" + str(int(Anzahl)))
                    if Check:
                        #appSuche.infoBox("Gespeichert", "Anzahl wurde geändert")
                        #appSuche.setEntry("Suche", IDToChange)
                        True
                    else:
                        appSuche.infoBox("Fehler", "Anzahl wurde nicht geändert")
                except: appSuche.infoBox("Fehler", "Fehler")
        if btn == "<F2>": # PLUS
            Anzahl = appSuche.numberBox("Hinzufügen", "Wie viele wollen sie HINZUFUGEN ?")
            if Anzahl == None:
                appSuche.errorBox("Fehler", "Abgebrochen")
            else:
                try:
                    Check = libs.send.add_article(ip, secure_key, IDToChange, int(Anzahl))
                    if Check:
                        #appSuche.infoBox("Gespeichert", "Anzahl wurde geändert")
                        #appSuche.setEntry("Suche", IDToChange)
                        True
                    else:
                        appSuche.infoBox("Fehler", "Anzahl wurde nicht geändert")
                except: 
                    appSuche.infoBox("Fehler", "Fehler")
    Suche()

appSuche.setFocus("Suche")

def NichtGesucht(btn):
    appSuche.setLabel("info1", "Suche noch nicht gestartet")

def suche_mit_name(btn):
    print("suche_mit_name")
    Suche = appSuche.getEntry("Suche")

    appSuche.clearListBox("Suche")

    #EndString = ""
    #for character in Suche:
    #    if character.isalpha() or character.isdigit():
    #        EndString = EndString + str(character)
    #Suche = EndString.upper()
    Suche = Suche.upper()
    appSuche.setEntry("Suche", Suche, callFunction=False)

    Ort = ""
    Lieferant = ""

    if Suche == "":
        NichtSuchen = True
    else:
        NichtSuchen = False

    if not NichtSuchen:
        AntwortListe = libs.send.search_article(ip, secure_key, {"name":Suche, "ort":Ort, "lieferant":Lieferant})
        print("AntwortListe: " + str(AntwortListe))

        for ID in AntwortListe:
            if not ID == "":
                print("ID: " + str(ID))
                Art = libs.send.get_article(ip, secure_key, ID)
                        
                Linie = str(ID)
                Linie = Linie + " | " + str(Art["artikel"])
                Linie = Linie + " | " + str(Art["lieferant"])
                Linie = Linie + " | " + str(Art["name_de"])
                Linie = Linie + " | " + str(Art["ort"])
                Linie = Linie + " | " + str(Art["preisvk"])
                Linie = Linie + " | " + str(Art["anzahl"])
                appSuche.addListItem("Suche", Linie)
                appSuche.setListItemBg("Suche", Linie, "#ffffff")

        appSuche.selectListItemAtPos("Suche", 0, callFunction=False)
    appSuche.setLabel("info1", "Suche Beendet")


appSuche.setEntryChangeFunction("Suche", NichtGesucht)
#appSuche.setEntryChangeFunction("Ort", Suche)
#appSuche.setEntryChangeFunction("Lieferant", Suche)
appSuche.enableEnter(Suche)

appSuche.bindKey("<F1>", StockChange)
appSuche.bindKey("<F2>", StockChange)
appSuche.bindKey("<F3>", BtnAverage)

appSuche.bindKey("<F4>", getMove)
appSuche.bindKey("<Control_L>", suche_mit_name)
appSuche.bindKey("<F10>", BtnPrintArtikel)
appSuche.bindKey("<F11>", BtnPrintOrt)
appSuche.bindKey("<F12>", BtnPrintBarcode)
appSuche.bindKey("<F9>", BtnPrintBarcode2)
appSuche.bindKey("<Delete>", Delete)


#appSuche.after(1000, Suche)
appSuche.go()
