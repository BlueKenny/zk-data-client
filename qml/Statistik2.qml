import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.0// NEW
//Qt.openUrlExternally()

Rectangle {
    id: statsPage
    width: mainWindow.width

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    color: vars.colorBackground

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        onClicked: {
            view.push(frameSelect)
        }
    }

    Label {
        id: labelTitle
        text: "Statistik"
        font.pixelSize: vars.fontTitle
        x: mainWindow.width / 2 - width / 2
        y: buttonBack.height / 2 - height / 2
    }


    Chart {
        id: chart
        anchors.top: buttonBack.bottom
        anchors.topMargin: 20

        height: parent.height - buttonBack.height - 40
        width: parent.width
        x_names: ["Januar", "-", "Februar", "-", "März", "-", "April", "-",, "Mai", "-", "Juni", "-", "July", "-", "August", "-", "September", "-", "Oktober", "-", "November", "-", "Dezember", "-"]
        points: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }


    //InfoLabel {}
    //function setInfoLabelText(text) {vars.infoLabelText = text}
    //Busy {}
    //function busy(status) {vars.busy = status}

    /*
    function load_next_month(year, month) {
        var month_as_string = month.toString()
        if (month_as_string.length === 1) {
            month_as_string = "0" + month
        }

        python.call("libs.send.get_worker_total_month", [vars.serverIP, vars.namePC, year, month_as_string], function(sum) {
            //chart.points.append(parseFloat(sum))
            chart.points[ month - 1 ] = parseFloat(sum)
            if (parseFloat == 0.0) {
                chart.x_names[ month - 1 ] = ""
            }

            chart.redraw()

            load_next_month(year, month + 1)
        });
    }*/

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.BlueFunc', function () {});
            importModule('libs.send', function () {});

            call("libs.send.get_stats_rechnung", [vars.serverIP, "2021"], function(answer_list) {
                //print("answer_list: " + answer_list)
                chart.points = answer_list
                chart.redraw()
            })
            /*
            call("libs.BlueFunc.Date", [], function(time) {
                var time_year = time.split("-")[0]
                //var time_month = time.split("-")[1]
                load_next_month(time_year, 1)
            });*/

        }
    }
}
