import QtQuick 2.7
import QtQuick.Window 2.15
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.0

import "../../zk-data-libs/qml" as ZKLibs
//import "/home/kenny/git/zk-data-libs/qml/" as ZKLibs

ApplicationWindow {
    id: rect_stock_search

    color: "#8cb3d9"

    title: "ZK-DATA-STOCK v2"

    width: 1000
    height: 800
    minimumHeight: 900
    minimumWidth: 800

    property var ip: ""
    property var secure_key: ""

    Timer {
        id: timer_login
        interval: 5000
        repeat: true
        running: true
        onTriggered: {
            python_stock_search.call("libs.send.get_server_name", [ip, secure_key], function(name) {
                if (name === "") {
                    timer_login.interval = 5000
                    rect_stock_search.title = "ZK_DATA-STOCK v2 - keine verbindung ?"
                    popup_login.open()
                } else {
                    timer_login.interval = 30000
                }
            })
        }
    }

    menuBar: MenuBar {
        id: menu_stock

        Menu {
            title: qsTr("&Erstellen")
            Action {
                text: qsTr("&id Automatisch Wählen")
                onTriggered: {
                    python_stock_search.call("libs.send.create_article", [ip, secure_key, ""], function(answer_article) {
                        var new_article = parseInt(answer_article["identification"])
                        console.warn("new_article: " + new_article)
                        stock_article_change.article_identification = parseInt(new_article)
                        stock_article_change.visible = true
                    })
                }
            }
            Action {
                text: qsTr("&id Manuel Wählen")
                onTriggered: {
                    popup_create_article.open()
                }
            }
            //MenuSeparator { }
            //Action { text: qsTr("&Quit") }
        }
        Menu {
            id: submenu_stock
            title: qsTr("&Stock")
            Action {
                text: qsTr("&Hinzufügen")
                onTriggered: {
                    popup_add_article.open()
                }
            }
            Action {
                text: qsTr("&Entfernen")
                onTriggered: {
                    popup_remove_article.open()
                }
            }
            Action {
                text: qsTr("&Gesamtanzahl ändern")
                onTriggered: {
                    python_stock_search.call("libs.send.get_article", [ip, secure_key, stock_article_change.article_identification], function(answer_article) {
                        var quantity = parseInt(answer_article["anzahl"])
                        text_from_popup_changeQuantity_article.text = quantity
                        popup_changeQuantity_article.open()
                    })
                }
            }
            enabled: false
        }
        Menu {
            id: submenu_print
            title: qsTr("&Drucken")
            Action {
                text: qsTr("&Artikel (Brother)")
                onTriggered: {
                    python_stock_search.call("libs.send.get_article", [ip, secure_key, stock_article_change.article_identification], function(answer_article) {
                        python_stock_search.call("libs.barcode.PrintBarcode2", [ip, secure_key, answer_article["identification"], answer_article["name_de"], answer_article["preisvk"], 1], function() {})
                    })
                }
            }
            MenuSeparator { }
            Action {
                text: qsTr("&Artikel (Zebra)")
                onTriggered: {
                    python_stock_search.call("libs.send.get_article", [ip, secure_key, stock_article_change.article_identification], function(answer_article) {
                        python_stock_search.call("libs.barcode.PrintBarcode", [ip, answer_article["identification"], answer_article["barcode"], answer_article["name_de"], answer_article["preisvk"]], function() {})
                    })
                }
            }
            Action { text: qsTr("&Ort (Zebra)") }
            Action { text: qsTr("&Artikelnummer (Zebra)") }
            enabled: false
        }

        Action {
            id: submenu_save
            text: qsTr("&Speichern")
            //enabled: false
        }
    }

    Popup {
        id: popup_create_article
        ColumnLayout {
            anchors.fill: parent
            Label { text: "Artikel Erstellen\nDie neue identification soll folgende sein:"}
            TextField {
                id: text_from_popup_create_article
                text: ""
                selectByMouse: true
                validator: IntValidator {bottom: 1; top: 999999}
                onEditingFinished: {
                    button_from_popup_create_article.clicked()
                }
            }
            Button {
                id: button_from_popup_create_article
                text: "Erstellen"
                onClicked: {
                    python_stock_search.call("libs.send.create_article", [ip, secure_key, text_from_popup_create_article.text], function(answer_article) {
                        var new_article = parseInt(answer_article["identification"])
                        stock_article_change.article_identification = new_article
                        stock_article_change.visible = true
                        popup_create_article.close()
                    })
                }
            }
        }
    }
    Popup {
        id: popup_add_article
        ColumnLayout {
            anchors.fill: parent
            Label { text: "Artikel Hinzufügen\nAnzahl die zum Stock hingefügt werden sollen:"}
            TextField {
                id: text_from_popup_add_article
                text: "1"
                selectByMouse: true
                validator: IntValidator {bottom: 1; top: 999999}
                onEditingFinished: {
                    button_from_popup_add_article.clicked()
                }
            }
            Button {
                id: button_from_popup_add_article
                text: "Hinzufügen"
                onClicked: {
                    python_stock_search.call("libs.send.add_article", [ip, secure_key, stock_article_change.article_identification, text_from_popup_add_article.text], function() {
                        // Update Article Search
                        stock_article_search.old_text = ""
                        stock_article_search.search_articles(stock_article_search.text)
                        // Update Article Change
                        var saved_identification = stock_article_change.article_identification
                        stock_article_change.article_identification = ""
                        stock_article_change.article_identification = saved_identification
                        popup_add_article.close()
                    })
                }
            }
        }
    }
    Popup {
        id: popup_remove_article
        ColumnLayout {
            anchors.fill: parent
            Label { text: "Artikel Entfernen\nAnzahl die aus Stock entfernt werden soll:"}
            TextField {
                id: text_from_popup_remove_article
                text: "1"
                selectByMouse: true
                validator: IntValidator {bottom: 1; top: 999999}
                onEditingFinished: {
                    button_from_popup_remove_article.clicked()
                }
            }
            Button {
                id: button_from_popup_remove_article
                text: "Entfernen"
                onClicked: {
                    python_stock_search.call("libs.send.add_article", [ip, secure_key, stock_article_change.article_identification, "-" + text_from_popup_add_article.text], function() {
                        // Update Article Search
                        stock_article_search.old_text = ""
                        stock_article_search.search_articles(stock_article_search.text)
                        // Update Article Change
                        var saved_identification = stock_article_change.article_identification
                        stock_article_change.article_identification = ""
                        stock_article_change.article_identification = saved_identification
                        popup_remove_article.close()
                    })
                }
            }
        }
    }
    Popup {
        id: popup_changeQuantity_article
        ColumnLayout {
            anchors.fill: parent
            Label { text: "Gesamtanzahl ändern\nWie oft gibt es diesen Artikel:"}
            TextField {
                id: text_from_popup_changeQuantity_article
                text: "1"
                selectByMouse: true
                validator: IntValidator {bottom: 1; top: 999999}
                onEditingFinished: {
                    button_from_popup_changeQuantity_article.clicked()
                }
            }
            Button {
                id: button_from_popup_changeQuantity_article
                text: "Gesamtanzahl ändern"
                onClicked: {
                    python_stock_search.call("libs.send.get_article", [ip, secure_key, stock_article_change.article_identification], function(answer_article) {
                        var old_quantity = parseInt(answer_article["anzahl"])
                        var new_quantity = parseInt(text_from_popup_changeQuantity_article.text)
                        var diff = new_quantity - old_quantity

                        python_stock_search.call("libs.send.add_article", [ip, secure_key, stock_article_change.article_identification, diff], function() {
                            // Update Article Search
                            stock_article_search.old_text = ""
                            stock_article_search.search_articles(stock_article_search.text)
                            // Update Article Change
                            var saved_identification = stock_article_change.article_identification
                            stock_article_change.article_identification = ""
                            stock_article_change.article_identification = saved_identification
                            popup_changeQuantity_article.close()
                        })

                    })

                }
            }
        }
    }


    Popup {
        id: popup_login
        ColumnLayout {
            anchors.fill: parent

            Label { text: "Server ip:"}
            TextField {
                id: text_from_popup_login_server
                text: "192.168.1.35:51515"
                selectByMouse: true
            }


            Label { text: "Passwort:"}
            TextField {
                id: text_from_popup_login_passwort
                text: ""
                selectByMouse: true
                onEditingFinished: {
                    button_from_popup_login.clicked()
                }
            }
            Button {
                id: button_from_popup_login
                text: "Login"
                onClicked: {
                    rect_stock_search.ip = text_from_popup_login_server.text
                    rect_stock_search.secure_key = text_from_popup_login_passwort.text

                    python_stock_search.call("libs.send.get_server_name", [ip, secure_key], function(name) {
                        if (name === "") {
                            rect_stock_search.title = "ZK_DATA-STOCK v2 - keine verbindung"
                            text_from_popup_login_passwort.text = ""
                        } else {
                            rect_stock_search.title = "ZK_DATA-STOCK v2 - " + name                            

                            python_stock_search.call("libs.send.get_article_categories", [ip, secure_key], function(categories) {
                                stock_article_search.article_categories = []

                                for (var i = 0; i < categories.length; i++) {
                                    if (categories[i]["enable"] == true) {
                                        stock_article_search.article_categories.push({"identification": categories[i]["identification"], "name": categories[i]["identification"] + ": " + categories[i]["name"], "isChecked": false})
                                    }
                                }

                                popup_login.close()
                            })
                        }
                    })

                }
            }
        }
    }

    ZKLibs.ArticleSearch {
        id: stock_article_search
        width: rect_stock_search.width/2
        height: rect_stock_search.height - menu_stock.height

        ip: rect_stock_search.ip
        secure_key: rect_stock_search.secure_key

        border.width: 1
        color: rect_stock_search.color

        onSelected_articleChanged: {
            stock_article_change.article_identification = selected_article
            submenu_stock.enabled = true
            submenu_print.enabled = true
            stock_article_change.visible = true
        }
    }

    ZKLibs.ArticleChange {
        id: stock_article_change
        width: rect_stock_search.width/2
        height: rect_stock_search.height - menu_stock.height
        anchors.left: stock_article_search.right

        ip: rect_stock_search.ip
        secure_key: rect_stock_search.secure_key

        border.width: 1
        color: rect_stock_search.color

        visible: false

        onArticle_saved: {
            console.warn("onArticle_saved")
            python_stock_search.call("libs.script_modules.run_scripts", [rect_stock_search.ip, rect_stock_search.secure_key, article], function() {})
        }
    }


    Python {
        id: python_stock_search
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.BlueFunc', function () {});
            importModule('libs.barcode', function () {});
            importModule('libs.script_modules', function () {});
            importModule('os', function () {});

            popup_login.open()
        }

    }

}
