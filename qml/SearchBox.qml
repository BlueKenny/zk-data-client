import QtQuick 2.7
import QtQuick.Controls 2.0

Item {
    id: root

    width: 200
    height: 50
    z: 1

    property var liste: []
    property var icon_normal: "/usr/share/icons/gnome/256x256/actions/search.png"
    property var text: ""

    onFocusChanged: {if (focus == false) {searchbox_list_rectangle.visible = false}}
    onTextChanged: {searchbox_text.text = root.text}

    function search(text) {
        console.warn("search(" + text + ")")

        searchbox_icon.visible = false
        searching_indicator.visible = true

        searchModel.clear()
        for (var i = 0; i < liste.length; i++) {
            //console.warn(liste[i])
            if (liste[i].toLowerCase().indexOf(text.toLowerCase()) == -1 ) {
            } else {
                searchModel.append({"display": liste[i]})
            }
        }

        searchbox_list_rectangle.visible = true
        searchbox_icon.visible = true
        searching_indicator.visible = false
    }

    Rectangle {
        id: searchbox_rectangle

        width: parent.width
        height: parent.height

        border.width: 1
        //color: "red"
        radius: 10

        Image {
            id: searchbox_icon
            source: icon_normal
            height: searchbox_text.height
            fillMode: Image.PreserveAspectFit
        }
        BusyIndicator{
            id: searching_indicator
            visible: false
            anchors.fill: searchbox_icon
            z: parent.z + 10
        }
        TextField {
            id: searchbox_text

            anchors.left: searchbox_icon.right
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.margins: 5
            //width: parent.width - searchbox_icon.width
            selectByMouse: true

            //focus: true

            onFocusChanged: {if (focus == false) {searchbox_list_rectangle.visible = false}}

            Keys.onPressed: {
                if ( event.key === Qt.Key_Up ) {
                    searchbox_list.decrementCurrentIndex()
                }
                if ( event.key === Qt.Key_Down ) {
                    searchbox_list.incrementCurrentIndex()
                }
                if ( event.key === Qt.Key_Return ) {
                    searchbox_text.text = searchModel.get(searchbox_list.currentIndex).display
                    searchbox_list_rectangle.visible = false
                }
            }

            onTextChanged: {
                if (focus == true) {
                    search(searchbox_text.text)
                }
            }
        }

        Rectangle {
            id: searchbox_list_rectangle
            visible: false

            anchors.left: searchbox_text.left
            //anchors.right: searchbox_text.right
            anchors.top: searchbox_text.bottom
            anchors.topMargin: 10
            //anchors.bottom: parent.bottom
            height: 500
            width: 500
            z: searchbox_text.z + 10

            border.width: 1

            ListView {
                id: searchbox_list
                clip: true
                visible: parent.visible

                anchors.fill: parent

                ListModel {
                    id: searchModel
                }

                Component {
                    id: searchDelegate
                    Item {
                        id: itemListe
                        width: searchbox_text.width
                        height: searchbox_text.height
                        z: searchbox_list_rectangle.z + 10

                        MouseArea {
                            id: mouseare_item
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                //searchbox_text.text = display
                                root.text = display
                                searchbox_list_rectangle.visible = false
                            }
                        }

                        Label {
                            text: display
                            width: parent.width
                            font.pixelSize: mouseare_item.containsMouse ? 25 : 20

                        }

                    }
                }

                model: searchModel
                delegate: searchDelegate

            }
        }

    }

    Component.onCompleted: {
        //model: ["BE", "DE", "FR", "LU", "NL", "IT", "RO", ""]
    }
}










