import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.0
import QtQuick.Window 2.2

import QtCharts 2.2


Window {
    id: window
    title: qsTr("ZK-Data Bewegungen")
    x: 0
    y: 0
    width: 500
    height: 500

    Item {
        id: variable
        //property string monat: "x"
        //property int total: 0
    }

    function antwortStats(item) {
        chartGewinn.clear()
        for (var i=0; i<item.length; i++) {
            chartGewinn.append(i + 1, item[i])
            variable.total = variable.total + item[i]
        }
    }

    function busy(status) {
        busyindicator.visible = status
    }

    ChartView {
        id: chart
        title: ""
        y: 0
        width: window.width
        height: window.height - y
        antialiasing: true

        ValueAxis {
            id: valueAxisX
            min: 1
            max: 10
            tickCount: 11
            labelFormat: "%.0f"
        }
        ValueAxis {
            id: valueAxisY
            min: 0
            max: 10
            tickCount: 11
            labelFormat: "%.0f"
        }

        AreaSeries {
        //LineSeries {
            name: "Stock"
            color: "blue"
            axisX: valueAxisX
            axisY: valueAxisY

            upperSeries: LineSeries {
                id: chartGewinn
            }
        }
    }

    BusyIndicator {
        id: busyindicator
        running: true //image.status === Image.Loadings
        x: window.width / 2 - width / 2
        y: window.height / 2 - height / 2
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.BlueFunc', function () {});

            setHandler("busy", busy);
            setHandler("antwortStats", antwortStats);

            // Server soll jede funktion machen..

            // Tag abfragen ( pos X 21)
            // andere pos = tag = -1

            //min y = 0
            //max y = max von den letzten 21 tage

            //für jeden tag abfragen..

            call('libs.BlueFunc.Date', [], function(date) {
                console.warn("date: " + date)
            });

            //call('LieferscheinAnzeigen.main.GetLieferschein', [], function() {});
            //call('LieferscheinAnzeigen.main.GetIdentification', [], function(lieferscheinNummer) {labelLieferscheinAnzeigenTitle.text = "Lieferschein: " + lieferscheinNummer});
        }
    }
}
