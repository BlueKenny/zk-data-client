import QtQuick 2.7
import QtQuick.Controls 2.0
import io.thp.pyotherside 1.4

Item {
    id: root
    signal closed()
    signal accepted()
    signal declined()

    //property bool visible: false
    property string title_one: "AI-Helper Vorschlag"
    property string title_two: ""
    property string text: ""
    property string button_accepted_text: "Annehmen"
    property string button_declined_text: "Ablehnen (X)"

    property string border_color: "#80000000"

    function open_popup(title_two, text) {
        if (text !== "") {
            root.title_two = title_two
            root.text = text
            root.visible = true
        }
    }

    z: parent.z + 5
    visible: false
    anchors.fill: parent

    onVisibleChanged: {
        if (visible == true) {textfield.forceActiveFocus()}
    }

    Rectangle {
        id: rootRectangle
        anchors.fill: parent
        visible: root.visible
        z: 2
        color: root.border_color//"#80000000"
        Label {
            text: root.title_one
            x: rootRectangle.width / 2 - width / 2
            font.pixelSize: 35//vars.fontTitle
            y: rectangle.y / 2 - height / 2
        }

        MouseArea {
            id: mouseAreaClose
            anchors.fill: rootRectangle
            z: rootRectangle.z - 2
            onClicked: {
                if ( rectangle.x > mouseX | mouseX > (rectangle.x + rectangle.width) | rectangle.y > mouseY | mouseY > (rectangle.y + rectangle.height )) {
                    root.visible = false
                    Qt.inputMethod.hide()
                    root.closed()
                }
            }
        }
        Rectangle {
            id: rectangle
            z: rootRectangle.z + 1
            border.width: 2
            radius: 20
            height: button_accepted.height*3 + 30//parent.height / 4 * 3
            width: parent.width / 4 * 3
            anchors.centerIn: parent

            Label {
                id: label_title_two
                text: root.title_two
                anchors.top: parent.top
                anchors.topMargin: 10
                x: parent.width / 2 - width / 2
            }

            TextField {
                id: textfield
                text: root.text
                selectByMouse: true
                onTextChanged: {root.text = text}
                font.pixelSize: vars.fontText + 2
                anchors.top: label_title_two.bottom
                anchors.topMargin: 10
                x: parent.width / 2 - width / 2
                width: parent.width * 0.9
                onAccepted: {button_accepted.clicked()}
            }

            Button {
                id: button_accepted
                text: root.button_accepted_text
                onClicked: {
                    root.accepted()
                    root.visible = false
                    root.closed()
                }
                anchors.top: textfield.bottom
                anchors.topMargin: 10
                x: parent.width / 2 - width - 10

                background: Rectangle {
                        color: parent.down ? "lightgreen" :
                                (parent.hovered ? "lightgreen" : "lightgreen")
                }

            }
            Button {
                id: button_declined
                text: root.button_declined_text
                onClicked: {
                    root.declined()
                    root.visible = false
                    root.closed()
                }
                anchors.left: button_accepted.right
                anchors.leftMargin: 10
                anchors.top: textfield.bottom
                anchors.topMargin: 10
                background: Rectangle {
                        color: parent.down ? "red" :
                                (parent.hovered ? "red" : "red")
                }

            }

        }
    }

}
