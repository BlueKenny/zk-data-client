import QtQuick 2.7
import io.thp.pyotherside 1.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.0
//import Qt.labs.platform 1.0

Rectangle {
    id: window
    width: mainWindow.width
    anchors.bottom: parent.bottom

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    property string file_data: ""
    property var trennzeichen_liste: [";", '","', ":", ","]
    property string trennzeichen: ""
    property var headers_objects: ["", "datum", "uhrzeit", "betrag", "konto_out", "mitteilung"]

    function load_next_konto(index) {
        python.call("libs.send.get_konto", [vars.serverIP, vars.secure_key, index], function(konto) {
            konto["value"] = konto["identification"]

            if (konto !== -1) {
                transaktion_add_Model.append(konto)

                load_next_konto(index + 1)
            }
        })

    }

    function readTextFile(fileUrl){
        var xhr = new XMLHttpRequest;
        xhr.open("GET", fileUrl); // set Method and File
        xhr.onreadystatechange = function () {
            if(xhr.readyState === XMLHttpRequest.DONE){ // if request_status == DONE
                var response = xhr.responseText;
                file_data = response;

                load_trennzeichen()
            }
        }
        xhr.send(); // begin the request
    }

    function load_trennzeichen() {
        console.warn("load_trennzeichen()")
        trennzeichen_model.clear()

        for (var i = 0; i < trennzeichen_liste.length; i++) {
            if ( (file_data.split("\n")[0]).indexOf( trennzeichen_liste[i] ) > -1 ) {
                console.warn("Trennzeichen gefunden [" + trennzeichen_liste[i] + "]")
                trennzeichen_model.append({"text": trennzeichen_liste[i]})
            }
        }

        comboBox_trennzeichen.currentIndex = 0
        //trennzeichen = ","//comboBox_trennzeichen.currentText
    }

    function load_headers() {
        console.warn("load_headers()")
        busyindicator.visible = true
        button_import.visible = false

        headers_model.clear()

        //var index_erste_linie = 0
        //if (checkBox_ignore_first_line.checked == true) {
        //    index_erste_linie = 1
        //}

        var anzahl_spalten = file_data.split("\n")[0].split(trennzeichen).length
        console.warn("anzahl_spalten: " + anzahl_spalten)

        for (var i = 0; i < anzahl_spalten; i++) {
            headers_model.append({"header_test_text": file_data.split("\n")[0].split(trennzeichen)[i].replace('"', ''), "header_value":  0})
        }

        busyindicator.visible = false
    }

    function test_import() {
        console.warn("test_import()")
        busyindicator.visible = true

        // "datum", "uhrzeit", "betrag", "konto_out", "mitteilung"
        transaktion_model.clear()

        console.warn("trennzeichen: " + trennzeichen)

        var index_erste_linie = 0
        if (checkBox_ignore_first_line.checked == true) {
            index_erste_linie = 1
        }

        for (var linie=index_erste_linie; linie < file_data.split("\n").length ; linie++) {
            if (file_data.split("\n")[linie].indexOf(trennzeichen) > -1) {
                //console.warn("linie: " + linie)
                var test_dict = {}
                test_dict["datum"] = "2021-01-01"
                test_dict["uhrzeit"] = "00:00"
                test_dict["betrag"] = "0.0"
                test_dict["konto_out"] = ""
                test_dict["mitteilung"] = ""
                test_dict["status"] = "?"

                for (var i=0; i < headers_model.count; i++) {
                    var key_spalte = i //headers_model.get(i)["header_value"]
                    var key_name = headers_objects[ headers_model.get(i)["header_value"] ]
                    var value = file_data.split("\n")[linie].split(trennzeichen)[key_spalte]

                    if (key_name !== "") {
                        test_dict[key_name] = value.replace('"', '')
                        console.warn("value: " + test_dict[key_name])
                    }
                }

                //console.warn("test_dict: " + test_dict)
                transaktion_model.append(test_dict)
            }

        }

        button_import.visible = true
        busyindicator.visible = false
    }

    function import_data() {
        console.warn("import_data()")

        button_test_import.visible = false


        import_data_index(0)

        busyindicator.visible = false
    }

    function import_data_index(index) {
        busyindicator.visible = true

        var datum = ""
        var uhrzeit = ""
        var betrag = 0.0
        var konto_in = 0
        var konto_out = ""
        var mitteilung = ""

        if (index < transaktion_model.count) {
            console.warn("index: " + index)

            datum = transaktion_model.get(index).datum
            uhrzeit = transaktion_model.get(index).uhrzeit
            betrag = transaktion_model.get(index).betrag
            konto_in = comboBox_konto.currentIndex + 1
            konto_out = transaktion_model.get(index).konto_out
            mitteilung = transaktion_model.get(index).mitteilung

            python.call("libs.send.create_transaktion_free", [vars.serverIP, vars.secure_key, datum, uhrzeit, betrag, konto_in, konto_out, mitteilung], function(answer) {
                if(answer === 1) {
                    transaktion_model.get(index).status = "Erfolgreich"
                } else {
                    if(answer === 0) {
                        transaktion_model.get(index).status = "Existier bereits"
                    } else {
                        transaktion_model.get(index).status = "Error"
                    }
                }
                import_data_index(index + 1)
            })


        }
/*
        for (var linie = 0; linie < transaktion_model.count; linie++) {
            console.warn("linie: " + linie)
            datum = transaktion_model.get(linie).datum
            uhrzeit = transaktion_model.get(linie).uhrzeit
            betrag = transaktion_model.get(linie).betrag
            konto_out = transaktion_model.get(linie).konto_out
            mitteilung = transaktion_model.get(linie).mitteilung

            python.call("libs.send.create_transaktion_free", [vars.serverIP, vars.secure_key, datum, uhrzeit, betrag, konto_out, mitteilung], function(answer) {
                if(answer === 1) {
                    transaktion_model.get(linie).status = "Erfolgreich"
                } else {
                    if(answer === 0) {
                        transaktion_model.get(linie).status = "Existier bereits"
                    } else {
                        transaktion_model.get(linie).status = "Error"
                    }
                }
            })

        }*/


        busyindicator.visible = false
    }

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        onClicked: {
            view.push(frameSelect)
        }
    }

    FileDialog {
        id: fileDialog
        title: "CSV datei zum Import wählen"
        folder: shortcuts.home
        //selectMultiple: false
        nameFilters: [ "CSV dateien (*.csv)" ]
        onAccepted: {
            console.log("CSV datei: " + fileDialog.fileUrl)
            readTextFile(fileDialog.fileUrl)
        }
        onRejected: {
            console.log("Canceled")
            view.push(frameSelect)
        }
    }

    Label {
        id: label_dateipfad
        font.pixelSize: vars.fontTitle
        x: parent.width / 2 - width / 2
        text: fileDialog.fileUrl
    }

    Label {
        id: label_comboBox_trennzeichen
        anchors.top: buttonBack.bottom
        anchors.topMargin: 20
        text: "Trennzeichen: "
    }
    ComboBox {
        id: comboBox_trennzeichen
        anchors.top: buttonBack.bottom
        anchors.topMargin: 20
        anchors.left: label_comboBox_trennzeichen.right
        anchors.leftMargin: 20

        model: ListModel {
            id: trennzeichen_model
        }
        onCurrentTextChanged: {
            trennzeichen = comboBox_trennzeichen.currentText
            load_headers()
            vars.busy = true
        }
    }

    Label {
        id: label_comboBox_konto
        anchors.top: comboBox_trennzeichen.bottom
        anchors.topMargin: 20
        text: "Konto: "
    }
    ComboBox {
        id: comboBox_konto
        anchors.top: comboBox_trennzeichen.bottom
        anchors.topMargin: 20
        anchors.left: label_comboBox_konto.right
        anchors.leftMargin: 20
        textRole: "name"

        model: transaktion_add_Model

        ListModel {
            id: transaktion_add_Model
        }
        onCurrentTextChanged: {
            button_import.visible = false
        }

    }

    CheckBox {
        id: checkBox_ignore_first_line
        text: "Erste linie ignorieren"
        anchors.top: buttonBack.bottom
        anchors.topMargin: 20
        anchors.left: comboBox_trennzeichen.right
        anchors.leftMargin: 20
        onCheckedChanged: {
            button_import.visible = false
        }
    }

    RoundButton {
        id: button_import
        text: "Import"
        anchors.right: list_headers.left
        anchors.rightMargin: 20
        anchors.bottom: list_headers.bottom
        anchors.bottomMargin: 20
        visible: false
        onClicked: {
            busyindicator.visible = true
            import_data()
        }
    }

    RoundButton {
        id: button_test_import
        text: "Laden"
        anchors.right: button_import.left
        anchors.rightMargin: 20
        anchors.bottom: list_headers.bottom
        anchors.bottomMargin: 20
        onClicked: {
            busyindicator.visible = true
            test_import()
        }
    }
    GridView {
        id: list_headers
        anchors.top: buttonBack.bottom
        anchors.topMargin: 20
        anchors.left: checkBox_ignore_first_line.right
        anchors.leftMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 20
        height: 200
        cellWidth: 200; cellHeight: 100
        clip: true
        //orientation: ListView.Horizontal


        model: ListModel {id: headers_model}

        Component {
            id: headers_delegate
            Rectangle {
                border.width: 1
                width: 200
                height: 100
                Label {
                    id: label_headers
                    text: header_test_text
                    x: 10
                }
                ComboBox {
                    id: comboBox_headers_objects
                    anchors.top: label_headers.bottom
                    anchors.topMargin: 20
                    width: parent.width - 20
                    x: 10

                    model: headers_objects
                    onCurrentTextChanged: {
                        header_value = comboBox_headers_objects.currentIndex
                        //test_import()
                    }

                }
            }
        }
        delegate: headers_delegate
    }


    Rectangle {
        id: rectangle_transaktion_list
        border.width: 1
        width: parent.width
        anchors.top: list_headers.bottom
        anchors.topMargin: 40
        anchors.bottom: parent.bottom

        Rectangle {
            id: rectangle_titles
            height: 50
            width: parent.width
            Label {
                id: label_title_datum
                text: "datum"
                width: parent.width / 10
            }
            Label {
                id: label_title_uhrzeit
                text: "uhrzeit"
                width: parent.width / 10
                x: parent.width / 10 * 2
            }
            Label {
                id: label_title_betrag
                text: "betrag"
                width: parent.width / 7
                x: parent.width / 7 * 3
            }
            Label {
                id: label_title_konto_out
                text: "konto_out"
                width: parent.width / 7
                x: parent.width / 7 * 4
            }
            Label {
                id: label_title_mitteilung
                text: "mitteilung"
                width: parent.width / 7
                x: parent.width / 7 * 5
            }
            Label {
                id: label_title_status
                text: "status"
                width: parent.width / 7
                x: parent.width / 7 * 6
            }
        }
        ListView {
            id: list_transaktionen
            clip: true
            anchors.top: rectangle_titles.bottom
            anchors.topMargin: 10

            anchors.bottom: parent.bottom
            width: parent.width

            model: ListModel {id: transaktion_model}

            Component {
                id: transaktion_delegate
                Rectangle {
                    height: 50
                    width: parent.width

                    Label {
                        id: label_title_datum
                        text: datum
                        width: parent.width / 10
                    }
                    Label {
                        id: label_title_uhrzeit
                        text: uhrzeit
                        width: parent.width / 10
                        x: parent.width / 10 * 2
                    }
                    Label {
                        id: label_title_betrag
                        text: betrag
                        width: parent.width / 7
                        x: parent.width / 7 * 3
                    }
                    Label {
                        id: label_title_konto_out
                        text: konto_out
                        width: parent.width / 7
                        x: parent.width / 7 * 4
                    }
                    Label {
                        id: label_title_mitteilung
                        text: mitteilung
                        width: parent.width / 7
                        x: parent.width / 7 * 5
                    }
                    Label {
                        id: label_title_status
                        text: status
                        width: parent.width / 7
                        x: parent.width / 7 * 6
                    }
                }
            }

            delegate: transaktion_delegate
        }
    }

    BusyIndicator {
        id: busyindicator
        running: true
        x: window.width / 2 - width / 2
        y: window.height / 2 - height / 2
        height: parent.height / 4
        width: parent.width / 4
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});


            fileDialog.visible = true
            //label_dateipfad.text = "file:///home/kenny/mutations20201221104053.csv"
            //label_dateipfad.text = "file:///home/kenny/document.csv" //"file:///home/kenny/mutations20201221104053.csv"
            //readTextFile(label_dateipfad.text)


            transaktion_add_Model.clear()
            load_next_konto(0)
        }
    }
}

