import QtQuick 2.7
import QtQuick.Controls 2.0

Rectangle {
    id: componentRoot
    y: mainWindow.height / 2 - height / 2
    x: mainWindow.width / 2 - width / 2
    height: vars.isPhone ? mainWindow.height / 4 : mainWindow.height / 8
    width: vars.isPhone ? mainWindow.width / 4 : mainWindow.width / 8
    visible: vars.busy
    color: "transparent"

    BusyIndicator{
        id: componentIndicator
        visible: vars.busy
        width: componentRoot.width
        height: componentRoot.height
        z: mainWindow.z + 10
    }
}
