import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0


Rectangle {
    id: window
    color: vars.colorBackground

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        onClicked: {
            view.push(frameSelect)
        }
    }

    Label {
        id: label_title
        text: "AdminTools"
        font.pixelSize: vars.fontTitle
        x: parent.width / 2 - width / 2
        y: buttonBack.height / 2 - height / 2
    }

    Rectangle {
        id: rectangle_tools
        anchors.top: buttonBack.bottom
        anchors.topMargin: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        width: parent.width

        color: "transparent"

        ListView {
            id: list_tools
            clip: true

            anchors.fill: parent

            model: ListModel {
                id: model_tools
            }

            Component {
                id: delegate_tools

                Item {
                    height: 50
                    width: parent.width

                    Label {
                        id: tool_name_label
                        text: command_name
                        x: parent.width / 2 - width / 2
                        font.pixelSize: vars.fontTitle

                        MouseArea {
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                python.call("os.system", ["cd /etc/zk-data-libs/libs/AdminTools/ && gnome-terminal -- /etc/zk-data-libs/libs/AdminTools/" + command + " " + vars.serverIP + " " + vars.secure_key])
                            }
                            onEntered: {tool_name_label.font.pixelSize = vars.fontTitle + 8}
                            onExited: {tool_name_label.font.pixelSize = vars.fontTitle}
                        }
                    }
                }
            }
            delegate: delegate_tools
        }

    }


    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('os', function () {});

            call("os.listdir", ["/etc/zk-data-libs/libs/AdminTools/"], function(tools) {
                var tools_list = tools.sort()

                //console.warn("tools: " + tools)
                for (var i = 0; i < tools_list.length; i++) {
                    console.warn("tools_list: " + tools_list[i])
                    model_tools.append({"command_name": tools_list[i].replace(".py", ""), "command": tools_list[i]})
                }
            })
        }
    }

}
