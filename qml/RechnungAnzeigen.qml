import QtQuick 2.7
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.0

Rectangle {
    id: window_rechnung_anzeigen
    color: vars.colorBackground

    property var mail_to: ""
    property var rechnung_title: ""

    property var rechnung_rest: "0"

    Keys.onPressed: {
        console.warn("key " + event.key + " pressed")
        if ( event.key === Qt.Key_F1 ) {
            vars.lieferscheinSuchen_Angebote = false
            //key_F1()
            view.push(frameLieferscheinSuchen)
        }
        if ( event.key === Qt.Key_F2 ) {
            //if (vars.userData["permission_invoice"] > 0) {
            view.push(frameRechnungSuchen)
            //}
        }
        if ( event.key === Qt.Key_F3 ) {
            //if (vars.userData["permission_invoice_e"] > 0) {
            view.push(frameRechnungESuchen)
            //}
        }
        if ( event.key === Qt.Key_F12 ) {
            python.call("libs.send.delete_invoice_pdf", [vars.serverIP, vars.secure_key, vars.rechnungID], function() {})
        }
    }

    function load_next_konto() {
        for (var i = 0; i < vars.transaction_konto.length; i++) {
            var konto = vars.transaction_konto[i]
            console.warn(i + ": " + konto)
            konto["value"] = konto["identification"]

            if (konto !== -1) {// todo show all if admin

                //if (konto["nutzung_kasse"] == true) {
                if (konto["enable"] == true) {
                    transaktion_add_Model.append(konto)
                }

            }
            if (transaktion_add_Model.count > 0) {
                rectangle_new_transaktion.visible = true
                if (transaktion_add_Model.count > vars.rechnungAnzeigenZahlungsart) {
                    console.warn("vars.rechnungAnzeigenZahlungsart: " + vars.rechnungAnzeigenZahlungsart)
                    combobox_new_transaktion.currentIndex = vars.rechnungAnzeigenZahlungsart
                }
            }
        }

    }

    function get_rechnung() {
        console.warn("get_rechnung() " + vars.rechnungID)
        vars.busy = true
        vars.login_timeout = vars.login_max_timeout

        rv_Model.clear()
        labelKunde.text = "Kunde: "

        python.call("libs.send.get_rechnung", [vars.serverIP, vars.secure_key, vars.rechnungID], function(rechnung) {
            //console.warn("Rechnung: " + rechnung)

            var lieferscheine = rechnung["lieferscheine"].split("|")
            console.warn("lieferscheine: " + lieferscheine)

            transaktion_Model.clear()

            if (vars.rechnungID.indexOf("RV") === 0) {
                buttonLieferscheinAdd.visible = true
                button_rechnung_erstellen.visible = true
                button_kassenverkauf.visible = true

                window_rechnung_anzeigen.rechnung_title = "Vorlage: " + vars.rechnungID
            } else {
                buttonLieferscheinAdd.visible = false
                button_rechnung_erstellen.visible = false
                button_kassenverkauf.visible = false

                if (vars.rechnungID.indexOf("R") === 0) {
                    window_rechnung_anzeigen.rechnung_title = "Rechnung: " + vars.rechnungID
                } else {
                    window_rechnung_anzeigen.rechnung_title = "Kassenverkauf: " + vars.rechnungID
                }

                python.call("libs.send.search_transaktion_by_rechnung", [vars.serverIP, vars.secure_key, vars.rechnungID], function(answer_list) {
                    if (answer_list === "") {
                        console.warn("no transaktion for invoice")
                    } else {
                        answer_list = answer_list.split("|")
                        console.warn("transaktion list: [" + answer_list + "]")
                        console.warn("length of transaktion list: " + answer_list.length)
                        if (answer_list.length > 0) {
                            for (var i = 0; i < answer_list.length; i++) {
                                print("transaktion i: " + answer_list[i])
                                python.call("libs.send.get_transaktion", [vars.serverIP, vars.secure_key,  answer_list[i] ], function(transaktion_item) {
                                    transaktion_Model.append(transaktion_item)
                                })
                            }
                        }
                    }
                })

                transaktion_add_Model.clear()
                load_next_konto()

                text_new_transaktion_wert.enabled = true
                text_new_transaktion_pin.enabled = true

            }

            if (rechnung["lieferscheine"] !== "") {
                for (var i = 0; i < lieferscheine.length; i++) {
                    print("lieferschein i: " + lieferscheine[i])
                    rv_Model.append({ "lieferschein_id": "L" + lieferscheine[i] })
                }
            }

            labelKunde.text = "Kunde: " + rechnung.kunde_id
            python.call("libs.send.GetKunden", [vars.serverIP, vars.secure_key, rechnung.kunde_id], function(kunde) {
                labelKunde.text = labelKunde.text + "\n" + kunde["name"]
                labelKunde.text = labelKunde.text + "\n" + kunde["ort"] + ", " + kunde["adresse"]
                labelKunde.text = labelKunde.text + "\n" + kunde["tva"]

                if (rechnung.kunde_id === "0") {
                    button_rechnung_erstellen.enabled = false
                } else {
                    button_rechnung_erstellen.enabled = true

                    button_mail.visible = true

                    if (vars.rechnungID.indexOf("RV") === 0) {
                        if( kunde["rappel"] > 0 ) {
                            button_rechnung_erstellen.color = "orange"
                            button_rechnung_erstellen.enabled = true
                        }
                    }

                    if (kunde["email3"].indexOf("@") !== -1) {
                        window_rechnung_anzeigen.mail_to = kunde["email3"]

                        if (kunde["email3"] === "POST@POST") {
                            window_rechnung_anzeigen.mail_to = ""
                            button_mail.enabled = false
                            button_mail.color = "orange"
                        }
                    } else {
                        print("No email found for this client, hiding mail button")
                        button_mail.visible = false
                    }
                }

            })

            label_rechnung_ref.text = "Ref: " + rechnung.ref
            label_rechnung_zahlen_htva.text = "HTVA: " + rechnung.total_ohne_steuern
            label_rechnung_zahlen_steuern.text = "Steuern: \n" + rechnung.steuern.replace("|", "\n").replace("|", "\n").replace("|", "\n").replace("|", "\n").replace(";", ":    ").replace(";", ":    ").replace(";", ":    ")
            label_rechnung_zahlen_total.text = "Gesamt: " + rechnung.total + " €"
            label_rechnung_zahlen_rest.text = "Rest: " + rechnung.rest + " €"

            label_rechnung_erstellt_von_benutzer.text = "Erstellt von " + rechnung.created_by

            rechnung_rest = rechnung.rest


            if (typeof rechnung.rappel !== "undefined") {
                button_select_date.visible = true
                label_rappel.text = "Erinnerungen: " + rechnung.rappel
            } else {
                button_select_date.visible = false
                label_rappel.text = "Erinnerungen: ??"
            }

            calendar_invoice_todo.selected_date = rechnung.datum_todo
            button_select_date.text = "Zahlungsdatum: " + rechnung.datum_todo

            vars.busy = false
        })
    }

    Label {
        id: label_rechnung_erstellt_von_benutzer
        text: ""
        anchors.top: parent.top
        anchors.right: parent.right
        font.pixelSize: vars.fontText
    }

    ButtonSelect {
        id: buttonBack
        text: "Hauptmenu"
        height: vars.backButtonHeight
        width: vars.backButtonWidth
        //font.pixelSize: vars.fontText
        onClicked: {
            var save_rechnung = vars.rechnungID
            vars.rechnungID = ""
            vars.rechnungID = save_rechnung
            view.push(frameSelect)
        }
    }

    Label {
        id: label_rappel
        text: "Erinnerungen: ?"
        anchors.left: buttonBack.right
        anchors.leftMargin: 10
    }

    gradient: Gradient {
        GradientStop { position: 0.2; color: "lightsteelblue" }
        GradientStop { position: 1.5; color: "steelblue" }
    }

    Label {
        id: rechnung_title
        text: vars.rechnungID
        y: buttonBack.height / 2 - height / 2
        x: parent.width / 2 - width / 2
        font.pixelSize: vars.fontTitle
        onTextChanged: {
            get_rechnung()
        }
    }

    Popup {
        id: popup
        x: 100
        y: 100
        width: 200
        height: 300
        modal: true

        /*
        TextInput {
            id: textAreaNewLieferschein
            anchors.fill: parent
            focus: true
        }*/
        TextInput {
            id: textinput_lieferschein
            anchors.fill: parent
            selectByMouse: true

            onAccepted: {popup.close()}
        }
        onClosed: {
            python.call("libs.send.add_lieferschein_to_vorlage", [vars.serverIP, vars.secure_key, textinput_lieferschein.text, vars.rechnungID], function(sucess) {
                if (sucess == true) {
                    get_rechnung()
                    textAreaNewLieferschein.text = ""
                }
            })
        }
        onOpened: {
            textAreaNewLieferschein.forceActiveFocus()
        }
    }

    Rectangle {
        id: rectangleLieferscheinListe
        //border.width: 1
        height: 200
        color: "transparent"
        anchors.top: buttonBack.bottom
        anchors.topMargin: 10
        anchors.leftMargin: 50
        anchors.rightMargin: 50
        anchors.left: parent.left
        anchors.right: parent.right

        ButtonSelect {
            id: buttonLieferscheinAdd
            text: "Lieferschein hinzufügen +"
            anchors.right: parent.right
            anchors.topMargin: 10

            width: 200
            height: parent.height - 20

            onClicked: {
                popup.open()

            }
        }

        ListView {
            id: listLieferscheineAnzeigen

            anchors.left: parent.left
            anchors.right: buttonLieferscheinAdd.left
            height: parent.height

            orientation: ListView.Horizontal

            anchors.top: buttonBack.bottom
            clip: true

            ListModel {
                id: rv_Model
            }
            Component {
                id: rv_Delegate
                Item {
                    id: itemListe
                    width: 200
                    height: parent.height

                    Rectangle {
                        border.width: 1

                        width: 200
                        height: parent.height

                        Label {
                            id: itemListe_lieferschein_id
                            text: lieferschein_id
                            anchors.centerIn: parent
                            font.pixelSize: 35
                        }
                        ButtonSelect {
                            id: itemListe_lieferschein_entfernen
                            anchors.top: parent.top
                            anchors.right: parent.right
                            height: 35
                            width: 35
                            text: "X"
                            color: "orange"

                            visible: button_kassenverkauf.visible

                            onClicked: {
                                python.call("libs.send.remove_lieferschein_from_vorlage", [vars.serverIP, vars.secure_key, lieferschein_id, vars.rechnungID], function() {
                                    get_rechnung()
                                })

                            }
                        }
                        MouseArea {
                            id: itemListe_mouseArea
                            anchors.top: itemListe_lieferschein_entfernen.bottom
                            anchors.left: parent.left
                            anchors.right: parent.right
                            anchors.bottom: parent.bottom

                            acceptedButtons: Qt.LeftButton | Qt.RightButton

                            hoverEnabled: true

                            onEntered: {
                                //itemListe_lieferschein_entfernen.visible = true
                                itemListe_lieferschein_id.font.pixelSize = 50
                            }
                            onExited: {
                                //itemListe_lieferschein_entfernen.visible = false
                                itemListe_lieferschein_id.font.pixelSize = 35
                            }
                            onClicked: {                                
                                if(mouse.button & Qt.RightButton) {
                                    vars.busy = true
                                    python.call("libs.send.get_delivery_pdf", [vars.serverIP, vars.secure_key, itemListe_lieferschein_id.text], function(pdf_path){
                                        python.call("os.system", ["wget -q -N http://" + vars.serverIP + ":51515" + pdf_path + " -P /tmp/ && evince /tmp/L" + itemListe_lieferschein_id.text + ".pdf"], function() {
                                            vars.busy = false
                                        })
                                    })
                                    /*
                                    python.call("PrinterSelect.main.Drucken", [vars.serverIP, vars.secure_key, 0, itemListe_lieferschein_id.text, false, 2], function(sucess) {
                                        console.warn("sucess of Printing " + sucess)
                                        if (sucess == true) {
                                            python.call("os.system",["evince http://" + vars.serverIP + "/zk-data/Lieferscheine/" + itemListe_lieferschein_id.text + ".pdf &"], function() {});
                                        }
                                        vars.busy = false
                                    });
                                    */

                                } else {
                                    vars.lieferscheinID = itemListe_lieferschein_id.text.replace("L", "")
                                    view.push(frameLieferscheinAnzeigen)


                                }


                            }
                        }

                    }

                }
            }

            model: rv_Model
            delegate: rv_Delegate

        }
    }

    Rectangle {
        id: rectangle_kunden_daten
        anchors.top: rectangleLieferscheinListe.bottom
        anchors.left: parent.left
        anchors.leftMargin: 50
        height: 100
        width: 400
        border.width: 1
        color: "transparent"
        Label {
            id: labelKunde
            text: "Kunde: "
            font.pixelSize: vars.fontText
            anchors.fill: parent
        }
    }

    Rectangle {
        id: rectangle_rechnung_zahlen
        anchors.top: rectangle_kunden_daten.top
        anchors.left: rectangle_kunden_daten.right
        anchors.right: rectangleLieferscheinListe.right
        height: rectangle_kunden_daten.height
        border.width: 1
        color: "transparent"

        Label {
            id: label_rechnung_ref
            anchors.bottom: parent.bottom
            font.pixelSize: vars.fontText
        }

        Label {
            id: label_rechnung_zahlen_htva
            font.pixelSize: vars.fontText
        }
        Label {
            id: label_rechnung_zahlen_steuern
            anchors.left: label_rechnung_zahlen_htva.right
            anchors.leftMargin: 50
            font.pixelSize: vars.fontText
        }
        Label {
            id: label_rechnung_zahlen_total
            anchors.left: label_rechnung_zahlen_steuern.right
            anchors.leftMargin: 50
            font.pixelSize: vars.fontText
        }
        Label {
            id: label_rechnung_zahlen_rest
            anchors.left: label_rechnung_zahlen_steuern.right
            anchors.top: label_rechnung_zahlen_total.bottom
            anchors.leftMargin: 50
            anchors.topMargin: 25
            font.pixelSize: vars.fontText + 5
        }
        ButtonSelect {
            id: button_select_date
            text: "Zahlungsdatum"
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.rightMargin: 10
            anchors.topMargin: 10
            height: parent.height / 2
            onClicked: {
                if (rechnung_rest !== 0.0) {
                    calendar_invoice_todo.visible = true
                }
            }

        }


    }

    ButtonSelect {
        id: button_vorschau
        text: "Anzeigen"
        anchors.left: rectangle_kunden_daten.left
        anchors.top: rectangle_kunden_daten.bottom
        height: rectangle_kunden_daten.height - 20
        anchors.topMargin: 10
        anchors.leftMargin: 10

        onClicked: {
            vars.busy = true

            python.call("libs.send.get_invoice_pdf", [vars.serverIP, vars.secure_key, vars.rechnungID], function(pdf_path){
                python.call("os.system", ["wget -q -N http://" + vars.serverIP + ":51515" + pdf_path + " -P /tmp/ && evince /tmp/" + vars.rechnungID + ".pdf"], function() {
                    vars.busy = false
                })
            })

            /*
            python.call("libs.send.print_invoice", [vars.serverIP, vars.secure_key, vars.rechnungID], function(link) {
                python.call("os.system",["evince /tmp/zk-data_pdf/" + vars.serverIP + "/" + vars.rechnungID + ".pdf &"], function() { });
                vars.busy = false
            })
            */
            /*
            python.call("PrinterSelect.main.Drucken", [vars.serverIP, vars.secure_key, 0, vars.rechnungID, true, 1], function(sucess) {
                console.warn("sucess of Printing " + sucess)
                if (sucess == true) {
                    python.call("os.system",["evince http://" + vars.serverIP + "/zk-data/Rechnung/" + vars.rechnungID + ".pdf &"], function() {});
                }
                vars.busy = false
            })
            */
        }
    }


    ButtonSelect {
        id: button_rechnung_erstellen
        text: "Rechung erstellen"
        anchors.left: rectangle_kunden_daten.right
        anchors.top: rectangle_kunden_daten.bottom
        height: rectangle_kunden_daten.height - 20
        anchors.topMargin: 10
        anchors.leftMargin: 10

        onClicked: {
            label_info_text.text = "Rechnung erstellen: "
            popupPin.open()
            textAreaPin.forceActiveFocus()
        }
    }

    ButtonSelect {
        id: button_kassenverkauf
        text: "Kassenverkauf"
        anchors.left: button_rechnung_erstellen.right
        anchors.top: rectangle_kunden_daten.bottom
        height: button_rechnung_erstellen.height
        anchors.topMargin: 10
        anchors.leftMargin: 10
        onClicked: {
            label_info_text.text = "Kassenverkauf erstellen: "
            popupPin.open()
            textAreaPin.forceActiveFocus()
        }
    }

    Popup {
        id: popupPin
        x: 100
        y: 100
        width: 200
        height: 300
        modal: true
        Label {
            id: label_info_text
            text: ""
            x: parent.width / 2 - width / 2
        }
        Label {
            id: label_benutzer_pin
            text: "Bitte benutzer Pin eingeben"
            anchors.top: label_info_text.bottom
            x: parent.width / 2 - width / 2
        }
        TextInput {
            id: textAreaPin
            anchors.top: label_benutzer_pin.bottom
            anchors.topMargin: 20
            anchors.bottom: parent.bottom
            width: parent.width
            onFocusChanged: console.log("Focus changed " + focus)
            selectByMouse: true
            echoMode: TextInput.Password
            onAccepted: {popupPin.close()}
        }
        onClosed: {
            var rechnung_mode = ""
            if (label_info_text.text.indexOf("Rechnung") === 0) {
                rechnung_mode = "R"
            } else {
                rechnung_mode = "K"
            }
            console.warn("rechnung_mode: " + rechnung_mode)
                console.warn("create Rechnung / Kassenverkauf")
                python.call("libs.send.create_rechnung", [vars.serverIP, vars.secure_key, rechnung_mode, vars.rechnungID, textAreaPin.text], function(answer) {
                    if (answer === -1) {
                        console.warn("ERROR")
                    } else {
                        vars.rechnungID = answer
                    }
                })
            //} else {
            //    console.warn("create Kassenverkauf")
            //}
        }
    }

    Rectangle {
        id: rectangle_new_transaktion
        border.width: 1

        anchors.left: button_vorschau.right
        anchors.leftMargin: 10
        anchors.top: rectangle_kunden_daten.bottom
        anchors.bottom: rectangle_transaktion.top
        anchors.right: buttonLieferscheinAdd.right

        visible: false

        Label {
            id: label_transaktion_add_title
            text: "Neue Zahlung"
            anchors.leftMargin: 10
            y: parent.height / 2 - height / 2
        }

        ComboBox {
            id: combobox_new_transaktion
            model: transaktion_add_Model//["Barzahlung", "Bankkontakt"]
            y: parent.height / 2 - height / 2
            anchors.left: label_transaktion_add_title.right
            anchors.leftMargin: 10
            width: button_vorschau.width
            height: button_vorschau.height / 2
            textRole: "name"

            ListModel {
                id: transaktion_add_Model
            }
            onCurrentIndexChanged: {
                if (focus == true) {
                    text_new_transaktion_wert.forceActiveFocus()
                    vars.rechnungAnzeigenZahlungsart = currentIndex
                    console.warn("vars.rechnungAnzeigenZahlungsart: " + vars.rechnungAnzeigenZahlungsart)
                }
            }
        }

        TextField {
            id: text_new_transaktion_wert
            anchors.left: combobox_new_transaktion.right
            anchors.leftMargin: 10
            y: parent.height / 2 - height / 2
            selectByMouse: true
            placeholderText: "Summe"
            enabled: false
            inputMethodHints: Qt.ImhDigitsOnly
            //validator: DoubleValidator {}
            onAccepted: {
                if (text == "") {
                    text = rechnung_rest
                }
                text_new_transaktion_pin.forceActiveFocus()
            }
        }
        TextField {
            id: text_new_transaktion_pin
            anchors.left: text_new_transaktion_wert.right
            anchors.leftMargin: 10
            y: parent.height / 2 - height / 2
            placeholderText: "Pin"
            selectByMouse: true
            enabled: false
            echoMode: TextInput.Password
            //visible: text_new_transaktion_wert.acceptableInput ? true : false
            onVisibleChanged: {
                text_new_transaktion_pin.text = ""
            }
            onAccepted: {
                text_new_transaktion_wert.enabled = false
                text_new_transaktion_pin.enabled = false

                python.call("libs.send.create_transaktion", [vars.serverIP, vars.secure_key, transaktion_add_Model.get(combobox_new_transaktion.currentIndex).identification, text_new_transaktion_wert.text, vars.rechnungID, text_new_transaktion_pin.text], function(sucess) {
                    if (sucess == true) {
                        text_new_transaktion_pin.clear()
                        text_new_transaktion_wert.clear()
                        get_rechnung()
                    } else {
                        text_new_transaktion_pin.clear()
                        get_rechnung()
                    }
                })
            }
        }

    }


    ButtonSelect {
        id: button_mail
        text: "Mail"
        anchors.right: rectangle_rechnung_zahlen.right
        anchors.top: rectangle_kunden_daten.bottom
        height: rectangle_kunden_daten.height - 20
        anchors.topMargin: 10
        anchors.leftMargin: 10

        onClicked: {
            vars.busy = true
            python.call("libs.send.get_invoice_pdf", [vars.serverIP, vars.secure_key, vars.rechnungID], function(pdf_path) {

                python.call("os.system",["wget -q -N http://" + vars.serverIP + ":51515" + pdf_path + " -P /tmp/"], function() {
                    python.call("os.system", ["thunderbird -compose to='" + window_rechnung_anzeigen.mail_to + "',subject='" + window_rechnung_anzeigen.rechnung_title + "',body='',attachment='/tmp/" + vars.rechnungID + ".pdf'"], function() {
                        vars.busy = false
                    })
                });

            })

            /*
            python.call("PrinterSelect.main.Drucken", [vars.serverIP, vars.secure_key, 0, vars.rechnungID, true, 1], function(sucess) {
                console.warn("sucess of Printing " + sucess)
                if (sucess == true) {
                    python.call("os.system",["wget -q -N http://" + vars.serverIP + "/zk-data/Rechnung/" + vars.rechnungID + ".pdf -P /tmp/"], function() {
                        python.call("os.system", ["thunderbird -compose to='" + window_rechnung_anzeigen.mail_to + "',subject='" + window_rechnung_anzeigen.rechnung_title + "',body='',attachment='/tmp/" + vars.rechnungID + ".pdf'"], function() {


                            vars.busy = false
                        })
                    });
                }
            })
            */

        }
    }

    Rectangle {
        id: rectangle_transaktion
        border.width: 1
        anchors.top: button_vorschau.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 20
        color: "transparent"

        ListView {
            id: transaktion_liste
            anchors.fill: parent

            clip: true

            model: transaktion_Model
            delegate: transaktion_Delegate
            ListModel {
                id: transaktion_Model
            }

            Component {
                id: transaktion_Delegate
                Item {
                    id: transaktion_Delegate_item

                    height: 35
                    width: transaktion_liste.width

                    Label {
                        id: label_transaktion_datum
                        text: datum + " - " + uhrzeit
                        font.pixelSize: vars.fontText + 5
                        anchors.left: transaktion_Delegate_item.left
                        anchors.leftMargin: 50
                    }
                    Label {
                        id: label_transaktion_konto_name
                        text: konto_name
                        anchors.left: label_transaktion_datum.right
                        anchors.leftMargin: 50
                        font.pixelSize: vars.fontText + 5
                    }
                    Label {
                        id: label_transaktion_betrag
                        text: betrag + " €"
                        anchors.left: label_transaktion_konto_name.right
                        anchors.leftMargin: 50
                        font.pixelSize: vars.fontText + 5
                    }
                    Label {
                        id: label_transaktion_mitteilung
                        text: mitteilung
                        anchors.right: label_transaktion_user.left
                        anchors.rightMargin: 50
                        font.pixelSize: vars.fontText + 5
                    }
                    Label {
                        id: label_transaktion_user
                        text: user
                        anchors.right: transaktion_Delegate_item.right
                        anchors.rightMargin: 50
                        font.pixelSize: vars.fontText + 5
                    }
                }
            }
        }

    }


    Busy {}
    Kalender {
        id: calendar_invoice_todo
        onSelected_dateChanged: {
            python.call("libs.send.set_rechnung_date_todo", [vars.serverIP, vars.secure_key, vars.rechnungID, selected_date], function(sucess) {
                if (sucess == true) {
                    button_select_date.text = "Zahlungsdatum: " + selected_date
                }
            })
        }
    }
/*
    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            addImportPath(Qt.resolvedUrl('/etc/zk-data/qml/'));
            importModule('libs.send', function () {});
            importModule('PrinterSelect', function () {});
            importModule('os', function () {});

            //get_rechnung()

            //setHandler("focusOrt", focusOrt);

            //call("Einstellungen.main.GetServer", [], function(ipAdr) {vars.serverIP = ipAdr});
            //call("Einstellungen.main.GetDrucker", [], function(ipAdr) {vars.druckerIP = ipAdr});
            //call("Einstellungen.main.GetName", [], function(namePC) {vars.namePC = namePC});}
        }
    }
*/
}
