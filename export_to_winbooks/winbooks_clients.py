#!/usr/bin/env python3
import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.BlueFunc

import codecs

header = ["NUMBER", "TYPE", "NAME1", "NAME2", "CIVNAME1", "CIVNAME2", "ADRESS1", "ADRESS2", "VATCAT", "COUNTRY", "VATNUMBER", "PAYCODE", "TELNUMBER", "FAXNUMBER", "BNKACCNT", "ZIPCODE", "CITY", "DEFLTPOST", "LANG", "CATEGORY", "CENTRAL", "VATCODE", "CURRENCY", "LASTREMLEV", "LASTREMDAT", "TOTDEB1", "TOTCRE1", "TOTDEBTMP1", "TOTCRETMP1", "TOTDEB2", "TOTCRE2", "TOTDEBTMP2", "TOTCRETMP2", "ISLOCKED", "MEMOTYPE", "ISDOC", "F28150", "WBMODIFIED"]
data = []


ip = sys.argv[1]
secure_key = sys.argv[2]
datum = sys.argv[3]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

path = datum
if not os.path.exists(path): os.mkdir(path)
#file = open(path + "/" + "CSF.CSV", "w")
file = codecs.open(path + "/" + "CSF.CSV", 'w', encoding='latin1').write("")
file = codecs.open(path + "/" + "CSF.CSV", 'a', encoding='latin1')

sep = ","
#data.append( sep.join(header) ) ### HEADER

#new_line = ""
#for i in header:
#    new_line = new_line + '"' + str(i) + '"'
#print(new_line)

def dict_to_file(dic):
    new_line = []
    for key, var in dic.items():
        new_line.append(str(var))    
    data.append( sep.join(new_line) )   


def create_line(client):                 
    this_dict = {}
    for i in header: this_dict[i] = ""
    #### 
    this_dict["NUMBER"] = client["identification"]
    this_dict["TYPE"] = "1"
    
    if client["name"].rstrip() == "":
        client["name"] = "BARKUNDE"
        
    this_dict["NAME1"] = client["name"]
    this_dict["ADRESS1"] = client["adresse"].replace(",", "")
    
    if client["tva"].rstrip() == "":
        this_dict["VATCAT"] = "1"    
    else:
        this_dict["VATCAT"] = "3"
        
    this_dict["COUNTRY"] = client["land"]
    
    #this_dict["VATNUMBER"] = client["tva"]
    if "BE" in client["tva"].upper():
        this_dict["VATNUMBER"] = '"' + client["tva"][-9] + client["tva"][-8] + client["tva"][-7] + "."+  client["tva"][-6] + client["tva"][-5] + client["tva"][-4] + "." + client["tva"][-3] + client["tva"][-2] + client["tva"][-1] + '"'
        
        
    else:
        if not client["tva"].rstrip() == "":
            tva_prefix = client["tva"][0] + client["tva"][1]
            this_dict["VATNUMBER"] = '"' + client["tva"].replace(tva_prefix, "").rstrip() + '"'
    
    this_dict["TELNUMBER"] = client["tel1"]
    this_dict["ZIPCODE"] = client["plz"]
    this_dict["CITY"] = client["ort"]
    
    
    dict_to_file(this_dict)
    



index = 0
#client = libs.send.GetKunden(ip, secure_key, index)    
clients = []

while True:
    rechnung_id = libs.send.search_rechnung(ip, secure_key, "", "", False, datum, index)    
    
    if rechnung_id == -1 or "RV" in rechnung_id: break
    else:
        rechnung = libs.send.get_rechnung(ip, secure_key, rechnung_id)    
        client_id = str(rechnung["kunde_id"])
        if not client_id in clients:
            if datum in rechnung["datum"]: 
                clients.append(client_id)
        
    index = index + 1



for client_id in sorted(clients):
    print("client_id: " + str(client_id))
    client = libs.send.GetKunden(ip, secure_key, client_id)
    create_line(client)
    
    
    
for line in data:
    print("write line: " + str(line))
    
    line.replace("-", "")
    
    file.write(line + "\r\n")
#file.write("\r\n".join(data))

