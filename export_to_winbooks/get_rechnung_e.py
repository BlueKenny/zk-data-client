#!/usr/bin/env python3

import sys
sys.path.append("/etc/zk-data-libs/")

import os

import libs.send
import libs.BlueFunc

server = libs.BlueFunc.getData("SERVERSTOCK")

import time

ip = sys.argv[1]
secure_key = sys.argv[2]
datum = sys.argv[3]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)
    
libs.BlueFunc.BlueMkDir(datum + "/" + "PDF_E")
index = 0
rechnung_e_id = 0

while not rechnung_e_id == -1:    
    rechnung_e_id = libs.send.search_rechnung_e(ip, secure_key, "", "", "", False, datum, index)
    rechnung_e_id = int(rechnung_e_id)

    if not rechnung_e_id == -1:
        rechnung_e = libs.send.get_rechnung_e(ip, secure_key, rechnung_e_id)
        liste = []
        for key in rechnung_e.keys():
            liste.append(str(key) + ": " + str(rechnung_e[key]))
            
        open(datum + "/" + "PDF_E/" + str(rechnung_e_id) + ".txt", "w").write("\n".join(liste))
        
        file = datum + "/PDF_E/" + str(rechnung_e_id) + ".pdf"
        file_large = datum + "/PDF_E/" + str(rechnung_e_id) + "LARGE.pdf"
        print("file: " + str(file))
        
        while not os.path.exists(file):
            print("Download " + str(file))
            # new
            url_to_pdf = libs.send.get_invoice_e_pdf(ip, secure_key, rechnung_e_id)
            # old
            #url_to_pdf = libs.send.print_invoice_e(ip, secure_key, rechnung_e_id)
            
            print("url_to_pdf: " + str(url_to_pdf))            
               
            # new
            url_to_pdf = "http://" + ip + ":51515/" + url_to_pdf
            os.system("wget -O " + file_large + " " + url_to_pdf + " && ps2pdf " + file_large + " " + file + " && rm " + file_large)
            # old
            #os.system("ps2pdf " + url_to_pdf + " " + file)
            
            #time.sleep(4)
        
        index = index + 1

    
    
    time.sleep(0.1)
