#!/usr/bin/env python3
import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send

try: import pick
except:
    os.system("pip3 install --user pick")
    import pick

server_liste = libs.send.get_server_list()
#for server in server_list():
title = "ZK-DATA Server \n\n"
server = pick.pick(server_liste, title + "Server auswählen: ", indicator='-> ')[0]

ip = server[1]
secure_key = sys.argv[1]
datum = sys.argv[2]

os.system("python3 ./winbooks_clients.py " + ip + " " + secure_key + " " + datum)
os.system("python3 ./winbooks_invoices.py " + ip + " " + secure_key + " " + datum)
os.system("python3 ./winbooks_k.py " + ip + " " + secure_key + " " + datum)
os.system("python3 ./get_rechnung_e.py " + ip + " " + secure_key + " " + datum)

#os.system("./export_rechnung_txt.py " + ip + " " + secure_key + " " + datum)

