#!/usr/bin/env python3
import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send


ip = sys.argv[1]
secure_key = sys.argv[2]
datum = sys.argv[3]


if not os.path.exists(datum): os.mkdir(datum)

file_path = datum + "/liste_factures.csv"


open(file_path, "w").write("")
file = open(file_path, "a")
file.write("date;" + datum + "\n\n")

split = ";"

index = 0

while True:
    rechnung_id = libs.send.search_rechnung(ip, secure_key, "R", "", False, datum, index)

    if rechnung_id == -1:
        break
    else:
        #print("rechnung_id: " + str(rechnung_id))
        
        rechnung = libs.send.get_rechnung(ip, secure_key, rechnung_id)        
        
        client_id = rechnung["kunde_id"]
        
        if not client_id == -1 and not client_id == 0:
            client = libs.send.GetKunden(ip, secure_key, client_id)
            client_name = client["name"]
            
        else:
            client_name = ""
          
        rest = str(rechnung["rest"])
        total = str(rechnung["total"])
        
        date = rechnung["datum"]
        date_todo = rechnung["datum_todo"]
            
            
        line = rechnung_id + split + client_id + ": " + client_name + split + rest + split + total + split + date + split + date_todo
        
        file.write(line + "\n")
        
        transaktionen = libs.send.search_transaktion_by_rechnung(ip, secure_key, rechnung_id).split("|")
        for transaktion_id in transaktionen:
            if not transaktion_id == "":
                transaktion = libs.send.get_transaktion(ip, secure_key, transaktion_id)
                betrag = str(transaktion["betrag"])
                
                line = "T" + transaktion_id + split + transaktion["datum"] + " " + transaktion["uhrzeit"] + split + betrag + split + transaktion["konto_name"]
                
                file.write(line + "\n")
            
            
        
        file.write("\n")
        
        index = index + 1
