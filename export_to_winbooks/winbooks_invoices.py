#!/usr/bin/env python3
import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.BlueFunc

import requests

import time

header = ["DOCTYPE", "DBKCODE", "DBKTYPE", "DOCNUMBER", "DOCORDER", "OPCODE", "ACCOUNTGL", "ACCOUNTRP", "BOOKYEAR", "PERIOD", "DATE", "DATEDOC", "DUEDATE", "COMMENT", "COMMENTEXT", "AMOUNT", "AMOUNTEUR", "VATBASE", "VATCODE", "CURRAMOUNT", "CURRCODE", "CUREURBASE", "VATTAX", "VATIMPUT", "CURRATE", "REMINDLEV", "MATCHNO", "OLDDATE", "ISMATCHED", "ISLOCKED", "ISIMPORTED", "ISPOSITIVE", "ISTEMP", "MEMOTYPE", "ISDOC", "DOCSTATUS", "DICFROM", "CODAKEY"]

data = []

ip = sys.argv[1]
secure_key = sys.argv[2]
datum = sys.argv[3]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

path = datum
if not os.path.exists(path): os.mkdir(path)
file = open(path + "/" + "ACT.CSV", "w")

sep = ","

def dict_to_file(dic):
    new_line = []
    for key, var in dic.items():
        new_line.append(str(var))    
    data.append( sep.join(new_line) )   


def create_line(rechnung):
    print("create_line(" + str(rechnung) + ")")
    
    rechnung["identification"] = rechnung["identification"].replace("R", "").replace("K", "").replace("-", "")

    p_dict = {
        221000: {"tva": 0.0, "htva": 0.0},## Export Ware 0% TVA
        221100: {"tva": 0.0, "htva": 0.0},## Export Dienstleistung 0% TVA
        211200: {"tva": 0.0, "htva": 0.0},## 6% TVA
        211400: {"tva": 0.0, "htva": 0.0},## 21% TVA
    }             
    
    for lieferschein_id in rechnung["lieferscheine"].split("|"):
        print("lieferschein_id: " + str(lieferschein_id))
        
        if not lieferschein_id == "":
            lieferschein = libs.send.get_delivery(ip, secure_key, lieferschein_id)
            #print("lieferschein:" + str(lieferschein))
            for index in range(0, len(lieferschein["linien"].split("|")) ):
                #print("index: " + str(index))
                            
                linie_tva_satz = float(lieferschein["tva"].split("|")[index])
                            
                ## tva
                quantity = float(lieferschein["anzahl"].split("|")[index])
                price = float(lieferschein["preis"].split("|")[index])
                pricevkh = float(lieferschein["preis_htva"].split("|")[index])
                            
                added_tva = (quantity * price) - (quantity * pricevkh)
                
                linie_total_htva = float((quantity * pricevkh))
                            
                if linie_tva_satz == 0.0:
                    article_id = lieferschein["bcode"].split("|")[index]
                    if not article_id == "":
                        article = libs.send.get_article(ip, secure_key, article_id)
                        # if article not existing ???
                        if not "categorie" in article: article["categorie"] = "0"
                        
                        if article["categorie"] == "1":
                            print("article ist in der kategorie arbeit")
                            p_dict[221100]["tva"] = p_dict[221100]["tva"] + added_tva
                            p_dict[221100]["htva"] = p_dict[221100]["htva"] + linie_total_htva
                        else:
                            p_dict[221000]["tva"] = p_dict[221000]["tva"] + added_tva
                            p_dict[221000]["htva"] = p_dict[221000]["htva"] + linie_total_htva
                        
                if linie_tva_satz == 6.0:
                    p_dict[211200]["tva"] = p_dict[211200]["tva"] + added_tva
                    p_dict[211200]["htva"] = p_dict[211200]["htva"] + linie_total_htva
                if linie_tva_satz == 21.0:
                    p_dict[211400]["tva"] = p_dict[211400]["tva"] + added_tva
                    p_dict[211400]["htva"] = p_dict[211400]["htva"] + linie_total_htva
                
                
    #print("p_dict: " + str(p_dict))   
    
    for code in p_dict:
        p_dict[code]["tva"] = round(p_dict[code]["tva"], 2)
        p_dict[code]["htva"] = round(p_dict[code]["htva"], 2)
        
        
    tva_dict = {}
    for tva_linie in rechnung["steuern"].split("|"):
        if ";" in tva_linie:
            tva_satz = tva_linie.split(";")[0]
            tva_wert = float(tva_linie.split(";")[1])
            
            if tva_satz == "21":
                if not p_dict[211400]["tva"] == tva_wert:
                    p_dict[211400]["tva"] = tva_wert
            
            if tva_satz == "6":
                if not p_dict[211200]["tva"] == tva_wert:
                    p_dict[211200]["tva"] = tva_wert
                    
  
                        
    ### Erste linie    
    this_dict = {}
    for i in header: this_dict[i] = ""
    #### 
    this_dict["DOCTYPE"] = 1
    this_dict["DBKCODE"] = rechnung["DBKCODE"] #"VENTES"
    this_dict["DOCNUMBER"] = rechnung["identification"]    
    this_dict["ACCOUNTRP"] = rechnung["kunde_id"]    
    this_dict["PERIOD"] = str(rechnung["datum"].split("-")[1])
    this_dict["DATEDOC"] = rechnung["datum"].replace("-", "")
    this_dict["DUEDATE"] = rechnung["datum_todo"].replace("-", "")
    this_dict["COMMENTEXT"] = rechnung["ref"].replace("+", "").replace("/", "")    
    this_dict["AMOUNTEUR"] = rechnung["total"]    
    
    dict_to_file(this_dict)
    
    
    #### MITTEL     
    ### linie pro TVA
    for code in p_dict:
        if not p_dict[code]["htva"] == 0.0:
            print("code: " + str(code))
            ####
            this_dict = {}
            for i in header: this_dict[i] = ""   
                
            this_dict["DOCTYPE"] = 3
            this_dict["DBKCODE"] = rechnung["DBKCODE"] #"VENTES"
            this_dict["DOCNUMBER"] = rechnung["identification"]                
            this_dict["VATIMPUT"] = code
            if code == 221000:## Export Ware 0% TVA
                this_dict["ACCOUNTGL"] = "700046"
                
            if code == 221100:## Export Dienstleistung 0% TVA
                this_dict["ACCOUNTGL"] = "700044"
                
            if code == 211200:## 6% TVA  
                this_dict["ACCOUNTGL"] = "700006"
                
            if code == 211400:## 21% TVA
                this_dict["ACCOUNTGL"] = "700021"    
                
            this_dict["AMOUNTEUR"] = -float(p_dict[code]["htva"])    
                        
            this_dict["ACCOUNTRP"] = rechnung["kunde_id"]   
            this_dict["PERIOD"] = str(rechnung["datum"].split("-")[1])
            this_dict["DATEDOC"] = rechnung["datum"].replace("-", "")
            this_dict["DUEDATE"] = rechnung["datum_todo"].replace("-", "")
            this_dict["COMMENTEXT"] = rechnung["ref"].replace("+", "").replace("/", "")
                
            dict_to_file(this_dict)
                

    #### Letzte linie        
    ### linie pro TVA
    for code in p_dict:
        if not p_dict[code]["htva"] == 0.0:
            #print("code: " + str(code))
            ####
            this_dict = {}
            for i in header: this_dict[i] = ""   
                
            this_dict["DOCTYPE"] = 3
            this_dict["DBKCODE"] = rechnung["DBKCODE"] #"VENTES"
            this_dict["DOCNUMBER"] = rechnung["identification"]                
            this_dict["VATCODE"] = code
                
            this_dict["AMOUNTEUR"] = -float(p_dict[code]["tva"])   
            this_dict["VATBASE"] = float(p_dict[code]["htva"])   
              
            this_dict["ACCOUNTRP"] = rechnung["kunde_id"]   
            this_dict["PERIOD"] = str(rechnung["datum"].split("-")[1])
            this_dict["DATEDOC"] = rechnung["datum"].replace("-", "")
            this_dict["DUEDATE"] = rechnung["datum_todo"].replace("-", "")
            this_dict["COMMENTEXT"] = rechnung["ref"].replace("+", "").replace("/", "")
                
            ## verification !!! 
            ### wenn es wirklich die letzte linie ist
            print("p_dict: " + str(p_dict))
            letzter_code = ""
            for a in p_dict:
                letzter_code = a                
                
            print("letzter_code: " + str(letzter_code))
            if letzter_code == code:
                check_linie = []
                # Erste linie
                check_linie.append(float(rechnung["total"]))
                # Mittlere & Letzte Linien
                for t in p_dict:
                    check_linie.append(-float(p_dict[t]["htva"]))
                    check_linie.append(-float(p_dict[t]["tva"]))
                # start check
                check_summe = 0.0
                for wert in check_linie:
                    check_summe = round(check_summe + wert, 2)
                if not check_summe == 0.00:
                    this_dict["AMOUNTEUR"] = this_dict["AMOUNTEUR"] - check_summe
                    this_dict["AMOUNTEUR"] = round(this_dict["AMOUNTEUR"], 2)
            
                    
                    
                    
            dict_to_file(this_dict)    
    
                    
            
    #####################################




index = 0
while True:    
    rechnung_id = libs.send.search_rechnung(ip, secure_key, "R", "", False, datum, index)    
    
    if rechnung_id == -1: break
    else:
        rechnung = libs.send.get_rechnung(ip, secure_key, rechnung_id)
        rechnung["DBKCODE"] = "VENTES"
        
        if datum in rechnung["datum"]:
            create_line(rechnung)        

            #### Download
            if not os.path.exists(datum + "/PDF_S/" + rechnung_id + ".pdf"):
                url = libs.send.get_invoice_pdf(ip, secure_key, rechnung_id)
                print("url: " + str(url))
                
                if not os.path.exists(datum + "/PDF_S/"):
                    os.mkdir(datum + "/PDF_S/")
                    
                os.system("wget -N http://" + ip + ":51515" + url + " -P " + datum + "/PDF_S/")
        
        index = index + 1
       


ventes_c = {}
index = 0
while True:    
    rechnung_id = libs.send.search_rechnung(ip, secure_key, "K", "", False, datum, index)    
    
    if rechnung_id == -1: break
    else:
        rechnung = libs.send.get_rechnung(ip, secure_key, rechnung_id)        
        if datum in rechnung["datum"]:
            if not rechnung["kunde_id"] == "0": ## inkl. Kunde
                rechnung["DBKCODE"] = "DIR"                
                create_line(rechnung)
                     
                #### Download
                if not os.path.exists(datum + "/PDF_DIR/" + rechnung_id + ".pdf"):
                    url = libs.send.get_invoice_pdf(ip, secure_key, rechnung_id)
                    
                    if not os.path.exists(datum + "/PDF_DIR/"):
                        os.mkdir(datum + "/PDF_DIR/")
                        
                    os.system("wget -N http://" + ip + ":51515" + url + " -P " + datum + "/PDF_DIR/")
                    
                    
                        
            else:                               ## ohne Kunde = pro datum
            
                #### Download
                if not os.path.exists(datum + "/PDF_VENC/" + str(rechnung["datum"]) + "/" + rechnung_id + ".pdf"):
                    url = libs.send.get_invoice_pdf(ip, secure_key, rechnung_id)
                    
                    if not os.path.exists(datum + "/PDF_VENC/"):
                        os.mkdir(datum + "/PDF_VENC/")
                    if not os.path.exists(datum + "/PDF_VENC/" + str(rechnung["datum"])):
                        os.mkdir(datum + "/PDF_VENC/" + str(rechnung["datum"]))
                                                
                    os.system("wget -N http://" + ip + ":51515" + url + " -P " + datum + "/PDF_VENC/" + str(rechnung["datum"]) + "/")              
                    
                date = rechnung["datum"]
                if date in ventes_c:
                    print("Exists")
                    ventes_c[date]["lieferscheine"] = ventes_c[date]["lieferscheine"] + "|" + rechnung["lieferscheine"]
                    ventes_c[date]["total_htva"] = round(ventes_c[date]["total_htva"] + rechnung["total_htva"], 2)
                    ventes_c[date]["total"] = round(ventes_c[date]["total"] + rechnung["total"], 2)
                    ventes_c[date]["ref"] = ""#ventes_c[date]["ref"] + "|" + rechnung["ref"] # for TEST
                    ventes_c[date]["rest"] = round(ventes_c[date]["rest"] + rechnung["rest"], 2)
                    
                    steuern = {}
                    for tva_linie in ventes_c[date]["steuern"].split("|"):
                        if ";" in tva_linie:
                            tva_satz = tva_linie.split(";")[0]
                            tva_wert = float(tva_linie.split(";")[1])
                            steuern[tva_satz] = tva_wert
                            
                    for tva_linie in rechnung["steuern"].split("|"):
                        if ";" in tva_linie:
                            tva_satz = tva_linie.split(";")[0]
                            tva_wert = float(tva_linie.split(";")[1])
                            
                            if tva_satz in steuern:
                                steuern[tva_satz] = round(steuern[tva_satz] + tva_wert, 2)
                            else:
                                steuern[tva_satz] = float(tva_wert)
                    
                    steuern_list = []
                    for tva in steuern:
                        print("tva: " + str(tva))
                        steuern_list.append(str(tva) + ";" + str(steuern[tva]))
                    ventes_c[date]["steuern"] = "|".join(steuern_list)
                    
                    
                else:
                    rechnung["identification"] = rechnung["datum"]
                    rechnung["DBKCODE"] = "VENC"
                     
                    del rechnung["vorlage"]
                    del rechnung["created_by"]
                    ventes_c[date] = rechnung     
        
        index = index + 1
        
        
for x in ventes_c:    
    print("ventes_c " + str(x) )
    #print(str(ventes_c[x]))
    create_line( ventes_c[x] )
    print("")        

file.write("\n".join(data))


