#!/usr/bin/env python3
import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.BlueFunc

ip = sys.argv[1]
secure_key = sys.argv[2]
dieses_datum = sys.argv[3]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

konto = 1

if not os.path.exists(dieses_datum + "/Kasse/" + str(konto)): os.system("mkdir -p " + dieses_datum + "/Kasse/" + str(konto))
    
year = dieses_datum.split("-")[0]
print("year: " + str(year))
try:
    month = dieses_datum.split("-")[1]
    if len(month) == 1: month = "0" + month
except: month = "06"
print("month: " + str(month))
try:
    day = dieses_datum.split("-")[2]
    if len(day) == 1: day = "0" + day
except: day = "15"
print("day: " + str(day))

datum = year + "-" + month + "-" + day
print("datum: " + str(datum))

dates = []

test_datum = libs.BlueFunc.DateAddDays(datum, -1)
while True:
    #print("test_datum: " + test_datum)
    t = libs.send.get_cashbook_date_after(ip, secure_key, konto, test_datum)
    #print("t: " + t)
    if str(dieses_datum) in str(t) or str(dieses_datum) == str(t):
        dates.append(t)
        
        if str(t) == str(test_datum): break
        else: test_datum = t
    else:
        break
test_datum = libs.BlueFunc.DateAddDays(datum, 1)
while True:
    #print("test_datum: " + test_datum)
    t = libs.send.get_cashbook_date_before(ip, secure_key, konto, test_datum)
    #print("t: " + t)
    if str(dieses_datum) in str(t) or str(dieses_datum) == str(t):
        dates.append(t)
        
        if str(t) == str(test_datum): break
        else: test_datum = t
    else:
        break
        
dates = sorted(dates)
print("dates: " + str(dates))

for date in dates:
    path = dieses_datum + "/Kasse/" + str(konto) + "/"
    if not os.path.exists(path + date + ".pdf"):
        url = libs.send.get_cashbook_pdf(ip, secure_key, konto, date)
        print("url: " + str(url))
        os.system("wget -q -N http://" + ip.split(":51515")[0] + ":51515" + url + " -P " + path)
